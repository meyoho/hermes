module bitbucket.org/mathildetech/hermes

go 1.13

replace (
	alauda.io/asm-controller v0.0.0-20191021093028-1c01fa5588a7 => bitbucket.org/mathildetech/asm-controller v0.0.0-20191021093028-1c01fa5588a7
	alauda.io/auth-controller v0.0.3-0.20190617223321-c32a3282b87c => bitbucket.org/mathildetech/auth-controller2 v0.0.3-0.20190617223321-c32a3282b87c
	github.com/Sirupsen/logrus v1.1.1 => github.com/sirupsen/logrus v1.1.1

	github.com/blang/semver v3.6.1+incompatible => github.com/blang/semver v3.5.1+incompatible
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b => k8s.io/klog v0.3.2
	golang.org/x/text => github.com/golang/text v0.3.0

	// replacing dependencies updated by alauda-backend/cyborg
	k8s.io/api v0.0.0-20190313235455-40a48860b5ab => k8s.io/api v0.0.0-20181213150558-05914d821849
	k8s.io/apimachinery v0.0.0-20190313205120-d7deff9243b1 => k8s.io/apimachinery v0.0.0-20181127025237-2b1284ed4c93
	k8s.io/apiserver v0.0.0-20181126153457-92fdef3a232a => k8s.io/apiserver v0.0.0-20181213151703-3ccfe8365421
	k8s.io/client-go v11.0.0+incompatible => k8s.io/client-go v0.0.0-20181213151034-8d9ed539ba31
	k8s.io/kube-openapi v0.0.0-20190603182131-db7b694dc208 => k8s.io/kube-openapi v0.0.0-20181021203552-90b54e673cf4
)

require (
	alauda.io/asm-controller v0.0.0-20191021093028-1c01fa5588a7
	bitbucket.org/mathildetech/alauda-backend v0.1.26-0.20191231014411-5aea2188c493
	bitbucket.org/mathildetech/app v1.0.1
	bitbucket.org/mathildetech/log v1.0.5
	github.com/alauda/cyborg v0.5.1
	github.com/emicklei/go-restful v2.9.6+incompatible
	github.com/ghodss/yaml v1.0.0
	github.com/gogo/protobuf v1.2.1
	github.com/google/uuid v1.1.1
	github.com/gorilla/websocket v1.4.1
	github.com/gregjones/httpcache v0.0.0-20190212212710-3befbb6ad0cc // indirect
	github.com/juju/errgo v0.0.0-20140925100237-08cceb5d0b53 // indirect
	github.com/juju/errors v0.0.0-20181118221551-089d3ea4e4d5
	github.com/onsi/gomega v1.5.0
	github.com/pkg/errors v0.8.1
	github.com/prometheus/client_golang v0.9.3-0.20190127221311-3c4408c8b829
	github.com/prometheus/common v0.2.0
	github.com/pulcy/kube-lock v0.0.0-20180318111338-34858f5cae77
	github.com/robfig/cron v1.2.0
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.3.2
	github.com/stretchr/testify v1.4.0
	istio.io/api v0.0.0-20190718035123-99722f53e70c
	k8s.io/api v0.0.0-20190313235455-40a48860b5ab
	k8s.io/apiextensions-apiserver v0.0.0-20181130152704-e298867b551b
	k8s.io/apimachinery v0.0.0-20190313205120-d7deff9243b1
	k8s.io/apiserver v0.0.0-20181126153457-92fdef3a232a // indirect
	k8s.io/client-go v11.0.0+incompatible
	k8s.io/klog v1.0.0
	sigs.k8s.io/yaml v1.1.0
)
