module bitbucket.org/mathildetech/app

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/fatih/color v1.7.0
	github.com/gosuri/uitable v0.0.0-20160404203958-36ee7e946282
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mgechev/dots v0.0.0-20181228164730-18fa4c4b71cc // indirect
	github.com/mgechev/revive v0.0.0-20190505013521-22b849f28677 // indirect
	github.com/olekukonko/tablewriter v0.0.1 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.3.1
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f // indirect
	golang.org/x/net v0.0.0-20190514140710-3ec191127204 // indirect
	golang.org/x/sys v0.0.0-20190516110030-61b9204099cb // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190517183331-d88f79806bbd // indirect
)
