package client

import (
	dc "github.com/alauda/cyborg/pkg/client"
	restful "github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	"bitbucket.org/mathildetech/log"
)

// DefaultManager default client manager
type DefaultManager struct {
	// Config configuration for manager
	config *Config

	// Configuration generation for secure authorized requests
	ConfigGeneratorFuncs []ConfigGenFunc

	// InsecureConfigGeneratorFuncs list of configuration generation methods
	// for insecure clients
	InsecureConfigGeneratorFuncs []ConfigGenFunc
}

var _ Manager = &DefaultManager{}

// NewManager inits a manager
func NewManager() *DefaultManager {
	return &DefaultManager{
		// GeneratorFuncs:               []GeneratorFunc{},
		ConfigGeneratorFuncs: []ConfigGenFunc{},
		// InsecureGeneratorFuncs:       []GeneratorFunc{},
		InsecureConfigGeneratorFuncs: []ConfigGenFunc{},
	}
}

// WithConfig sets a configuration to manager
func (m *DefaultManager) WithConfig(config *Config) *DefaultManager {
	m.config = config
	return m
}

// With adds client configuration generator
func (m *DefaultManager) With(gen ...ConfigGenFunc) *DefaultManager {
	m.ConfigGeneratorFuncs = append(m.ConfigGeneratorFuncs, gen...)
	return m
}

// WithInsecure adds insecure client configuration generator
func (m *DefaultManager) WithInsecure(gen ...ConfigGenFunc) *DefaultManager {
	m.InsecureConfigGeneratorFuncs = append(m.InsecureConfigGeneratorFuncs, gen...)
	return m
}

// InsecureClient returns InCluster configuration client
// using pod's service account or according to kubeconfig during init
func (m *DefaultManager) InsecureClient() (client kubernetes.Interface, err error) {
	if m.config == nil || len(m.InsecureConfigGeneratorFuncs) == 0 {
		err = errors.NewUnauthorized("No client configuration provided")
		return
	}
	client, err = m.genClient(nil, m.InsecureConfigGeneratorFuncs...)
	if err != nil {
		m.config.Log.Error("insecure client generation failed", log.Err(err))
	}
	return
}

// Client returns a client given a request authorization options
func (m *DefaultManager) Client(req *restful.Request) (client kubernetes.Interface, err error) {
	if m.config == nil || len(m.ConfigGeneratorFuncs) == 0 {
		err = errors.NewUnauthorized("No client configuration provided")
		return
	}
	client, err = m.genClient(req, m.ConfigGeneratorFuncs...)
	if err != nil {
		m.config.Log.Error("secure client generation failed", log.Err(err))
	}
	return
}

// Config gives an authenticated rest config given request
func (m *DefaultManager) Config(req *restful.Request) (config *rest.Config, err error) {
	for _, gen := range m.ConfigGeneratorFuncs {
		config, err = gen(m.config, req)
		if err == nil && config != nil {
			m.config.setupConfig(config, err)
			return
		}
	}
	return
}

// DynamicClient genreates a dynamic client instance
func (m *DefaultManager) DynamicClient(req *restful.Request, gvk *schema.GroupVersionKind) (client dynamic.NamespaceableResourceInterface, err error) {
	if m.config == nil || len(m.ConfigGeneratorFuncs) == 0 {
		err = errors.NewUnauthorized("No client configuration provided")
		return
	}
	client, err = m.genDynamicClient(req, gvk, m.ConfigGeneratorFuncs...)
	if err != nil {
		m.config.Log.Error("secure client generation failed", log.Err(err))
	}
	return
}

func (m *DefaultManager) genClient(req *restful.Request, genFuncs ...ConfigGenFunc) (client kubernetes.Interface, err error) {
	var config *rest.Config
	for _, gen := range genFuncs {
		config, err = gen(m.config, req)
		if err == nil && config != nil {
			m.config.setupConfig(config, err)
			client, err = kubernetes.NewForConfig(config)
		}
		if err == nil && client != nil {
			return
		}
	}
	return
}

func (m *DefaultManager) genDynamicClient(req *restful.Request, gvk *schema.GroupVersionKind, genFuncs ...ConfigGenFunc) (client dynamic.NamespaceableResourceInterface, err error) {
	var (
		config       *rest.Config
		cyborgClient *dc.KubeClient
	)
	for _, gen := range genFuncs {
		config, err = gen(m.config, req)
		if err != nil || config == nil {
			continue
		}
		m.config.setupConfig(config, err)
		gv := gvk.GroupVersion()
		config.GroupVersion = &gv

		if gv.String() == "v1" {
			config.APIPath = "/api"
		} else {
			config.APIPath = "/apis"
		}
		cyborgClient, err = dc.NewKubeClient(config, "default")
		if err != nil || cyborgClient == nil {
			continue
		}
		client, err = cyborgClient.ClientForGVK(*gvk)
		if err == nil && client != nil {
			return
		}
	}
	return
}

// ManagerConfig returns a clone of manager's configuration.
func (m *DefaultManager) ManagerConfig() Config {
	return *m.config
}
