package main

import (
	"math/rand"
	"os"
	"runtime"
	"time"

	"bitbucket.org/mathildetech/hermes/cmd/hermes/app"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	if len(os.Getenv("GOMAXPROCS")) == 0 {
		runtime.GOMAXPROCS(runtime.NumCPU())
	}
	app.NewApp().Run()
}
