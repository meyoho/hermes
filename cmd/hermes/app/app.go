package app

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/registry"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server/options"
	"bitbucket.org/mathildetech/app"
	"bitbucket.org/mathildetech/hermes/pkg/collect"
	"bitbucket.org/mathildetech/hermes/pkg/handler"
	"bitbucket.org/mathildetech/hermes/pkg/logging"
)

const commandDesc = `The application is a WEB server. In addition to providing apis for alauda service mesh UI .`

// NewApp creates a new hermes app
func NewApp() *app.App {
	opts := options.With(
		options.NewLogOptions(),
		options.NewKlogOptions(),
		options.NewInsecureServingOptions(),
		options.NewClientOptions(),
		&options.DebugOptions{EnableProfiling: true},
		options.NewMetricsOptions(),
		options.NewAPIRegistryOptions(),
		options.NewOpenAPIOptions(),
		options.NewErrorOptions())
	opts.Add(&logging.LoggingOptioner{})
	opts.Add(collect.NewCollectOptions())
	application := app.NewApp("hermes",
		"asm",
		app.WithOptions(opts),
		app.WithDescription(commandDesc),
		app.WithRunFunc(run(opts)),
	)
	return application
}

func run(opts options.Optioner) app.RunFunc {
	return func(basename string) error {
		srv := server.New(basename)
		registry.AddBuilder(handler.RegisterToServer)
		if err := opts.ApplyToServer(srv); err != nil {
			return err
		}

		//add cron job
		cjob := collect.GetCronJob()
		err := cjob.AddFunc()
		if err != nil {
			return err
		}
		defer cjob.Stop()
		go cjob.Start()

		srv.Start()
		return nil
	}
}
