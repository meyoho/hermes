package models

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/client"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	restful "github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
)

// QueryOptions just for query paramaters or pass query related objs to bussiness function
type QueryOptions struct {
	Server   server.Server
	Request  *restful.Request
	Response *restful.Response
	Query    *dataselect.Query
	Manager  client.Manager
}

func NewQueryOptions(server server.Server, req *restful.Request, resp *restful.Response) *QueryOptions {
	query := abcontext.Query(req.Request.Context())

	return &QueryOptions{
		Server:   server,
		Request:  req,
		Response: resp,
		Query:    query,
		Manager:  server.GetManager(),
	}

}

// GetToken return the token from request
func (query *QueryOptions) GetToken() string {

	return client.GetToken(query.Request)
}

// GetClusterName return the clusterName from request
func (query *QueryOptions) GetClusterName() string {
	cfg := query.Manager.ManagerConfig()
	return client.GetClusterName(cfg.MultiClusterParameterName, query.Request)
}

// GetRestConfig return the restconfig for k8s client
func (query *QueryOptions) GetConfig() (config *rest.Config, err error) {
	return query.Manager.Config(query.Request)
}

// GetDynamicClient return the dynamicClient
func (query *QueryOptions) GetDynamicClient(gvk *schema.GroupVersionKind) (client dynamic.NamespaceableResourceInterface, err error) {

	cfg, err := query.Manager.Config(query.Request)
	if err != nil {
		return nil, err
	}

	client, err = query.Manager.GetDynamicClient(cfg, gvk)
	if err != nil {
		return nil, err
	}

	return

}
