package models

import (
	wk "bitbucket.org/mathildetech/hermes/pkg/workload"
)

// ViewWorkload just as workload show for UI
type ViewWorkload struct {
	// Name of the workload
	// required: true
	// example: reviews-v1
	Name string `json:"name"`

	// Type of the workload
	// required: true
	// example: deployment
	Type     string   `json:"type"`
	Services []string `json:"services"`

	// Pods bound to the workload
	Pods []*ViewPod `json:"pods,omitempty"`
}

// ViewPod as pod show for UI
type ViewPod struct {
	Name   string `json:"name"`
	Status string `json:"status"`
}

// ViewPod as pod show for UI
type SourceWorkloads struct {
	Name string   `json:"name"`
	Apps []string `json:"apps,omitempty"`
}

// ViewPod as pod show for UI
type WorkloadPath struct {
	Paths []string `json:"paths"`
}

// Parse as parsing the workload to view model
func (wd *ViewWorkload) Parse(workload *wk.Workload) {
	wd.Name = workload.Name
	wd.Type = workload.Type

	// TODO : just for integration test, will be commented out for production
	//wd.Pods = wd.ParsePods(workload.Pods)

}

// ParsePods as  parsing pods to view model
func (wd *ViewWorkload) ParsePods(pods []*wk.Pod) (viewPods []*ViewPod) {

	for _, pod := range pods {
		viewPods = append(viewPods, &ViewPod{
			Name:   pod.Name,
			Status: pod.Status,
		})

	}

	return
}
