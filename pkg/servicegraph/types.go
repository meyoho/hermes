package servicegraph

import (
	"fmt"
	"hash/fnv"
	"reflect"
	"strings"

	"bitbucket.org/mathildetech/hermes/pkg/microservice"
	"istio.io/api/networking/v1alpha3"
)

type Graph struct {
	Namespace          string          `json:"namespace"`
	StartTime          int             `json:"start_time"`
	EndTime            int             `json:"end_time"`
	Nodes              []*Node         `json:"nodes"`
	Edges              []*Edge         `json:"edges"`
	InjectServiceNodes bool            `json:"-"`
	Bussiness          BussinessModels `json:"-"`
}

type Node struct {
	Id              uint32            `json:"id"`
	Namespace       string            `json:"namespace"`
	Service         string            `json:"service"`
	NodeType        string            `json:"node_type,omitempty"`
	Workload        string            `json:"workload"`
	Version         string            `json:"version"`
	IsRoot          bool              `json:"is_root"`
	App             string            `json:"app"`
	HasIstioSideCar *bool             `json:"has_istio_sidecar,omitempty"`
	SvcList         map[string]string `json:"-"`
	HasTLS          *bool             `json:"has_TLS,omitempty"`
	HasCB           *bool             `json:"has_CB,omitempty"`
	Metadata        Metadata          `json:"metadata"`
	BelongTo        BelongTo          `json:"belongs_to,omitempty"`
}

// BussinessModels
type BussinessModels struct {
	Workloads        Workloads
	Microservices    Microservices
	DestinationRules []v1alpha3.DestinationRule
}

// BelongTo means Node connected to other resources, such as microservice or gateways, etc.
type BelongTo struct {
	Microservice string `json:"microservice,omitempty"`
}

func (n *Node) handleUnknownApp() {

	workloadOk := IsValideIstioLabel(n.Workload)
	appOk := IsValideIstioLabel(n.App)

	// workload
	if !appOk && workloadOk {
		if strings.HasPrefix(n.Workload, "istio") {
			n.App = n.Workload
			return
		}

		n.App = fmt.Sprintf("unknown(wl_%s)", n.Workload)
		return
	}

	if n.Version == Unknown {
		n.Version = ""
	}

}

func (n *Node) handleSvcList() {

	if n.NodeType == NodeTypeWorkload && n.SvcList != nil {
		svcString := ""
		for svc := range n.SvcList {
			svcString = fmt.Sprintf("%s,%s", svcString, svc)
		}

		svcString = strings.TrimLeft(svcString, ",")

		n.Service = svcString
	}

}

func (n *Node) handleBelongTo(microservices Microservices) {
	var microservice microservice.MicroService
	var err error

	if n.NodeType == NodeTypeWorkload {
		microservice, err = microservices.GetMicroserviceByDeploymentName(n.Workload)
		if err != nil {
			logger.Errorf("get microservice by deployment err occurs : %s", err)
			return
		}

	}

	if n.NodeType == NodeTypeService {
		microservice, err = microservices.GetMicroserviceByServiceName(n.Service)
		if err != nil {
			logger.Errorf("get microservice by service err occurs : %s", err)
			return
		}
	}
	if reflect.ValueOf(n.BelongTo).IsZero() {
		n.BelongTo = BelongTo{}
	}

	n.BelongTo.Microservice = microservice.Name

}

func (n *Node) hasCircuitBreaker(drs []v1alpha3.DestinationRule) {

	if len(drs) > 0 {

		// for the workload nodes
		if n.NodeType == NodeTypeWorkload && n.SvcList != nil {

			for _, svcname := range n.SvcList {
				if hasCircuitBreaker(svcname, drs) {
					hascb := true
					n.HasCB = &hascb
				}
				if hasPolicy(svcname, drs) {
					hastls := true
					n.HasTLS = &hastls
				}
			}
		}

		// for the service nodes
		if n.NodeType == NodeTypeService && n.Service != "" {

			if hasCircuitBreaker(n.Service, drs) {
				hascb := true
				n.HasCB = &hascb
			}
			if hasPolicy(n.Service, drs) {
				hastls := true
				n.HasTLS = &hastls
			}

		}
	}

}

type Edge struct {
	SourceId uint32   `json:"source_id"`
	TargetId uint32   `json:"target_id"`
	Metadata Metadata `json:"metadata"`
}

func ID(workload, namespace, app, version, service string) (uint32, string) {

	h := fnv.New32a()

	// first, check for the special-case "unknown" source node
	if Unknown == namespace && Unknown == workload && Unknown == app && "" == service {
		logger.Debugf("unknown_source: namespace=[%s] workload=[%s] app=[%s] version=[%s] service=[%s] ", namespace, workload, app, version, service)
		h.Write([]byte(fmt.Sprintf("unknown_source")))
		// skip unknown source
		return h.Sum32(), NodeTypeUnknown
	}

	workloadOk := IsValideIstioLabel(workload)
	serviceOk := IsValideIstioLabel(service)

	// app or version invalided and workload service invalided, skip
	if !workloadOk && !serviceOk {
		logger.Debugf("Failed ID gen: namespace=[%s] workload=[%s] app=[%s] version=[%s] service=[%s] ", namespace, workload, app, version, service)
		return 0, NodeTypeUnknown
	}

	// workload invalided ,using service,
	//Currently we cannot support service node type ,skip
	if !workloadOk {
		h.Write([]byte(fmt.Sprintf("svc_%s_%s", namespace, service)))
		return h.Sum32(), NodeTypeService
	}

	// workload as id
	h.Write([]byte(fmt.Sprintf("wl_%s_%s", namespace, workload)))
	return h.Sum32(), NodeTypeWorkload
}

// IsValideIstioLabel just validates that a  label value is not empty or unknown
func IsValideIstioLabel(labelVal string) bool {
	return labelVal != "" && labelVal != Unknown
}
