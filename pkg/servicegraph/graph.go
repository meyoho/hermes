package servicegraph

import (
	"context"
	"sort"

	"bitbucket.org/mathildetech/hermes/pkg/destinationrule"
	"bitbucket.org/mathildetech/hermes/pkg/logging"
	"istio.io/api/networking/v1alpha3"

	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/models"
	"bitbucket.org/mathildetech/hermes/pkg/prometheus"
	wd "bitbucket.org/mathildetech/hermes/pkg/workload"
	"github.com/prometheus/common/model"

	client "k8s.io/client-go/kubernetes"
)

var (
	logger = logging.RegisterScope("servicegraph")
)

const (
	Unknown               string = "unknown" // Istio unknown label value
	NodeTypeService       string = "service"
	NodeTypeUnknown       string = "unknown" // The special "unknown" traffic gen node
	NodeTypeWorkload      string = "workload"
	NodeTypeUnknowService string = "unknown_service"

	ReporterTypeDestination string = "destination"
	MutualTLS               string = "mutual_tls"
)

// GetGraph returns the graph with namespace
func GetGraph(ctx context.Context, opts *models.QueryOptions, k8sclient client.Interface, promClient *prometheus.Client, namespace string, startTime, endTime int, injectServiceNodes bool, clusterName string) (*Graph, error) {
	nsQuery := common.NewSameNamespaceQuery(namespace)
	workloadsChan := make(chan Workloads)
	errChan := make(chan error)

	// 这里是启动协程去获取workloads,跟主线程获取prometheus的数据并行处理，
	go getWorkloads(k8sclient, nsQuery, workloadsChan, errChan)

	// 主线程获取prometheus数据
	metrics, err := promClient.GetNamespaceTraffic(namespace, startTime, endTime)
	if err != nil {
		return nil, err
	}

	workloads := <-workloadsChan

	graph := Graph{Namespace: namespace, StartTime: startTime, EndTime: endTime, InjectServiceNodes: injectServiceNodes}

	// get destinationRules by namespaces
	dyClientDR, err := destinationrule.DRClient(ctx, opts)
	if err != nil {
		logger.Errorf("error get destinationrule.DRClient err is %v", err)
		return nil, err
	}

	drs, err := destinationrule.GetDestinationRuleListWithNS(dyClientDR, graph.Namespace)
	if err != nil {
		logger.Errorf(" error get GetGraph GetDestinationRuleListWithNS,err is %+v \n", err)
		return nil, err
	}

	microservices, err := FetchMicroservicesByNamespace(ctx, opts, graph.Namespace)
	if err != nil {
		logger.Errorf("FetchMicroservicesByNamespace err occurs,err is %+v \n", err)
		return nil, err
	}

	graph.Bussiness = BussinessModels{Workloads: workloads, DestinationRules: drs, Microservices: microservices}
	logger.Debugf("graph bussiness models  %+v \n", graph.Bussiness)
	return populateGraph(&graph, &metrics), nil
}

func populateGraph(graph *Graph, vector *model.Vector) *Graph {
	for _, s := range *vector {
		m := s.Metric
		lSourceWlNs, sourceWlNsOk := m["source_workload_namespace"]
		lSourceWl, sourceWlOk := m["source_workload"]
		lSourceApp, sourceAppOk := m["source_app"]
		lSourceVer, sourceVerOk := m["source_version"]
		lDestSvcNs, destSvcNsOk := m["destination_service_namespace"]
		lDestSvcName, destSvcNameOk := m["destination_service_name"]
		lDestWl, destWlOk := m["destination_workload"]
		lDestApp, destAppOk := m["destination_app"]
		lDestVer, destVerOk := m["destination_version"]
		//lCsp, cspOk := m["connection_security_policy"]
		lProtocol, protocolOk := m["request_protocol"]
		lCode, codeOk := m["response_code"]
		lFlags, flagsOk := m["response_flags"]

		if !sourceWlNsOk || !sourceWlOk || !sourceAppOk || !sourceVerOk || !destSvcNsOk || !destSvcNameOk || !destWlOk || !destAppOk || !destVerOk || !protocolOk || !flagsOk || !codeOk {
			logger.Debugf("Skipping %s, missing expected TS labels \n", m.String())
			continue
		}

		sourceWlNs := string(lSourceWlNs)
		sourceWl := string(lSourceWl)
		sourceApp := string(lSourceApp)
		sourceVer := string(lSourceVer)
		destSvcNs := string(lDestSvcNs)
		destSvcName := string(lDestSvcName)
		destWl := string(lDestWl)
		destApp := string(lDestApp)
		destVer := string(lDestVer)
		val := float64(s.Value)
		protocol := string(lProtocol)
		code := string(lCode)
		flags := string(lFlags)
		//csp := string(lCsp)

		_, sourceNodeType := ID(sourceWl, sourceWlNs, sourceApp, sourceVer, "")
		if sourceNodeType == NodeTypeUnknown {
			// generated id failed ,skip
			continue
		}

		_, destNodeType := ID(destWl, destSvcNs, destApp, destVer, destSvcName)
		if destNodeType == NodeTypeUnknown {
			// generated id failed ,skip
			continue
		}

		// make code more readable by setting "host" because "destSvcName" holds destination.service.host | request.host | "unknown"
		host := destSvcName

		if graph.InjectServiceNodes && destNodeType == NodeTypeWorkload {
			graph.addTriffic(val, protocol, code, flags, host, sourceWl, sourceWlNs, sourceApp, sourceVer, "", "", destSvcNs, "", "", destSvcName, code)
			graph.addTriffic(val, protocol, code, flags, host, "", destSvcNs, "", "", destSvcName, destWl, destSvcNs, destApp, destVer, destSvcName, code)
		} else {
			graph.addTriffic(val, protocol, code, flags, host, sourceWl, sourceWlNs, sourceApp, sourceVer, "", destWl, destSvcNs, destApp, destVer, destSvcName, code)
		}
	}

	graph.setRootNodes()
	graph.sortNodes()
	return graph
}

func (g *Graph) addTriffic(val float64, protocol, code, flags, host, sourceWl, sourceWlNs, sourceApp, sourceVer, sourceService, destWl, destSvcNs, destApp, destVer, destSvcName, responseCode string) {

	sourceId, sourceNodeType := ID(sourceWl, sourceWlNs, sourceApp, sourceVer, sourceService)
	if sourceNodeType == NodeTypeUnknown {
		// generated id failed ,skip
		return
	}
	targetId, destNodeType := ID(destWl, destSvcNs, destApp, destVer, destSvcName)
	if destNodeType == NodeTypeUnknown {
		// generated id failed ,skip
		return
	}
	// a destination service node with no incoming traffic , is dead.
	// This is caused by an edge case (pod life-cycle change) that we don't want to see.
	if !g.InjectServiceNodes && destNodeType == NodeTypeService && val == 0 {
		return
	}

	_, sourceWorkload := g.Bussiness.Workloads.findWorkload(g.Namespace, sourceWlNs, sourceWl, sourceNodeType)

	_, destWorkload := g.Bussiness.Workloads.findWorkload(g.Namespace, destSvcNs, destWl, destNodeType)

	var source, dest *Node
	var edge *Edge

	hasSourceNode, hasDestNode := false, false

	if g.Namespace == sourceWlNs {
		// no source service replace with sourceApp
		source, hasSourceNode = g.addNode(sourceId, sourceWl, sourceWlNs, sourceApp, sourceVer, sourceApp, sourceNodeType, sourceWorkload)

	}
	if g.Namespace == destSvcNs {
		dest, hasDestNode = g.addNode(targetId, destWl, destSvcNs, destApp, destVer, destSvcName, destNodeType, destWorkload)
	}
	if hasSourceNode && hasDestNode {
		edge = g.addEdge(sourceId, targetId, responseCode, val)
	}

	if source != nil && dest != nil && edge != nil {

		AddToMetadata(protocol, val, code, flags, host, source.Metadata, dest.Metadata, edge.Metadata)

	} else {
		logger.Errorf("souce %v or dest %v or edge %v not exists,skip AddToMetadata", source, dest, edge)
	}
}

func (g *Graph) addNode(id uint32, workload, namespace, app, version, service, nodeType string, wk *wd.Workload) (*Node, bool) {
	var foundNode *Node
	var isFound bool = false
	var hasIstioSideCar bool

	// always using app from workload
	if nodeType == NodeTypeWorkload && wk != nil {
		app = wk.GetApp()
		version = wk.GetVersion()

		// sidecar
		hasIstioSideCar = wk.Pods.HasIstioSideCar()
	}

	for _, n := range g.Nodes {
		if id == n.Id {
			foundNode = n
			isFound = true
		}
	}
	if !isFound {
		metadata := make(Metadata)
		newNode := &Node{Id: id, Namespace: namespace, Service: service, Workload: workload, Version: version, App: app, NodeType: nodeType, Metadata: metadata}

		g.Nodes = append(g.Nodes, newNode)

		foundNode = newNode
	}

	if foundNode != nil && foundNode.SvcList == nil {
		foundNode.SvcList = make(map[string]string)
	}

	if foundNode != nil {
		foundNode.SvcList[service] = service
		isFound = true
	}

	if isFound && g.InjectServiceNodes && workload != Unknown {
		foundNode.Workload = workload
	}

	//
	if isFound {
		if isok(app) {
			foundNode.App = app
		}

		if isok(version) {
			foundNode.Version = version
		}
	}

	if nodeType == NodeTypeWorkload && wk != nil {

		// set has istio sidecar
		foundNode.HasIstioSideCar = &hasIstioSideCar
	}

	return foundNode, isFound
}

func hasCircuitBreaker(serviceName string, drs []v1alpha3.DestinationRule) bool {
	if drs == nil || len(drs) < 1 || serviceName == "" {
		logger.Debugf("hasCircuitBreaker false, false,serviceName is %s ", serviceName)
		return false
	}
	for _, dr := range drs {
		if serviceName == dr.Host {
			if destinationrule.HasCircuitBreaker(&dr) {
				logger.Debugf("hasCircuitBreaker true true,serviceName is %s \n", serviceName)
				return true
			}
		}
	}
	logger.Debugf("hasCircuitBreaker nothing, nothing,serviceName is %s ", serviceName)
	return false
}

func hasPolicy(serviceName string, drs []v1alpha3.DestinationRule) bool {
	if drs == nil || len(drs) < 1 || serviceName == "" {
		logger.Debugf("hasPolicy false, false,serviceName is %s ", serviceName)
		return false
	}
	for _, dr := range drs {
		if serviceName == dr.Host {
			if destinationrule.HasPolicy(&dr) {
				logger.Debugf("hasPolicy true true,serviceName is %s \n", serviceName)
				return true
			}
		}
	}
	logger.Debugf("hasPolicy nothing, nothing,serviceName is %s ", serviceName)
	return false
}

func (g *Graph) addEdge(sourceId, targetId uint32, code string, val float64) *Edge {
	for _, e := range g.Edges {
		if e.SourceId == sourceId && e.TargetId == targetId {

			return e
		}
	}

	// new edges
	metadata := make(Metadata)
	e := &Edge{SourceId: sourceId, TargetId: targetId, Metadata: metadata}

	g.Edges = append(g.Edges, e)

	return e
}

// Node that is a source of an edge and not a target of any edge is a root.
func (g *Graph) setRootNodes() {
	for _, n := range g.Nodes {
		n.handleUnknownApp()
		n.handleSvcList()
		n.hasCircuitBreaker(g.Bussiness.DestinationRules)
		n.handleBelongTo(g.Bussiness.Microservices)
		source := false
		target := false
		for _, e := range g.Edges {
			if e.SourceId == n.Id {
				source = true
			}
			if e.TargetId == n.Id && e.SourceId != e.TargetId {
				target = true
			}
		}
		if source && !target {
			n.IsRoot = true
		}
	}
}

func (g *Graph) sortNodes() {

	sort.Slice(g.Nodes, func(i, j int) bool {

		if g.Nodes[i].IsRoot {
			return true
		}
		if g.Nodes[j].IsRoot {
			return false
		}
		return g.Nodes[i].Id < g.Nodes[j].Id
	})
}

func handleMissLabels(node *Node, app, version string) {

	//fmt.Println(fmt.Sprintf("nodeApp %s,%s", node.App, app))
	if node.App != app {
		node.App = app
	}

	if node.Version != version {
		node.Version = version
	}
}

// GetNodeGraph returns the graphs base on selected node's metrics
func GetNodeGraph(ctx context.Context, opts *models.QueryOptions, k8sclient client.Interface, promClient *prometheus.Client, namespace, workload, app, version, service, selectedNamespace string, startTime, endTime int, injectServiceNodes bool, clusterName string) (*Graph, error) {
	nsQuery := common.NewSameNamespaceQuery(selectedNamespace)
	workloadsChan := make(chan Workloads)
	errChan := make(chan error)

	// 这里是启动协程去获取workloads,跟主线程获取prometheus的数据并行处理，
	go getWorkloads(k8sclient, nsQuery, workloadsChan, errChan)

	metrics, err := promClient.GetNodeTraffic(namespace, workload, service, startTime, endTime, selectedNamespace)
	if err != nil {
		return nil, err
	}

	workloads := <-workloadsChan

	wk, _ := workloads.FindWorkload(workload)

	graph := Graph{Namespace: selectedNamespace, StartTime: startTime, EndTime: endTime, InjectServiceNodes: injectServiceNodes}
	// get destinationRules by namespaces
	dyClientDR, err := destinationrule.DRClient(ctx, opts)
	if err != nil {
		logger.Debugf("destinationrule.DRClient err is %+v \n", err)
		return nil, err
	}

	drs, err := destinationrule.GetDestinationRuleListWithNS(dyClientDR, graph.Namespace)
	if err != nil {
		logger.Debugf("destinationrule.getDestinationRuleListWithNS err is %+v \n ", err)
		return nil, err
	}

	microservices, err := FetchMicroservicesByNamespace(ctx, opts, graph.Namespace)
	if err != nil {
		logger.Errorf("FetchMicroservicesByNamespace err occurs,err is %+v \n", err)
		return nil, err
	}

	graph.Bussiness = BussinessModels{Workloads: workloads, DestinationRules: drs, Microservices: microservices}
	logger.Debugf("graph bussiness models  %+v \n", graph.Bussiness)

	graph.NewNode(namespace, workload, app, version, service, wk)

	return populateGraph(&graph, &metrics), nil
}

func getWorkloads(k8sclient client.Interface, nsQuery *common.NamespaceQuery, workloadsChan chan Workloads, errChan chan error) {
	workloads, err := wd.FetchWorkloads(k8sclient, nsQuery, "")
	if err != nil {
		logger.Errorf("fetching workloads error, namespace %s: %s", nsQuery.ToRequestParam(), err)
		errChan <- err
	}
	workloadsChan <- workloads
}

func (g *Graph) NewNode(ns, wlk, app, version, service string, wk *wd.Workload) *Node {

	id, nodeType := ID(wlk, ns, app, version, service)

	node, _ := g.addNode(id, wlk, ns, app, version, service, nodeType, wk)

	return node

}

// isok just validates that a  label value is not empty or unknown
func isok(labelVal string) bool {
	return labelVal != "" && labelVal != Unknown
}
