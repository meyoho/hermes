package servicegraph

import wk "bitbucket.org/mathildetech/hermes/pkg/workload"

type Workloads []*wk.Workload

func (ws *Workloads) findWorkload(ns, wlNs, wl, nodeType string) (isValided bool, wkl *wk.Workload) {
	isValided = true
	var wk *wk.Workload
	//fmt.Printf("ws: %v /n", ws)

	// serviceNode no workload but valided for graph
	if nodeType == NodeTypeService {
		return true, wk
	}

	// 只处理当前ns的workload,不在当前ns的节点认为是有效节点
	if ns == wlNs && IsValideIstioLabel(wl) {
		wkl, wlfind := ws.FindWorkload(wl)
		if !wlfind {
			//fmt.Printf("can't find workload %s /n", wl)
			isValided = false
		} else {
			wk = wkl
		}

	}

	return isValided, wk

}

func (ws Workloads) FindWorkload(workloadName string) (*wk.Workload, bool) {

	if workloadName == "" || workloadName == Unknown {
		return nil, false
	}

	for _, wk := range ws {
		//fmt.Println(fmt.Sprintf(" %s,%s ", wk.Name, workloadName))
		if wk.Name == workloadName {
			return wk, true
		}
	}
	return nil, false

}
