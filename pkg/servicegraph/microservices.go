package servicegraph

import (
	"context"

	"bitbucket.org/mathildetech/hermes/pkg/microservice"
	"bitbucket.org/mathildetech/hermes/pkg/models"

	"github.com/pkg/errors"
)

type Microservices []microservice.MicroService

// FetchMicroservicesByNamespace return the microservices by namespaces.
func FetchMicroservicesByNamespace(ctx context.Context, opts *models.QueryOptions, ns string) (microServices []microservice.MicroService, err error) {
	microServiceDyClient, err := opts.GetDynamicClient(&microservice.GVK)
	if err != nil {
		return nil, err
	}

	microServices, err = microservice.GetMicroServiceResourceList(microServiceDyClient, ns)

	return
}

// GetMicroserviceByDeploymentName return the microservice which deployment belonged to.
func (microServices Microservices) GetMicroserviceByDeploymentName(deployment string) (microService microservice.MicroService, err error) {

	if len(microServices) > 0 {
		for _, ms := range microServices {
			if len(ms.Spec.Deployments) > 0 {
				for _, deploy := range ms.Spec.Deployments {
					if deploy.Name == deployment {
						return ms, nil
					}
				}
			}
		}
	}
	return microService, errors.Errorf("microservice matched with deployment %s was not found", deployment)
}

// GetMicroserviceByServiceName return the microservice which service belonged to.
func (microServices Microservices) GetMicroserviceByServiceName(service string) (microService microservice.MicroService, err error) {

	if len(microServices) > 0 {
		for _, ms := range microServices {
			if len(ms.Spec.Deployments) > 0 {
				for _, svc := range ms.Spec.Services {
					if svc.Name == service {
						return ms, nil
					}
				}
			}
		}
	}
	return microService, errors.Errorf("microservice matched with service %s was not found", service)
}
