package prometheus

import (
	"fmt"
	"time"

	v1 "github.com/prometheus/client_golang/api/prometheus/v1"
	"github.com/prometheus/common/model"
)

func (c *Client) GetPodRequestCountIn(pod, namespace string, startTime, endTime int) (model.Vector, error) {
	query := fmt.Sprintf(`sum(increase(%s{reporter="source",source_workload_namespace="%s",destination_workload_namespace="%s",destination_pod="%s"} [%vs])) by (response_code)`,
		ISTIO_REQUEST_TOTAL,
		namespace,
		namespace,
		pod,
		endTime-startTime)
	return c.Query(query, time.Unix(int64(endTime), 0))
}

func (c *Client) GetPodRequestCountOut(pod, namespace string, startTime, endTime int, destionation_service_label string) (model.Vector, error) {
	query := fmt.Sprintf(`sum(increase(%s{reporter="source",source_workload_namespace="%s",source_pod="%s" %s} [%vs])) by (response_code)`,
		ISTIO_REQUEST_TOTAL,
		namespace,
		pod,
		destionation_service_label,
		endTime-startTime)
	return c.Query(query, time.Unix(int64(endTime), 0))
}

func (c *Client) GetPodRequestRateIn(pod, namespace string, startTime, endTime, step int) (model.Matrix, error) {
	query := fmt.Sprintf(`sum(rate(%s{reporter="source",source_workload_namespace="%s",destination_workload_namespace="%s",destination_pod="%s"} [%vs]))`,
		ISTIO_REQUEST_TOTAL,
		namespace,
		namespace,
		pod,
		step)
	return c.QueryRange(query, time.Unix(int64(startTime), 0), time.Unix(int64(endTime), 0), step)
}

func (c *Client) GetPodAVGRequestRateIn(pod, namespace string, startTime, endTime, step int) (model.Vector, error) {
	query := fmt.Sprintf(`avg(rate(%s{reporter="source",source_workload_namespace="%s",destination_workload_namespace="%s",destination_pod="%s"} [%vs]))`,
		ISTIO_REQUEST_TOTAL,
		namespace,
		namespace,
		pod,
		endTime-startTime)
	return c.Query(query, time.Unix(int64(endTime), 0))
}

func (c *Client) GetPodRequestRateOut(pod, namespace string, startTime, endTime, step int, destionation_service_label string) (model.Matrix, error) {
	query := fmt.Sprintf(`sum(rate(%s{reporter="source",source_workload_namespace="%s",source_pod="%s" %s} [%vs]))`,
		ISTIO_REQUEST_TOTAL,
		namespace,
		pod,
		destionation_service_label,
		step)
	return c.QueryRange(query, time.Unix(int64(startTime), 0), time.Unix(int64(endTime), 0), step)
}

func (c *Client) GetPodAVGRequestRateOut(pod, namespace string, startTime, endTime, step int, destionation_service_label string) (model.Vector, error) {
	query := fmt.Sprintf(`avg(rate(%s{reporter="source",source_workload_namespace="%s",source_pod="%s" %s} [%vs]))`,
		ISTIO_REQUEST_TOTAL,
		namespace,
		pod,
		destionation_service_label,
		endTime-startTime)
	return c.Query(query, time.Unix(int64(endTime), 0))
}

func (c *Client) GetPodErrorRateIn(pod, namespace string, startTime, endTime, step int) (model.Matrix, error) {
	query := fmt.Sprintf(`sum(rate(%s{reporter="source",source_workload_namespace="%s",destination_workload_namespace="%s",destination_pod="%s",response_code=~"[5|4].*"} [%vs]))`,
		ISTIO_REQUEST_TOTAL,
		namespace,
		namespace,
		pod,
		step)
	return c.QueryRange(query, time.Unix(int64(startTime), 0), time.Unix(int64(endTime), 0), step)
}

func (c *Client) GetPodAVGResponseTime(pod, namespace string, startTime, endTime int) (model.Vector, error) {
	labels := fmt.Sprintf(`{reporter="source",source_workload_namespace="%s",destination_workload_namespace="%s",destination_pod="%s"}`,
		namespace,
		namespace,
		pod)
	query := fmt.Sprintf(`sum(rate(%s_sum%s[%vs]))/ sum(rate(%s_count%s[%vs]))`,
		ISTIO_RESPONSE_TIME,
		labels,
		endTime-startTime,
		ISTIO_RESPONSE_TIME,
		labels,
		endTime-startTime)
	return c.Query(query, time.Unix(int64(endTime), 0))
}

func (c *Client) GetPodErrorRateOut(pod, namespace string, startTime, endTime, step int, destionation_service_label string) (model.Matrix, error) {
	query := fmt.Sprintf(`sum(rate(%s{reporter="source",source_workload_namespace="%s",source_pod="%s",response_code=~"[5|4].*" %s} [%vs]))`,
		ISTIO_REQUEST_TOTAL,
		namespace,
		pod,
		destionation_service_label,
		step)
	return c.QueryRange(query, time.Unix(int64(startTime), 0), time.Unix(int64(endTime), 0), step)
}

func (c *Client) GetPodRequestResponseTime(pod, namespace string, startTime, endTime, step int) map[string]model.Matrix {
	params := IstioMetricsQuery{
		Namespace: namespace,
		Pod:       pod,
		Direction: "inbound",
		Reporter:  "source",
		BaseMetricsQuery: BaseMetricsQuery{
			Range: v1.Range{
				End:   time.Unix(int64(endTime), 0),
				Start: time.Unix(int64(startTime), 0),
				Step:  time.Duration(step) * time.Second,
			},
			Quantiles:    []string{"0.5", "0.95", "0.99"},
			Avg:          true,
			ByLabels:     []string{""},
			RateInterval: fmt.Sprintf("%ds", step),
			RateFunc:     "rate",
		},
	}

	return getHistograms(c.Api, &params)

}
