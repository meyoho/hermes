package config

import "os"

const (
	istioNamespace                 = "istio-system"
	defaultAlaudaNamespace         = "cpaas-system"
	defaultServiceMeshChartVersion = "v3.0"
)

const (
	alaudaNamespaceEnv         = "ASM_ALAUDA_NAMESPACE"
	serviceMeshChartVersionEnv = "SERVICEMESH_CHART_VERSION"
)

func AlaudaNamespace() string {
	if ns := os.Getenv(alaudaNamespaceEnv); ns != "" {
		return ns
	}
	return defaultAlaudaNamespace
}

func ServiceMeshChartVersionEnv() string {
	if ns := os.Getenv(serviceMeshChartVersionEnv); ns != "" {
		return ns
	}
	return defaultServiceMeshChartVersion
}

func IstioNamespace() string {
	return istioNamespace
}

func IsIstioNamespace(namespace string) bool {

	return namespace == istioNamespace

}
