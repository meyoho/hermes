package config

import (
	"bitbucket.org/mathildetech/hermes/pkg/logging"
	"os"
	"strconv"
	"strings"
)

var logger = logging.RegisterScope("config")

// Global configuration for the application.
var configuration *Config

// Server configuration
type Server struct {
	Address                    string `yaml:",omitempty"`
	Port                       int    `yaml:",omitempty"`
	WebRoot                    string `yaml:"web_root,omitempty"`
	StaticContentRootDirectory string `yaml:"static_content_root_directory,omitempty"`
	CORSAllowAll               bool   `yaml:"cors_allow_all,omitempty"`
}

// Config defines full YAML configuration.
type Config struct {
	Server Server `yaml:",omitempty"`
}

// Get the global Config
func Get() (conf *Config) {
	return configuration
}

// Set the global Config
func Set(conf *Config) {
	configuration = conf
}

func getDefaultString(envvar string, defaultValue string) (retVal string) {
	retVal = os.Getenv(envvar)
	if retVal == "" {
		retVal = defaultValue
	}
	return
}

func getDefaultStringArray(envvar string, defaultValue string) (retVal []string) {
	csv := os.Getenv(envvar)
	if csv == "" {
		csv = defaultValue
	}
	retVal = strings.Split(csv, ",")
	return
}

func getDefaultInt(envvar string, defaultValue int) (retVal int) {
	retValString := os.Getenv(envvar)
	if retValString == "" {
		retVal = defaultValue
	} else {
		if num, err := strconv.Atoi(retValString); err != nil {
			logger.Warnf("Invalid number for envvar [%v]. err=%v", envvar, err)
			retVal = defaultValue
		} else {
			retVal = num
		}
	}
	return
}

func getDefaultInt64(envvar string, defaultValue int64) (retVal int64) {
	retValString := os.Getenv(envvar)
	if retValString == "" {
		retVal = defaultValue
	} else {
		if num, err := strconv.ParseInt(retValString, 10, 64); err != nil {
			logger.Warnf("Invalid number for envvar [%v]. err=%v", envvar, err)
			retVal = defaultValue
		} else {
			retVal = num
		}
	}
	return
}

func getDefaultBool(envvar string, defaultValue bool) (retVal bool) {
	retValString := os.Getenv(envvar)
	if retValString == "" {
		retVal = defaultValue
	} else {
		if b, err := strconv.ParseBool(retValString); err != nil {
			logger.Warnf("Invalid boolean for envvar [%v]. err=%v", envvar, err)
			retVal = defaultValue
		} else {
			retVal = b
		}
	}
	return
}

func getDefaultFloat32(envvar string, defaultValue float32) (retVal float32) {
	retValString := os.Getenv(envvar)
	if retValString == "" {
		retVal = defaultValue
	} else {
		if f, err := strconv.ParseFloat(retValString, 32); err != nil {
			logger.Warnf("Invalid float number for envvar [%v]. err=%v", envvar, err)
			retVal = defaultValue
		} else {
			retVal = float32(f)
		}
	}
	return
}
