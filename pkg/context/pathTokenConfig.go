package context

import (
	"context"

	"k8s.io/client-go/rest"
)

// Context simple pointer to context.Context interface
type Context context.Context

type contextKey struct{ Name string }

const (
	PathTokenContextKey = "PathTokenContext"
)

// WithPathTokenConfig inserts a config into the context
func WithPathTokenConfig(ctx context.Context, config *rest.Config) context.Context {
	return context.WithValue(ctx, PathTokenContextKey, config)
}

// PathTokenConfig fetches a config from a context if existing.
// will return nil if the context doesnot have the client
func PathTokenConfig(ctx context.Context) *rest.Config {
	val := ctx.Value(PathTokenContextKey)
	if val != nil {
		return val.(*rest.Config)
	}
	return nil
}
