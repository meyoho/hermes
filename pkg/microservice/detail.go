package microservice

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/network"
	"github.com/pkg/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"strings"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/dynamic"
	client "k8s.io/client-go/kubernetes"
)

type Workload struct {
	Name    string `json:"name"`
	Version string `json:"version,omitempty"`
}

type ServiceItem struct {
	Name             string `json:"name"`
	IsCreateBySystem bool   `json:"iscreatebysystem"`
}

// MicroServiceSpec defines the desired state of MicroService
type MicroServiceSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Deployments  []Workload    `json:"deployments,omitempty"`
	Statefulsets []Workload    `json:"statefulsets,omitempty"`
	Daemonsets   []Workload    `json:"daemonsets,omitempty"`
	Services     []ServiceItem `json:"services,omitempty"`
}

type MicroServiceRelation struct {
	Deployment appsv1.Deployment `json:"deployment,omitempty"`
	Services   []ServiceItem     `json:"services,omitempty"`
}

// MicroServiceStatus defines the observed state of MicroService
type MicroServiceStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

// +genclient
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// MicroService is the Schema for the microservices API
// +k8s:openapi-gen=true
type MicroService struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   MicroServiceSpec   `json:"spec,omitempty"`
	Status MicroServiceStatus `json:"status,omitempty"`
}

func setServiceTypeMeta(svc *corev1.Service) {
	svc.TypeMeta.SetGroupVersionKind(schema.GroupVersionKind{
		Group:   "",
		Version: "v1",
		Kind:    "Service",
	})
}

func setDeploymentTypeMeta(deploy *appsv1.Deployment) {
	deploy.TypeMeta.SetGroupVersionKind(schema.GroupVersionKind{
		Group:   "apps",
		Version: "v1",
		Kind:    "Deployment",
	})
}
func deleteDeploymentUselessData(deploy *appsv1.Deployment) {
	logger.Debugf("deleteDeploymentUselessData begin \n")
	delete(deploy.Labels, "app.alauda.io/uuid")
	deploy.ObjectMeta.Generation = 0
	deploy.ObjectMeta.ResourceVersion = ""
	deploy.ObjectMeta.SelfLink = ""
	deploy.ObjectMeta.UID = ""
	deploy.Status = appsv1.DeploymentStatus{}
	logger.Debugf("deleteDeploymentUselessData end \n")
}

func GetMicroServiceResource(dyclient dynamic.NamespaceableResourceInterface, namespace, msname string) (*MicroService, error) {
	unstruct, err := dyclient.Namespace(namespace).Get(msname, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	microService, err := GetMicroServiceFromUnstructured(unstruct)
	if err != nil {
		return nil, err
	}

	return microService, nil
}

func GetMicroServiceResourceList(dyclient dynamic.NamespaceableResourceInterface, namespace string) ([]MicroService, error) {
	//unstruct, err := dyclient.Namespace(namespace).List(metav1.ListOptions{})
	unstructs, err := dyclient.Namespace(namespace).List(common.ListEverything)
	if err != nil {
		return nil, err
	}
	var microServices []MicroService
	if nil != unstructs && len(unstructs.Items) > 0 {
		for _, msUnstruct := range unstructs.Items {
			microService, err := GetMicroServiceFromUnstructured(&msUnstruct)
			if err != nil {
				return nil, err
			}
			microServices = append(microServices, *microService)
		}
	}

	return microServices, nil
}

func CreateMicroServiceSvc(k8sclient client.Interface, dyclient dynamic.NamespaceableResourceInterface, namespace, msname string, spec *corev1.Service) (*corev1.Service, error) {
	//get ms resource
	microService, err := GetMicroServiceResource(dyclient, namespace, msname)
	if err != nil {
		return nil, err
	}

	svcExist := false
	for _, svcitem := range microService.Spec.Services {
		if svcitem.Name == spec.ObjectMeta.Name {
			svcExist = true
			break
		}
	}

	//if already exist , return already exist error
	if svcExist {
		return nil, errors.New("the svc name is same in microservice")
	}

	//create svc first, then add to crd, can not add crd first while controller will delete svc if not exist
	service, err := k8sclient.CoreV1().Services(namespace).Create(spec)
	if err != nil {
		return nil, err
	}
	setServiceTypeMeta(service)

	//controller will watch svc ,and set them deployment to the svc selector label
	svcitem := ServiceItem{
		Name:             service.ObjectMeta.Name,
		IsCreateBySystem: true,
	}
	microService.Spec.Services = append(microService.Spec.Services, svcitem)
	msus, err := GetUnstructuredFromMicroService(microService)
	if err != nil {
		//if failed del svc
		_ = k8sclient.CoreV1().Services(namespace).Delete(spec.ObjectMeta.Name, &metav1.DeleteOptions{})
		return nil, err
	}

	_, err = dyclient.Namespace(namespace).Update(msus, metav1.UpdateOptions{})
	if err != nil {
		//if failed ,del svc
		_ = k8sclient.CoreV1().Services(namespace).Delete(spec.ObjectMeta.Name, &metav1.DeleteOptions{})
		return nil, err
	}

	return service, nil
}

func DeleteDeploymentSelectorLabel(deployment *appsv1.Deployment, labelmap map[string]string, k8sclient client.Interface) error {
	//deployment exist ,go on to deal
	modified := false
	//label may be nil
	if deployment.Spec.Template.ObjectMeta.Labels == nil {
		return nil
	}

	for k := range labelmap {
		if _, ok := deployment.Spec.Template.ObjectMeta.Labels[k]; ok {
			delete(deployment.Spec.Template.ObjectMeta.Labels, k)
			modified = true
		}
	}

	if modified {
		_, err := k8sclient.AppsV1().Deployments(deployment.ObjectMeta.Namespace).Update(deployment)
		if err != nil {
			return errors.WithStack(err)
		}
	}
	return nil
}

func DeleteOldServiceLabelWithDeployments(namespace, svcname string, k8sclient client.Interface, spec *corev1.Service) error {
	service, err := k8sclient.CoreV1().Services(namespace).Get(svcname, metav1.GetOptions{})
	if err != nil {
		//get no deploy then do continue
		return err
	}
	setServiceTypeMeta(service)

	existdeploymentNames := spec.ObjectMeta.Annotations[MicroServiceDeploymentAnnotation]
	existDeployMap := make(map[string]bool)
	if existdeploymentNames != "" {
		existdeploymentNameList := strings.Split(existdeploymentNames, ",")
		for _, deploymentName := range existdeploymentNameList {
			existDeployMap[deploymentName] = true
		}
	}

	deploymentNames := service.ObjectMeta.Annotations[MicroServiceDeploymentAnnotation]
	if deploymentNames != "" {
		deploymentNameList := strings.Split(deploymentNames, ",")
		for _, deploymentName := range deploymentNameList {
			//if dep in the exist map ,do not delete
			if _, ok := existDeployMap[deploymentName]; ok {
				continue
			}
			deployment, err := k8sclient.AppsV1().Deployments(namespace).Get(deploymentName, metav1.GetOptions{})
			if err != nil {
				//get no deploy then do continue
				continue
			}
			setDeploymentTypeMeta(deployment)

			err = DeleteDeploymentSelectorLabel(deployment, service.Spec.Selector, k8sclient)
			if err != nil {
				return errors.WithStack(err)
			}
		}
	}
	return nil
}

func SetDeploymentSelectorLabel(deployment *appsv1.Deployment, labelmap map[string]string, k8sclient client.Interface) error {

	//deployment exist ,go on to deal
	modified := false
	//label may be nil
	if deployment.Spec.Template.ObjectMeta.Labels == nil {
		deployment.Spec.Template.ObjectMeta.Labels = make(map[string]string)
		for k, v := range labelmap {
			deployment.Spec.Template.ObjectMeta.Labels[k] = v
		}
		modified = true
	} else {
		for k, v := range labelmap {
			if deployment.Spec.Template.ObjectMeta.Labels[k] != v {
				deployment.Spec.Template.ObjectMeta.Labels[k] = v
				modified = true
			}
		}
	}

	if modified {
		_, err := k8sclient.AppsV1().Deployments(deployment.ObjectMeta.Namespace).Update(deployment)
		if err != nil {
			return errors.WithStack(err)
		}
	}
	return nil
}

//set every deployment label by service
func SetDeploymentSelectorLabelByAsmCreateService(service *corev1.Service, k8sclient client.Interface) error {
	deploymentNames := service.ObjectMeta.Annotations[MicroServiceDeploymentAnnotation]
	if deploymentNames != "" {
		deploymentNameList := strings.Split(deploymentNames, ",")
		for _, deploymentName := range deploymentNameList {
			deployment, err := k8sclient.AppsV1().Deployments(service.ObjectMeta.Namespace).Get(deploymentName, metav1.GetOptions{})
			if err != nil {
				//get no deploy then do continue
				continue
			}
			setDeploymentTypeMeta(deployment)
			err = SetDeploymentSelectorLabel(deployment, service.Spec.Selector, k8sclient)
			if err != nil {
				return errors.WithStack(err)
			}
		}
	}

	return nil
}

func UpdateMicroServiceSvc(k8sclient client.Interface, namespace, msname, svcname string, spec *corev1.Service) (*corev1.Service, error) {
	//delete old resource
	err := DeleteOldServiceLabelWithDeployments(namespace, svcname, k8sclient, spec)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	err = SetDeploymentSelectorLabelByAsmCreateService(spec, k8sclient)
	if err != nil {
		return nil, err
	}
	//controller will watch svc ,and set the new deployment to the svc selector label

	//create svc first, then add to crd, can not add crd first while controller will delete svc if not exist
	service, err := k8sclient.CoreV1().Services(namespace).Update(spec)
	if err != nil {
		return nil, err
	}
	setServiceTypeMeta(service)
	return service, nil
}

func filterService(originsvc *corev1.ServiceList, msname string) *corev1.ServiceList {
	svclist := &corev1.ServiceList{}
	for _, svc := range originsvc.Items {
		if _, ok := svc.ObjectMeta.Labels[MicroServiceCreatorLabel]; ok {
			if labelval, ok := svc.ObjectMeta.Labels[MicroServiceNameLabel]; ok {
				if labelval != msname {
					//if the svc is create by asm, and not the same msname ,then can not add to
					continue
				}
			}
		}
		svclist.Items = append(svclist.Items, svc)
	}

	return svclist
}

func getDeploymentRelationServices(deployments []appsv1.Deployment, svcs []corev1.Service,
	namespace string) ([]MicroServiceRelation, error) {

	deploySvcRelations := make([]MicroServiceRelation, 0)
	for _, deployment := range deployments {
		//analyse svc and deployment
		netWorkResources, err := network.GetNetworkResources(deployment.Spec.Template.Spec.Containers, svcs, namespace, deployment.Spec.Template.ObjectMeta.Labels)
		if err != nil {
			return nil, err
		}
		logger.Debugf("get netWorkResources  is %+v ", netWorkResources)
		svcNamelist := make([]ServiceItem, 0, len(netWorkResources))
		svcNameMap := make(map[string]bool)
		for _, d := range netWorkResources {
			isCreateBySystem := false
			if d.GetLabels() != nil {
				if d.GetLabels()[MicroServiceCreatorLabel] == ASMCREATERESOURCE {
					isCreateBySystem = true
				}
			}
			svcNameMap[d.GetName()] = isCreateBySystem
		}
		for k, v := range svcNameMap {
			svcitem := ServiceItem{
				Name:             k,
				IsCreateBySystem: v,
			}
			svcNamelist = append(svcNamelist, svcitem)
		}
		microServiceRelation := MicroServiceRelation{
			Deployment: deployment,
			Services:   svcNamelist,
		}
		deploySvcRelations = append(deploySvcRelations, microServiceRelation)
	}

	return deploySvcRelations, nil
}

func GetNamespaceDeploymentServiceRelationDetail(k8sclient client.Interface, namespace string) ([]MicroServiceRelation, error) {

	svclist, err := k8sclient.CoreV1().Services(namespace).List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	for x := range svclist.Items {
		setServiceTypeMeta(&(svclist.Items[x]))
	}
	deploymentlist, err := k8sclient.AppsV1().Deployments(namespace).List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	for x := range deploymentlist.Items {
		setDeploymentTypeMeta(&(deploymentlist.Items[x]))
	}
	return getDeploymentRelationServices(deploymentlist.Items, svclist.Items, namespace)
}

func GetServiceRelationDeploymentDetail(k8sclient client.Interface, name, namespace string) ([]MicroServiceRelation,
	error) {

	svc, err := k8sclient.CoreV1().Services(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	setServiceTypeMeta(svc)

	var svcs []corev1.Service
	svcs = append(svcs, *svc)

	deploymentlist, err := k8sclient.AppsV1().Deployments(namespace).List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	for x := range deploymentlist.Items {
		setDeploymentTypeMeta(&(deploymentlist.Items[x]))
	}
	relations, err := getDeploymentRelationServices(deploymentlist.Items, svcs, namespace)
	if err != nil {
		return nil, err
	}
	var filterRelations []MicroServiceRelation
	for _, relation := range relations {
		if len(relation.Services) > 0 {
			filterRelations = append(filterRelations, relation)
		}
	}
	return filterRelations, nil
}

func GetMicroServiceRelationDetail(k8sclient client.Interface, dyclient dynamic.NamespaceableResourceInterface,
	dsQuery *dataselect.Query, namespace string, name string) ([]MicroServiceRelation, error) {
	//get ms resource
	microService, err := GetMicroServiceResource(dyclient, namespace, name)
	if err != nil {
		return nil, err
	}

	//get all svc  or get only labeled svc
	listOptions := common.ConvertToListOptions(dsQuery)
	var svclist *corev1.ServiceList
	if listOptions.LabelSelector != "" {
		svclist = &corev1.ServiceList{}
		for _, msSvc := range microService.Spec.Services {
			svc, err := k8sclient.CoreV1().Services(namespace).Get(msSvc.Name, metav1.GetOptions{})
			if err != nil {
				return nil, err
			}
			svclist.Items = append(svclist.Items, *svc)
		}
	} else {
		svclist, err = k8sclient.CoreV1().Services(namespace).List(metav1.ListOptions{})
		if err != nil {
			return nil, err
		}
	}

	filtersvclist := filterService(svclist, name)

	if filtersvclist != nil && filtersvclist.Items != nil {
		for x := range filtersvclist.Items {
			setServiceTypeMeta(&(filtersvclist.Items[x]))
		}
	}

	var deployments []appsv1.Deployment
	for _, msDeployment := range microService.Spec.Deployments {
		//analyse svc and deployment
		deployment, err := k8sclient.AppsV1().Deployments(namespace).Get(msDeployment.Name, metav1.GetOptions{})
		if err != nil {
			//get no deploy then do continue
			continue
		}
		deployments = append(deployments, *deployment)
		logger.Debugf("get deployment  is %s ", msDeployment.Name)
	}

	return getDeploymentRelationServices(deployments, filtersvclist.Items, namespace)
}

func CreateMicroServiceDeployment(k8sclient client.Interface, dyclient dynamic.NamespaceableResourceInterface, namespace, msname string, deployment *appsv1.Deployment) (*appsv1.Deployment, error) {
	//get ms resource
	logger.Debugf("createMicroServiceDeployment begin \n")
	microService, err := GetMicroServiceResource(dyclient, namespace, msname)
	if err != nil {
		logger.Debugf("getMicroServiceResource err erris %v \n", err)
		return nil, err
	}
	logger.Debugf("createMicroServiceDeployment.GetMicroServiceResource is %+v \n", microService)

	deployExist := false
	deployVersionExist := false
	for _, deploy := range microService.Spec.Deployments {
		if deploy.Name == deployment.ObjectMeta.Name {
			deployExist = true
			break
		}
		if deployment.Spec.Template.Labels["version"] == deploy.Version {
			deployVersionExist = true
			break
		}
	}

	//if already exist , return already exist error
	if deployExist {
		logger.Debugf("return already exist error, deployExist= %v \n", deployExist)
		return nil, errors.New("the deployment name is same in microservice")
	}
	//if already exist version , return already exist error version
	if deployVersionExist {
		logger.Debugf("return already exist version error, deployVersionExist= %v \n", deployVersionExist)
		return nil, errors.New("the deployment version is same in microservice")
	}
	deleteDeploymentUselessData(deployment)
	logger.Debugf("deleteDeploymentUselessData deployis %+v \n", deployment)

	//create deployment first, then add to crd, can not add crd first while controller will delete svc if not exist
	deploy, err := k8sclient.AppsV1().Deployments(namespace).Create(deployment)
	if err != nil {
		return nil, err
	}
	logger.Debugf("Create deploy  %+v \n", deploy)

	setDeploymentTypeMeta(deploy)

	//controller will watch svc ,and set them deployment to the svc selector label
	deployitem := Workload{
		Name:    deploy.ObjectMeta.Name,
		Version: deploy.Spec.Template.Labels["version"],
	}
	microService.Spec.Deployments = append(microService.Spec.Deployments, deployitem)
	msus, err := GetUnstructuredFromMicroService(microService)
	if err != nil {
		logger.Debugf("getUnstructuredFromMicroService err=  %+v \n", err)
		//if failed del svc
		_ = k8sclient.AppsV1().Deployments(namespace).Delete(deployment.ObjectMeta.Name, &metav1.DeleteOptions{})
		return nil, err
	}

	_, err = dyclient.Namespace(namespace).Update(msus, metav1.UpdateOptions{})
	if err != nil {
		logger.Debugf("Update err=  %+v \n", err)

		//if failed ,del svc
		_ = k8sclient.AppsV1().Deployments(namespace).Delete(deployment.ObjectMeta.Name, &metav1.DeleteOptions{})
		return nil, err
	}
	logger.Debugf("createMicroServiceDeployment end \n")

	return deploy, nil
}
