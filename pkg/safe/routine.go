package safe

import (
	"bitbucket.org/mathildetech/hermes/pkg/logging"
	"runtime/debug"
)

// Go starts a recoverable goroutine
func Go(goroutine func()) {
	GoWithRecover(goroutine, defaultRecoverGoroutine)
}

// GoWithRecover starts a recoverable goroutine using given customRecover() function
func GoWithRecover(goroutine func(), customRecover func(err interface{})) {
	go func() {
		defer func() {
			if err := recover(); err != nil {
				customRecover(err)
			}
		}()
		goroutine()
	}()
}

func defaultRecoverGoroutine(err interface{}) {
	logging.Logger.Errorf("Error in Go routine: %s", err)
	logging.Logger.Errorf("Stack: %s", debug.Stack())
}
