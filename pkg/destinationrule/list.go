package destinationrule

import (
	"encoding/json"
	"fmt"

	"github.com/gogo/protobuf/jsonpb"
	istio_v1alpha3 "istio.io/api/networking/v1alpha3"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/dynamic"

	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"

	api "bitbucket.org/mathildetech/hermes/pkg/common"
)

type DestinationRuleDetailList struct {
	ListMeta         api.ListMeta                `json:"listMeta"`
	DestinationRules []unstructured.Unstructured `json:"destinationRules"`
	Errors           []error                     `json:"errors"`
}

// validate Traffic Policy LoadBalancer is nil
func validateTrafficPolicy(destinationRule *istio_v1alpha3.DestinationRule) bool {
	if destinationRule.TrafficPolicy == nil {
		return true
	}
	if destinationRule.TrafficPolicy.LoadBalancer == nil {
		return true
	}
	return false

}

// GetDestinationRuleListWithNS
func GetDestinationRuleListWithNSUnstructured(dyclient dynamic.NamespaceableResourceInterface, namespace string) ([]unstructured.Unstructured, error) {
	obj, err := dyclient.Namespace(namespace).List(api.ListEverything)
	if err != nil {
		logger.Debugf("get DestinationRuleListWithNSUnstructured err is %s", err)
		return nil, err
	}
	if obj != nil && len(obj.Items) > 0 {
		return obj.Items, nil
	}
	return nil, nil
}

// GetDestinationRuleListWithNS
func GetDestinationRuleListWithNS(dyclient dynamic.NamespaceableResourceInterface, namespace string) ([]istio_v1alpha3.DestinationRule, error) {
	items, err := GetDestinationRuleListWithNSUnstructured(dyclient, namespace)
	if err != nil {
		logger.Errorf(" destinationrule err is %+v ", err)
		return nil, err
	}
	// else unstructured type == false,return v1alpha3.DestinationRule struct
	var drs []istio_v1alpha3.DestinationRule
	if items != nil && len(items) > 0 {
		for _, value := range items {
			dr, err := MapToSpec(value.Object["spec"].(map[string]interface{}))
			if err != nil {
				return nil, err
			}
			drs = append(drs, *dr)
		}
	}
	return drs, nil
}

// GetDestinationRuleListWithPolicy
func GetDestinationRuleListWithPolicy(dyclient dynamic.NamespaceableResourceInterface, namespace string, policyType string, dsQuery *dataselect.Query) (*DestinationRuleDetailList, error) {
	obj, err := dyclient.Namespace(namespace).List(api.ListEverything)
	if err != nil {
		return nil, err
	}

	items := obj.Items
	// return, donnot have policy array or have Policy array
	if policyType != DestinationtuleAll && len(items) > 0 {
		var drsWithPolicy []unstructured.Unstructured
		var drsWithoutPolicy []unstructured.Unstructured
		for _, value := range items {
			var data []byte
			if data, err = json.Marshal(value.Object["spec"]); err != nil {
				return nil, err
			}
			dr := &istio_v1alpha3.DestinationRule{}
			if err = jsonpb.UnmarshalString(string(data), dr); err != nil {
				return nil, err
			}
			// set donnot have policy array and have Policy array
			if validateTrafficPolicy(dr) {
				drsWithoutPolicy = append(drsWithoutPolicy, value)
			} else {
				drsWithPolicy = append(drsWithPolicy, value)
			}
		}
		if policyType == DestinationtuleWithPolicy {
			items = drsWithPolicy
		} else if policyType == DestinationtuleWithoutPolicy {
			items = drsWithoutPolicy
		}
	}

	// filter using standard filters
	result, filteredTotal := filterCondition(items, dsQuery)

	list := &DestinationRuleDetailList{
		ListMeta: api.ListMeta{
			TotalItems: filteredTotal,
		},
		DestinationRules: result,
		Errors:           []error{},
	}
	return list, nil
}

func filterCondition(items interface{}, dsQuery *dataselect.Query) ([]unstructured.Unstructured, int) {
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, dsQuery)
	return dataselect.FromCellToUnstructuredSlice(itemCells), filteredTotal
}

func GetDestinationRuleByHostDetail(dyclient dynamic.NamespaceableResourceInterface, namespace string, hostName string) (*[]unstructured.Unstructured, error) {

	listOptions := convertToDetailOptions(hostName)
	obj, err := dyclient.Namespace(namespace).List(listOptions)
	if err != nil {
		return nil, err
	}
	if obj.Items == nil && len(obj.Items) == 0 {
		return nil, fmt.Errorf("%s", hostName)
	}

	return &obj.Items, nil
}

func convertToDetailOptions(hostName string) (ls metav1.ListOptions) {
	ls = api.ListEverything
	if hostName == "" {
		return
	}
	ls.LabelSelector = fmt.Sprintf("%s in (%s)", HostName, hostName)
	return ls
}
