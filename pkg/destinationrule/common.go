package destinationrule

import (
	"context"
	"encoding/json"

	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/logging"
	"bitbucket.org/mathildetech/hermes/pkg/models"
	"github.com/gogo/protobuf/jsonpb"
	"istio.io/api/networking/v1alpha3"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
)

const (
	Group                        = "networking.istio.io"
	Version                      = "v1alpha3"
	Kind                         = "DestinationRule"
	DRKind                       = "destinationrules"
	DestinationtuleAll           = "0"
	DestinationtuleWithPolicy    = "1"
	DestinationtuleWithoutPolicy = "2"
)

var (
	HostName = "asm." + common.GetLocalBaseDomain() + "/hostname"

	logger = logging.RegisterScope("destinationrule")

	GVK = schema.GroupVersionKind{
		Group:   Group,
		Version: Version,
		Kind:    Kind,
	}

	drGVR = schema.GroupVersionResource{
		Group:    Group,
		Version:  Version,
		Resource: DRKind,
	}
)

func DRClient(ctx context.Context, opts *models.QueryOptions) (dynamic.NamespaceableResourceInterface, error) {
	dyClient, err := opts.GetDynamicClient(&GVK)
	if err != nil {
		return nil, err
	}
	return dyClient, nil
}

func MapToSpec(m map[string]interface{}) (*v1alpha3.DestinationRule, error) {
	var data []byte
	var err error

	if data, err = json.Marshal(m); err != nil {
		return nil, err
	}
	destinationrule := &v1alpha3.DestinationRule{}
	if err = jsonpb.UnmarshalString(string(data), destinationrule); err != nil {
		return nil, err
	}

	return destinationrule, nil
}
