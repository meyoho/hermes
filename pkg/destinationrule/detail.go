package destinationrule

import (
	api "bitbucket.org/mathildetech/hermes/pkg/common"
	"istio.io/api/networking/v1alpha3"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/dynamic"
)

func GetDestinationRuleDetail(dyclient dynamic.NamespaceableResourceInterface, namespace string, name string) (*unstructured.Unstructured, error) {
	return dyclient.Namespace(namespace).Get(name, api.GetOptionsInCache)
}

func HasCircuitBreaker(dr *v1alpha3.DestinationRule) bool {
	if dr == nil || dr.TrafficPolicy == nil {
		return false
	}
	if dr.TrafficPolicy.OutlierDetection != nil {
		return true
	}
	return false
}

func HasPolicy(dr *v1alpha3.DestinationRule) bool {
	if dr == nil || dr.TrafficPolicy == nil {
		return false
	}
	if dr.TrafficPolicy.Tls != nil {
		return true
	}
	return false
}
