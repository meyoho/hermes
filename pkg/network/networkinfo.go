package network

import (
	"bitbucket.org/mathildetech/hermes/pkg/common"
	core "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

func GetNetworkResources(cs []core.Container, ss []core.Service, namespace string, resourceSelector map[string]string) ([]unstructured.Unstructured, error) {
	rs := generateDeploymentNetworRelations(&cs, &ss, namespace, resourceSelector)
	matchedResource := make([]unstructured.Unstructured, 0, 2)
	unstrMap := make(map[string]*unstructured.Unstructured)
	for _, s := range ss {
		unstr, err := common.ConvertResourceToUnstructured(&s)
		if err != nil {
			return matchedResource, err
		}
		unstrMap[common.GetKeyOfUnstructured(unstr)] = unstr
	}

	for _, r := range rs {
		serviceKey := common.GenKeyOfUnstructured(common.ResourceKindService, r.ServiceData.Name)
		matchedResource = append(matchedResource, *unstrMap[serviceKey])
	}
	return matchedResource, nil
}
