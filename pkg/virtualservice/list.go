package virtualservice

import (
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/dynamic"

	dataselect "bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"

	"bitbucket.org/mathildetech/hermes/pkg/common"
)

type VirtualServiceDetailList struct {
	ListMeta common.ListMeta             `json:"listMeta"`
	Items    []unstructured.Unstructured `json:"items"`
	Errors   []error                     `json:"errors"`
}

func GetVirtualServiceList(dyclient dynamic.NamespaceableResourceInterface, namespace string, dsQuery *dataselect.Query) (*VirtualServiceDetailList, error) {
	listOptions := dsQuery.ToListOptions()
	obj, err := dyclient.Namespace(namespace).List(listOptions)
	if err != nil {
		return nil, err
	}
	items := obj.Items

	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, dsQuery)
	result := dataselect.FromCellToUnstructuredSlice(itemCells)

	list := &VirtualServiceDetailList{
		ListMeta: common.ListMeta{
			TotalItems: filteredTotal,
		},
		Items:  result,
		Errors: []error{},
	}
	return list, nil
}
