package kubeclient

import (
	"bitbucket.org/mathildetech/hermes/pkg/clusterRegister"
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/config"
	"github.com/pkg/errors"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/rest"
	"sync"
	"time"
)

var once = sync.Once{}

type informerClusterGetter struct {
	sharedInformer  dynamicinformer.DynamicSharedInformerFactory
	clusterInformer informers.GenericInformer
}

func newInformerClusterGetter() (clusterInfoGetter, error) {
	cfg, err := rest.InClusterConfig()
	if err != nil {
		return nil, errors.WithStack(err)
	}
	dyclient, err := dynamic.NewForConfig(cfg)
	if err != nil {
		return nil, errors.WithStack(err)
	}
	g := &informerClusterGetter{}

	g.sharedInformer = dynamicinformer.NewDynamicSharedInformerFactory(dyclient, time.Hour)
	g.clusterInformer = g.sharedInformer.ForResource(clusterRegister.ClusterRegistryGVR)
	g.sharedInformer.Start(nil)
	return g, nil
}

func (g *informerClusterGetter) waitForInformarCacheSync() {
	once.Do(func() {
		ch := make(chan struct{}, 1)

		go func() {
			<-time.After(5 * time.Second)
			ch <- struct{}{}
		}()
		g.sharedInformer.WaitForCacheSync(ch)
	})

}
func (g *informerClusterGetter) get(clusterName string) (*clusterRegister.Cluster, error) {
	g.waitForInformarCacheSync()
	obj, err := g.clusterInformer.Lister().ByNamespace(config.AlaudaNamespace()).Get(clusterName)
	if err != nil {
		return nil, err
	}
	cluster := &clusterRegister.Cluster{}
	if err := common.JsonConvert(obj, cluster); err != nil {
		return nil, err
	}
	return cluster, nil
}

func (g *informerClusterGetter) list() ([]clusterRegister.Cluster, error) {
	g.waitForInformarCacheSync()
	items, err := g.clusterInformer.Lister().ByNamespace(config.AlaudaNamespace()).List(nil)
	if err != nil {
		return nil, err
	}
	result := make([]clusterRegister.Cluster, len(items))
	for i, item := range items {
		cluster := &clusterRegister.Cluster{}
		if err := common.JsonConvert(item, cluster); err != nil {
			logger.Errorf("cluster %s FromUnstructured error  %+v", item, err)
			continue
		}
		result[i] = *cluster
	}
	return nil, nil
}
