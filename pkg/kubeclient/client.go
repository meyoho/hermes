package kubeclient

import (
	"bitbucket.org/mathildetech/hermes/pkg/clusterRegister"
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/logging"
	"errors"
	pkgerrors "github.com/pkg/errors"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

const (
	TokenKey = "token"
)

var Manager *ClientManager
var logger = logging.RegisterScope("kubeclient")

func NewClientManager() (*ClientManager, error) {

	g, err := newInformerClusterGetter()
	if err != nil {
		return nil, err
	}
	m := &ClientManager{
		clusterInfoGetter: g,
	}
	return m, nil
}

type ClientManager struct {
	clusterInfoGetter clusterInfoGetter
}

func (m *ClientManager) InClusterKubeClient() (kubernetes.Interface, error) {
	cfg, err := rest.InClusterConfig()
	if err != nil {
		return nil, pkgerrors.WithStack(err)
	}
	return m.kubeClient(cfg)
}

func (m *ClientManager) kubeClient(cfg *rest.Config) (kubernetes.Interface, error) {
	c, err := kubernetes.NewForConfig(cfg)
	if err != nil {
		return nil, pkgerrors.WithStack(err)
	}
	return c, nil
}

func (m *ClientManager) clusterToConfig(c *clusterRegister.Cluster) (*rest.Config, error) {
	if len(c.Spec.KubernetesAPIEndpoints.ServerEndpoints) == 0 || c.Spec.KubernetesAPIEndpoints.ServerEndpoints[0].ServerAddress == "" {
		return nil, pkgerrors.WithStack(errors.New("could not found an avaliable server endpoint from cluster config."))
	}

	config := &rest.Config{
		Host: c.Spec.KubernetesAPIEndpoints.ServerEndpoints[0].ServerAddress,
	}
	if len(c.Spec.KubernetesAPIEndpoints.CABundle) == 0 {
		config.TLSClientConfig.Insecure = true
	} else {
		config.TLSClientConfig.CAData = c.Spec.KubernetesAPIEndpoints.CABundle
	}

	if c.Spec.AuthInfo.Controller == nil || c.Spec.AuthInfo.Controller.Name == "" ||
		c.Spec.AuthInfo.Controller.Namespace == "" {
		return nil, pkgerrors.WithStack(errors.New("Secret Reference is not provided fully in the cluster config."))
	}

	secret := &corev1.Secret{}
	kubeClient, err := m.InClusterKubeClient()
	if err != nil {
		return nil, err
	}
	secret, err = kubeClient.CoreV1().Secrets(c.Spec.AuthInfo.Controller.Namespace).Get(c.Spec.AuthInfo.Controller.Name, common.GetOptionsInCache)
	if err != nil {
		return nil, err
	}
	token, exist := secret.Data[TokenKey]
	if !exist {
		return nil, pkgerrors.WithStack(errors.New("Auth secret data is invalid, no token field found."))
	}
	config.BearerToken = string(token[:])
	return config, nil
}

func (m *ClientManager) RemoteRestConfig(clusterName string) (*rest.Config, error) {
	c, err := m.clusterInfoGetter.get(clusterName)
	if err != nil {
		return nil, err
	}
	return m.clusterToConfig(c)
}
func (m *ClientManager) ListRemoteRestConfig() (map[string]*rest.Config, error) {
	clusters, err := m.clusterInfoGetter.list()
	if err != nil {
		return nil, err
	}
	result := make(map[string]*rest.Config, len(clusters))
	for _, cluster := range clusters {
		cfg, err := m.clusterToConfig(&cluster)
		if err != nil {
			logger.Errorf("generate rest config from cluster %s failed: %+v\n", cluster.Name, err)
		}
		result[cluster.Name] = cfg
	}
	return result, nil
}

func init() {
	var err error
	if Manager, err = NewClientManager(); err != nil {
		logger.Fatalf("initializa client manager failed: %+v\n", err)
	}
}
