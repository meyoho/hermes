package kubeclient

import (
	"bitbucket.org/mathildetech/hermes/pkg/clusterRegister"
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/config"
	dc "github.com/alauda/cyborg/pkg/client"
	"k8s.io/client-go/dynamic"

	"k8s.io/client-go/rest"
)

type clusterInfoGetter interface {
	get(cluster string) (*clusterRegister.Cluster, error)
	list() ([]clusterRegister.Cluster, error)
}

type simpleClusterInfoGetter struct {
	client dynamic.NamespaceableResourceInterface
}

func newSimpleClusterInfoGetter() (clusterInfoGetter, error) {
	cfg, err := rest.InClusterConfig()
	if err != nil {
		return nil, err
	}
	gv := clusterRegister.ClusterRegistryGVK.GroupVersion()
	cfg.GroupVersion = &gv
	cfg.APIPath = "/apis"

	cyborgClient, err := dc.NewKubeClient(cfg, "default")
	if err != nil || cyborgClient == nil {
		return nil, err
	}

	client, err := cyborgClient.ClientForGVK(clusterRegister.ClusterRegistryGVK)
	if err != nil {
		return nil, err
	}
	g := &simpleClusterInfoGetter{
		client: client,
	}
	return g, nil

}

func (g *simpleClusterInfoGetter) get(clusterName string) (*clusterRegister.Cluster, error) {
	obj, err := g.client.Namespace(config.AlaudaNamespace()).Get(clusterName, common.GetOptionsInCache)
	if err != nil {
		return nil, err
	}
	cluster := &clusterRegister.Cluster{}
	if err := common.JsonConvert(obj, cluster); err != nil {
		return nil, err
	}
	return cluster, nil
}

func (g *simpleClusterInfoGetter) list() ([]clusterRegister.Cluster, error) {
	obj, err := g.client.Namespace(config.AlaudaNamespace()).List(common.ListEverything)
	if err != nil {
		return nil, err
	}
	result := make([]clusterRegister.Cluster, len(obj.Items))
	for i, us := range obj.Items {
		cluster := &clusterRegister.Cluster{}
		if err := common.JsonConvert(&us, cluster); err != nil {
			logger.Errorf("cluster %s FromUnstructured error  %v", us.GetName(), err)
			continue
		}
		result[i] = *cluster
	}
	return result, nil
}
