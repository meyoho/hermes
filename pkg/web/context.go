package web

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	restful "github.com/emicklei/go-restful"
)

// Context is the struct to combine the restful message with our own serviceProvider
type Context struct {
	Server   server.Server
	Request  *restful.Request
	Response *restful.Response
	Query    *dataselect.Query
}
