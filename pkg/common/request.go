package common

import (
	"github.com/emicklei/go-restful"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"strconv"
)

const (
	DefaultLimit = 20
)

func RequestToListOptions(req *restful.Request) (metav1.ListOptions, error) {
	ls := metav1.ListOptions{}
	ls.FieldSelector = req.QueryParameter("fieldSelector")
	ls.LabelSelector = req.QueryParameter("labelSelector")
	ls.Continue = req.QueryParameter("continue")
	limitStr := req.QueryParameter("limit")
	limit := DefaultLimit
	if limitStr != "" {
		var err error
		limit, err = strconv.Atoi(req.QueryParameter("limit"))
		if err != nil {
			return ls, err
		}
	}
	ls.Limit = int64(limit)
	return ls, nil
}
