package common

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"

	"github.com/pkg/errors"

	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/apimachinery/pkg/labels"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

func GetLocalBaseDomain() string {
	if os.Getenv("LABEL_BASE_DOMAIN") != "" {
		return os.Getenv("LABEL_BASE_DOMAIN")
	}
	return "alauda.io"
}

func ConvertToListOptions(dsQuery *dataselect.Query) (ls metav1.ListOptions) {
	ls = metav1.ListOptions{
		LabelSelector:   labels.Everything().String(),
		FieldSelector:   fields.Everything().String(),
		ResourceVersion: "0",
	}
	if dsQuery == nil || dsQuery.FilterQuery == nil {
		return
	}
	for _, r := range dsQuery.FilterQuery.FilterByList {
		switch r.Property {
		case dataselect.LabelProperty:
			str := fmt.Sprintf("%s", r.Value)
			split := strings.Split(str, ":")
			if len(split) > 1 {
				ls.LabelSelector = fmt.Sprintf("%s=%s", split[0], split[1])
			}
		case dataselect.LabelEqualProperty:
			str := fmt.Sprintf("%s", r.Value)
			split := strings.Split(str, ":")
			if len(split) > 1 {
				ls.LabelSelector = fmt.Sprintf("%s==%s", split[0], split[1])
			}
		}
	}
	return
}

func ConvertResourcesToUnstructuredList(resources []interface{}) ([]*unstructured.Unstructured, error) {
	resourceList := make([]*unstructured.Unstructured, 0)
	if resources == nil && len(resources) == 0 {
		return resourceList, nil
	}

	for _, r := range resources {
		if r != nil {
			value, err := ConvertResourceToUnstructured(r)
			if err != nil {
				return resourceList, nil
			}
			resourceList = append(resourceList, value)
		}
	}
	return resourceList, nil
}

func GetKeyOfUnstructured(unstr *unstructured.Unstructured) string {
	return unstr.GetKind() + unstr.GetName()
}

func GenKeyOfUnstructured(kind, name string) string {
	return kind + name
}

func FilterNamespacedServicesBySelector(services []corev1.Service, namespace string,
	resourceSelector map[string]string) []corev1.Service {

	var matchingServices []corev1.Service
	for _, service := range services {
		if service.ObjectMeta.Namespace == namespace &&
			IsSelectorMatching(service.Spec.Selector, resourceSelector) {
			matchingServices = append(matchingServices, service)
		}
	}

	return matchingServices
}

// ConvertResourceToUnstructured func
func ConvertResourceToUnstructured(resource interface{}) (*unstructured.Unstructured, error) {
	unstr := &unstructured.Unstructured{}
	data, err := json.Marshal(resource)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(data, unstr)
	if err != nil {
		return nil, err
	}
	return unstr, nil
}

func JsonConvert(from interface{}, to interface{}) error {
	var data []byte
	var err error
	if data, err = json.Marshal(from); err != nil {
		return errors.WithStack(err)
	}
	return errors.WithStack(json.Unmarshal(data, to))
}

func RandStringRunes(n int) string {
	var letterRunes = []rune("0123456789abcdefghijklmnopqrstuvwxyz")
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
