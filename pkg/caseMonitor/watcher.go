package caseMonitor

import (
	"sort"
	"strings"
	"sync"
	"time"

	"bitbucket.org/mathildetech/hermes/pkg/config"

	pkgerrors "github.com/pkg/errors"

	versionedClient "alauda.io/asm-controller/pkg/client/clientset/versioned"

	asminformers "alauda.io/asm-controller/pkg/client/informers/externalversions"
	asminformersV1beta2 "alauda.io/asm-controller/pkg/client/informers/externalversions/asm/v1beta2"

	asmv1beta2 "alauda.io/asm-controller/pkg/apis/asm/v1beta2"

	"k8s.io/client-go/rest"

	"k8s.io/client-go/tools/cache"

	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/safe"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	refreshLabelKey   = "asm.healthcheck.refresh"
	refreshValueOK    = "true"
	refreshValueNotOk = "false"

	istioPrefix       = "istio"
	asmControllerName = "asm-controller"

	componentNotInstallERR = "NOTINSTALL"
)

var requiredJobs = map[string]string{
	"componenthealthcheck": "elasticsearch,prometheus,asm-controller,grafana,istio-citadel,istio-galley,istio-ingressgateway,istio-pilot,istio-policy,istio-sidecar-injector,istio-telemetry,jaeger-operator,jaeger-prod-collector,jaeger-prod-query",
	"sidecarinjection":     "istio-citadel,istio-sidecar-injector",
	"monitoringservice":    "grafana",
}

var defalutCacheSpan time.Duration = 90 * time.Second // 1.5 minutes

// Watcher for cluster monitor result
type Watcher struct {
	pusher              *Pusher
	client              versionedClient.Interface
	clusterName         string
	stop                chan struct{}
	config              *rest.Config
	caseMonitors        sync.Map
	newSubscriber       chan *Client
	onceClose           sync.Once
	informer            asminformers.SharedInformerFactory
	caseMonitorInformer asminformersV1beta2.CaseMonitorInformer
}

// NewWatcher returns a watcher with crd
func NewWatcher(clusterName string, config *rest.Config) *Watcher {

	logger.Infof("Watcher with config host %s", config.Host)

	clientset, err := versionedClient.NewForConfig(config)
	if err != nil {
		logger.Errorf("Watcher %s initing client failed", clusterName)
		return nil
	}

	asmInformerFactory := asminformers.NewSharedInformerFactory(clientset, time.Second*10)

	caseMonitorInformer := asmInformerFactory.Asm().V1beta2().CaseMonitors()

	w := &Watcher{
		client:              clientset,
		clusterName:         clusterName,
		config:              config,
		stop:                make(chan struct{}),
		newSubscriber:       make(chan *Client),
		caseMonitors:        *new(sync.Map),
		informer:            asmInformerFactory,
		caseMonitorInformer: caseMonitorInformer,
	}

	caseMonitorInformer.Informer().AddEventHandler(cache.ResourceEventHandlerFuncs{
		UpdateFunc: func(old, cur interface{}) {

			oldCase := old.(*asmv1beta2.CaseMonitor)
			curCaseMonitor := cur.(*asmv1beta2.CaseMonitor)

			if w.shouldNotify(oldCase, curCaseMonitor) {

				w.Notify(*curCaseMonitor)
			}
		},
	})

	return w
}

// Start main processor for watcher
func (w *Watcher) Start() {
	logger.Infof("Watcher %s starting", w.clusterName)
	safe.Go(func() {
		w.informer.Start(w.stop)
	})

	for {
		select {
		case <-w.stop:
			logger.Infof("Watcher %s stopping", w.clusterName)
			return
		case client := <-w.newSubscriber:
			w.NotifyNewSubscriber(client)
		}
	}
}

func (w *Watcher) shouldNotify(old, cur *asmv1beta2.CaseMonitor) bool {

	if old.Spec.MonitorResults.LatestUpdated.Equal(&cur.Spec.MonitorResults.LatestUpdated) {
		return false
	}
	return true
}

func (w *Watcher) loadCases() {

	logger.Debugf("Watcher %s loadCases", w.clusterName)

	list, err := w.client.AsmV1beta2().CaseMonitors(config.AlaudaNamespace()).List(common.ListEverything)
	if err != nil {
		logger.Errorf("Watcher start list err : %+v", pkgerrors.WithStack(err))

		w.stop <- struct{}{}
		w.pusher.unwatch <- w.clusterName
		return
	}
	cms := list.Items
	for _, cm := range cms {

		w.caseMonitors.Store(cm.GetName(), cm)
	}
}

// Notify the new message to pusher
func (w *Watcher) Notify(caseMonitor asmv1beta2.CaseMonitor) {

	logger.Debugf("Watcher %s Notify %v", w.clusterName, caseMonitor)

	w.caseMonitors.Store(caseMonitor.GetName(), caseMonitor)

	message := w.parseToMessage(caseMonitor)
	if message != nil {
		w.pusher.broadcast <- *message
	}

}

// NotifyNewSubscriber the message to client
func (w *Watcher) NotifyNewSubscriber(client *Client) {

	logger.Debugf("Watcher %s NotifyNewSubscriber %v", w.clusterName, client)

	if w.isExpire() {
		w.loadCases()
	}

	w.caseMonitors.Range(func(_, cmi interface{}) bool {
		caseMonitor := cmi.(asmv1beta2.CaseMonitor)
		message := w.parseToMessage(caseMonitor)
		if message != nil {
			client.send <- message.result
		}
		return true
	})

}

func (w *Watcher) isExpire() bool {
	length := 0
	w.caseMonitors.Range(func(key, cmi interface{}) bool {
		caseMonitor := cmi.(asmv1beta2.CaseMonitor)
		if isExpire(caseMonitor) {
			w.caseMonitors.Delete(key)
			return true
		}

		length++
		return true
	})

	return length == 0

}

func isExpire(caseMonitor asmv1beta2.CaseMonitor) bool {
	if caseMonitor.Spec.MonitorResults.LatestUpdated.IsZero() {
		return true
	}
	current := time.Now()
	latestCheckTime := caseMonitor.Spec.MonitorResults.LatestUpdated.Time
	return current.Sub(latestCheckTime) > defalutCacheSpan
}

func (w *Watcher) parseToMessage(caseMonitor asmv1beta2.CaseMonitor) *Message {
	if isExpire(caseMonitor) {
		return nil
	}

	if caseMonitor.Spec.MonitorResults.Targets != nil && len(caseMonitor.Spec.MonitorResults.Targets) > 0 {

		monitorResult := &MonitorResult{
			Name:          caseMonitor.Name,
			LatestUpdated: caseMonitor.Spec.MonitorResults.LatestUpdated.Time.Format(time.RFC3339),
			MessageType:   MessageTypeMessage,
		}

		joblist := make(map[string]bool)

		jobs := []*MonitorJob{}
		for _, target := range caseMonitor.Spec.MonitorResults.Targets {

			monitorJob := w.parseTarget(&target)

			jobs = append(jobs, monitorJob)

			joblist[monitorJob.Name] = true

		}

		jobs = notInstallFilter(joblist, jobs, requiredJobs[caseMonitor.Name])

		sort.Slice(jobs, func(i, j int) bool {
			return jobs[i].Name < jobs[j].Name
		})

		monitorResult.MonitorJobs = jobs

		message := &Message{
			cluster: w.clusterName,
			result:  monitorResult,
		}
		return message
	}

	return nil
}

// RefreshJob trigger by client to regresh
func (w *Watcher) RefreshJob(jobName string) error {

	logger.Debugf("Watcher RefreshJob %s", jobName)
	caseMonitor, err := w.client.AsmV1beta2().CaseMonitors(config.AlaudaNamespace()).Get(jobName, common.GetOptionsInCache)

	if err != nil {

		logger.Errorf("Watcher RefreshJob get job %s error %v", jobName, err)
		return err
	}

	caseMonitor.Labels[refreshLabelKey] = refreshValueOK
	if caseMonitor.Spec.MonitorResults.LatestUpdated.IsZero() {
		caseMonitor.Spec.MonitorResults.LatestUpdated = metav1.Now()
	}
	_, err = w.client.AsmV1beta2().CaseMonitors(config.AlaudaNamespace()).Update(caseMonitor)
	if err != nil {
		logger.Errorf("Watcher RefreshJob update job %s error %v", jobName, err)
		return err
	}

	return nil
}

func (w *Watcher) parseTarget(target *asmv1beta2.Target) *MonitorJob {

	monitorJob := &MonitorJob{
		Name:       target.Name,
		LastErrors: target.LastErrors,
	}

	monitorJob.TargetType = jobTypeConvert(target.Name, target.TargetType)

	monitorJob.Health = target.Health == HealthGood

	if monitorJob.LastErrors == nil {
		monitorJob.LastErrors = []string{}
	}

	return monitorJob

}

// Stop the watcher
func (w *Watcher) Stop() {

	w.onceClose.Do(func() {
		close(w.stop)
		close(w.newSubscriber)
	})

}

func jobTypeConvert(targetName, targetType string) (jobType string) {

	if targetType == TargetTypeWorkload {
		if strings.HasPrefix(targetName, istioPrefix) || targetName == asmControllerName {
			jobType = TargetTypeCore
		} else {
			jobType = TargetTypeExternal
		}

	} else {
		jobType = TargetTypeCase
	}

	return

}

// notInstallFilter just checks the required component
func notInstallFilter(joblist map[string]bool, jobs []*MonitorJob, requiredJobstr string) []*MonitorJob {

	requireds := strings.Split(requiredJobstr, ",")
	for _, requiredJob := range requireds {

		if ok, _ := joblist[requiredJob]; !ok {

			jobs = append(jobs, &MonitorJob{
				Name:       requiredJob,
				Health:     false,
				TargetType: jobTypeConvert(requiredJob, TargetTypeWorkload),
				LastErrors: []string{componentNotInstallERR},
			})

		}
	}

	return jobs

}
