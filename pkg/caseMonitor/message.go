package caseMonitor

const (
	TargetTypeCore     = "core"
	TargetTypeExternal = "external"
	TargetTypeCase     = "case"

	TargetTypePing     = "ping"
	TargetTypeWorkload = "workload"

	HealthTrue  = "true"
	HealthFalse = "false"

	HealthUnknown = "unknown"
	HealthGood    = "up"
	HealthBad     = "down"

	EventSucceeded EventPhase = "Succeeded"
	EventFailed    EventPhase = "Failed"
	EventUnknown   EventPhase = "Unknown"

	EventSucceededMessage = "event executed successfully"

	MessageTypeEvent   MessageType = "event"
	MessageTypeMessage MessageType = "message"
)

type EventPhase string

type MessageType string

type Message struct {
	cluster string
	result  *MonitorResult
}

type MonitorResult struct {
	Name          string        `json:"name"`
	LatestUpdated string        `json:"latestUpdatedTime"`
	MonitorJobs   []*MonitorJob `json:"monitorJobs"`
	MessageType   MessageType   `json:"messageType"`
}

type MonitorJob struct {
	Name       string   `json:"name"`
	TargetType string   `json:"type"`
	LastErrors []string `json:"error"`
	Health     bool     `json:"running"`
}

type EventMessage struct {
	EventType   string       `json:"event"`
	JobName     string       `json:"job"`
	Cluster     string       `json:"cluster"`
	Trigger     *Client      `json:"-"`
	Status      *EventStatus `json:"status,omitempty"`
	MessageType MessageType  `json:"messageType"`
}

type EventStatus struct {
	Phase   EventPhase `json:"phase,omitempty"`
	Message string     `json:"message,omitempty"`
}
