package caseMonitor

import (
	"encoding/json"
	"sync"
	"time"

	"bitbucket.org/mathildetech/hermes/pkg/logging"
	pkgerrors "github.com/pkg/errors"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512

	PingMessage = "ping"
)

var (
	newline = []byte{'\n'}
	space   = []byte{' '}
)
var logger = logging.RegisterScope("casemonitor")

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

// Client is a middleman between the websocket connection and the Pusher.
type Client struct {
	pusher *Pusher

	cluster string

	// The websocket connection.
	conn *websocket.Conn

	// Buffered channel of outbound messages.
	send chan interface{}

	onceClose sync.Once
}

// readPump pumps messages from the websocket connection to the Pusher.
//
// The application runs readPump in a per-connection goroutine. The application
// ensures that there is at most one reader on a connection by executing all
// reads from this goroutine.
func (c *Client) readPump() {
	defer func() {
		logger.Error("client readPump unregister client")
		c.pusher.unregister <- c
	}()
	c.conn.SetReadLimit(maxMessageSize)
	c.conn.SetReadDeadline(time.Now().Add(pongWait))
	c.conn.SetPongHandler(func(msg string) error {

		c.conn.SetReadDeadline(time.Now().Add(pongWait))
		return nil
	})
	for {

		var event EventMessage
		msgType, msg, err := c.conn.ReadMessage()

		logger.Debugf("readmessage %v,%s,%v", msgType, msg, err)
		if err != nil {
			logger.Errorf("Client readPump error: %s", err)

			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				logger.Errorf("Client readPump IsUnexpectedCloseError error: %+v", pkgerrors.WithStack(err))
				break
			}

			if websocket.IsCloseError(err, websocket.CloseAbnormalClosure, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				logger.Errorf("Client readPump  IsCloseError : %+v", pkgerrors.WithStack(err))
				break
			}
		}

		if msg != nil && string(msg) != PingMessage {

			err = json.Unmarshal(msg, &event)
			if err != nil {
				logger.Errorf("Client %v received not supported event ", c.conn)
				break
			}

			if event.EventType != "" {
				event.Cluster = c.cluster
				event.Trigger = c

				c.pusher.event <- event
			}
		}

	}
}

// writePump pumps messages from the Pusher to the websocket connection.
//
// A goroutine running writePump is started for each connection. The
// application ensures that there is at most one writer to a connection by
// executing all writes from this goroutine.
func (c *Client) writePump() {
	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()

		logger.Error("client writepump unregister client")
		c.pusher.unregister <- c
	}()
	for {
		select {
		case message, ok := <-c.send:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				// The Pusher closed the channel.
				c.conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			logger.Debugf("Client send message : %v", message)

			err := c.conn.WriteJSON(message)
			if err != nil {
				logger.Errorf("client writepump err : %+v", pkgerrors.WithStack(err))
				return
			}

		case <-ticker.C:
			c.conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := c.conn.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				logger.Errorf("client writepump err : %v", err)
				return
			}
		}
	}
}

func (c *Client) stop() {
	c.onceClose.Do(func() {
		close(c.send)
		c.conn.Close()
	})
}
