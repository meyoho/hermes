package caseMonitor

import (
	"bitbucket.org/mathildetech/hermes/pkg/kubeclient"
	"sync"
	"time"

	"bitbucket.org/mathildetech/hermes/pkg/safe"
	"github.com/gorilla/websocket"
	"k8s.io/client-go/rest"
)

const (
	TokenKey = "token"
)

// Pusher maintains the set of active clients and broadcasts messages to the
// clients.
type Pusher struct {
	// Registered clients.
	clients map[string]map[*Client]bool

	wlock    sync.RWMutex
	watchers map[string]*Watcher

	// Inbound messages from the clients.
	broadcast chan Message

	// Register requests from the clients.
	register chan *Client

	event chan EventMessage

	// Unregister requests from clients.
	unregister chan *Client

	watch   chan string
	unwatch chan string
	lock    sync.RWMutex
}

type clusterInfo struct {
	clusterName  string
	clusterToken string
	ClusterHost  string
}

func NewPusher() *Pusher {
	return &Pusher{
		broadcast:  make(chan Message),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[string]map[*Client]bool),
		watch:      make(chan string),
		watchers:   make(map[string]*Watcher),
		event:      make(chan EventMessage),
		unwatch:    make(chan string),
	}
}

// main processor to handler all sub routine
func (p *Pusher) Run() {

	for {
		select {
		case client := <-p.register:
			safe.Go(func() {
				p.registerClient(client)
			})
		case client := <-p.unregister:
			safe.Go(func() {
				p.unregisterClient(client)
			})
		case message := <-p.broadcast:
			safe.Go(func() {
				p.broadcastMessage(message)
			})
		case cluster := <-p.watch:
			safe.Go(func() {
				p.watching(cluster)
			})
		case event := <-p.event:
			safe.Go(func() {
				p.triggerEvent(event)
			})
		case cluster := <-p.unwatch:
			safe.Go(func() {
				p.unWatching(cluster)
			})
		}
	}
}

// remove the watcher form watcher list
func (p *Pusher) unWatching(cluster string) {

	p.wlock.Lock()
	defer p.wlock.Unlock()
	if watcher, ok := p.watchers[cluster]; ok {
		delete(p.watchers, cluster)
		watcher.Stop()
	}
}

// add cluster wather to watcher list
func (p *Pusher) watching(cluster string) {
	logger.Debugf("Pusher watching %s", cluster)
	p.wlock.Lock()
	defer p.wlock.Unlock()

	if _, ok := p.watchers[cluster]; !ok {
		var cfg *rest.Config
		var err error
		if cfg, err = kubeclient.Manager.RemoteRestConfig(cluster); err != nil {
			logger.Errorf("fetch rest config failed: %+v\n", err)
			return
		}
		logger.Debugf("remoteconfig: %v\n", cfg)
		watcher := NewWatcher(cluster, cfg)
		if watcher != nil {
			watcher.pusher = p
			p.watchers[cluster] = watcher
			safe.Go(func() {
				watcher.Start()
			})
		}
	}
}

// new client registration
func (p *Pusher) registerClient(client *Client) {

	p.lock.Lock()

	if clients, ok := p.clients[client.cluster]; ok {
		clients[client] = true
	} else {

		clients := make(map[*Client]bool)

		clients[client] = true
		p.clients[client.cluster] = clients

	}
	p.lock.Unlock()

	p.subscribeClient(client)
}

func (p *Pusher) unregisterClient(client *Client) {
	logger.Debugf("Pusher unregisterClient %v", client)
	p.lock.Lock()

	if clients, ok := p.clients[client.cluster]; ok {
		delete(clients, client)
		p.lock.Unlock()

		client.stop()
		return
	}
	p.lock.Unlock()
}

// client first connection to get list from watcher
func (p *Pusher) subscribeClient(client *Client) {

	logger.Debugf("Pusher subscribeClient %v", client)

	if _, ok := p.watchers[client.cluster]; !ok {
		time.Sleep(2 * time.Second)
	}

	if watcher, ok := p.watchers[client.cluster]; ok {

		logger.Debugf("Pusher subscribeClient find watcher %v", client)
		watcher.newSubscriber <- client
		return
	}

}

// watcher notify to client
func (p *Pusher) broadcastMessage(message Message) {
	logger.Debugf("Pusher broadcastMessage %v", message)

	p.lock.RLock()
	clients, ok := p.clients[message.cluster]
	// copy to local var to avoid concurrent panic
	tmpClients := make(map[*Client]bool, len(clients))
	for k, v := range clients {
		tmpClients[k] = v
	}
	p.lock.RUnlock()
	if ok {
		for client := range tmpClients {
			client.send <- message.result
		}
	}
}

// serveWs handles websocket requests from the peer.
func (p *Pusher) ServeWs(cluster string, conn *websocket.Conn, config *rest.Config) {

	logger.Debugf("Pusher ServeWs %s with config %v", cluster, config)

	client := &Client{pusher: p, cluster: cluster, conn: conn, send: make(chan interface{}, 10)}

	safe.Go(func() {
		p.watch <- cluster
	})
	safe.Go(func() {
		p.register <- client
	})

	// Allow collection of memory referenced by the caller by doing all work in
	// new goroutines.
	safe.Go(func() {
		client.writePump()
	})
	safe.Go(func() {
		client.readPump()
	})
}

// client event
func (p *Pusher) triggerEvent(event EventMessage) {

	if _, ok := p.watchers[event.Cluster]; !ok {
		return
	}

	Watcher := p.watchers[event.Cluster]

	switch event.EventType {
	case "refresh":
		err := Watcher.RefreshJob(event.JobName)
		event.MessageType = MessageTypeEvent
		if err != nil {

			event.Status = &EventStatus{
				Phase:   EventFailed,
				Message: err.Error(),
			}

			logger.Errorf("Pusher triggerEvent err: %v", event)

			event.Trigger.send <- event
			return
		}

		logger.Debugf("Pusher triggerEvent succeed: %v", event)

	}
}
