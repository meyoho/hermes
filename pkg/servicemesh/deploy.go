package servicemesh

import (
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/config"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	pkgerrors "github.com/pkg/errors"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"sync"
	"time"
)

var (
	deployManager *DeployManager
)

type DeployManager struct {
	managerCh chan *ServiceMeshManager
	quit      chan struct{}
	wg        sync.WaitGroup
}

func (m *ServiceMeshManager) Ready() {
	close(m.ready)
}

func (m *ServiceMeshManager) WaitCanResp() {
	select {
	case <-m.canResp:
	case <-time.After(5 * time.Second):
	}
}

func (m *ServiceMeshManager) Deploy(sm *ServiceMesh) error {

	if m.execute == nil {
		if err := m.setupCurrentRemoteCluster(sm.Spec.Cluster); err != nil {
			return err
		}
		m.obj = sm
		m.execute = func() error {
			select {
			case <-m.ready:
			case <-time.After(5 * time.Second):
			}
			ctx, cancel := context.WithTimeout(m.ctx, 5*time.Minute)
			m.monitorCanceled(ctx, cancel, sm)
			defer cancel()
			if err := m.updateServiceMeshState(sm.Name, ServiceMeshPending, nil); err != nil {
				logger.Error(err)
			}
			var deployError error
			if deployError = m.prodDeploy(ctx, sm); deployError != nil {
				if err := m.updateServiceMeshState(sm.Name, ServiceMeshFailed, deployError); err != nil {
					logger.Error(err)
				}
			} else {
				if err := m.updateServiceMeshState(sm.Name, ServiceMeshSynced, nil); err != nil {
					logger.Error(err)
				}
			}
			return deployError
		}
	}
	return deployManager.putQueue(m)
}

func (m *ServiceMeshManager) Cleanup(sm *ServiceMesh) error {

	if m.execute == nil {
		if err := m.setupCurrentRemoteCluster(sm.Spec.Cluster); err != nil {
			return err
		}
		m.obj = sm
		m.execute = func() error {
			select {
			case <-m.ready:
			case <-time.After(5 * time.Second):
			}
			ctx, cancel := context.WithTimeout(m.ctx, 5*time.Minute)
			m.monitorCanceled(ctx, cancel, sm)
			defer cancel()
			existed, err := m.clusterExisted(sm.Spec.Cluster)
			if err != nil {
				return err
			}
			if existed {
				if err := m.updateServiceMeshState(sm.Name, ServiceMeshDeleting, nil); err != nil {
					logger.Error(err)
				}
				close(m.canResp)
				var deployError error
				if deployError = m.prodCleanup(ctx, sm.Spec.Cluster); deployError != nil {
					if err := m.updateServiceMeshState(sm.Name, ServiceMeshFailed, deployError); err != nil {
						logger.Error(err)
					}
					return deployError
				}
			} else {
				if err := m.directDeleteClusterHelmRequests(sm.Spec.Cluster); err != nil {
					return err
				}
			}
			return m.DeleteServiceMesh(sm.Name)
		}
	}
	return deployManager.putQueue(m)
}
func (m *ServiceMeshManager) monitorCanceled(ctx context.Context, cancelFunc context.CancelFunc, sm *ServiceMesh) {
	m.goFunc(func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-time.After(time.Second):
			}
			logger.Debugf("monitor servicemesh canceled %s", sm.Name)
			unstruct, err := m.smClient.Namespace(config.AlaudaNamespace()).Get(sm.Name, metav1.GetOptions{})
			if err != nil {
				logger.Errorf("monitor canceled failed: %v", err)
				continue
			}
			sm := &ServiceMesh{}
			if err := common.JsonConvert(unstruct, sm); err != nil {
				logger.Error(pkgerrors.WithStack(err))
				continue
			}
			if sm.Status.Canceled {
				if err := m.Cancel(sm.Name, false); err != nil {
					logger.Errorf("reset servicemesh failed: %v", err)
				}
				cancelFunc()
				return
			}
		}
	})
}

func (m *ServiceMeshManager) updateServiceMeshState(name string, state ServiceMeshState, ierr error) error {
	unstruct, err := m.smClient.Namespace(config.AlaudaNamespace()).Get(name, metav1.GetOptions{})
	if err != nil {
		return pkgerrors.WithStack(err)
	}
	sm := &ServiceMesh{}
	if err := common.JsonConvert(unstruct, sm); err != nil {
		return err
	}
	sm.Status.State = state
	sm.Status.ErrorMessage = ""
	if ierr != nil {
		sm.Status.ErrorMessage = ierr.Error()
	}
	if err := common.JsonConvert(sm, unstruct); err != nil {
		return err
	}
	_, err = m.smClient.Namespace(config.AlaudaNamespace()).Update(unstruct, metav1.UpdateOptions{})
	return pkgerrors.WithStack(err)
}
func (m *ServiceMeshManager) Cancel(name string, canceled bool) error {
	unstruct, err := m.smClient.Namespace(config.AlaudaNamespace()).Get(name, metav1.GetOptions{})
	if err != nil {
		return err
	}
	sm := &ServiceMesh{}
	if err := common.JsonConvert(unstruct, sm); err != nil {
		return err
	}
	sm.Status.Canceled = canceled
	if err := common.JsonConvert(sm, unstruct); err != nil {
		return err
	}
	_, err = m.smClient.Namespace(config.AlaudaNamespace()).Update(unstruct, metav1.UpdateOptions{})
	return err
}

func (m *ServiceMeshManager) directDeleteClusterHelmRequests(cluster string) error {

	stages, err := m.cleanupStages(cluster)
	if err != nil {
		return err
	}
	for _, stage := range stages {
		for _, item := range stage {
			if err := m.deleteHelmRequest(item.payload.(string)); err != nil {
				return err
			}
		}
	}
	return nil
}

func (m *ServiceMeshManager) clusterExisted(cluster string) (bool, error) {
	_, err := m.clusterClient.Namespace(config.AlaudaNamespace()).Get(cluster, metav1.GetOptions{})
	if err != nil {
		if k8serrors.IsNotFound(err) {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

type executeItem struct {
	payload   interface{}
	err       error
	preRun    func() error
	postRun   func() error
	preCheck  func() (bool, error)
	postCheck func() (bool, error)
}

func (m *ServiceMeshManager) cleanupStages(cluster string) ([][]*executeItem, error) {
	return [][]*executeItem{
		{
			&executeItem{payload: helmRequestName(cluster, istioChart),
				postRun: func() error {
					return m.cleanupGrafanaJaegerGlobalResource(cluster)
				},
			},
		},
		{
			&executeItem{payload: helmRequestName(cluster, istioInitChart),
				preRun: func() error {
					return m.cleanupIstioCRD()
				},
			},
		},
		{
			&executeItem{payload: helmRequestName(cluster, asmInitChart)},
			&executeItem{payload: helmRequestName(cluster, jaegerOperatorChart),
				postRun: func() error {
					crdName := "jaegers.jaegertracing.io"
					m.remoteAPIExtensionClient.ApiextensionsV1beta1().CustomResourceDefinitions().Delete(crdName, &metav1.DeleteOptions{})
					return nil
				},
			},
		}, {
			&executeItem{payload: helmRequestName(cluster, alaudaServiceMeshChart),
				preCheck: m.waitAsmCRDCleanup,
				postRun:  m.deleteIstioResource,
			},
		},
	}, nil
}

func (m *ServiceMeshManager) deployStages(sm *ServiceMesh) ([][]*executeItem, error) {
	asmInitReq := asmInitReqFactory(sm)
	istioInitReq := istioInitReqFactory(sm)
	istioReq := istioReqFactory(sm)
	jaegerOperatorReq := jaegerOperatorReqFactory(sm)
	alaudaServiceMeshReq := alaudaServiceMeshReqFactory(sm)
	return [][]*executeItem{
		{
			{payload: istioInitReq,
				postCheck: func() (bool, error) {
					logger.Debug("check istio init crd job succeed")
					return m.checkIstioInitCrdJobSucceed() == nil, nil
				},
				preRun: func() error {
					if err := m.ensureIstioNamespace(); err != nil {
						return err
					}
					return nil
				}},
			{payload: asmInitReq},
			{payload: jaegerOperatorReq,
				postRun: func() error {
					logger.Debug("ensure jaeger global ingress endpoint resoucres")
					if err := m.ensureJaegerGlobalIngressService(sm); err != nil {
						return err
					}
					return nil
				},
			},
		},
		{
			{payload: istioReq,
				postRun: func() error {
					err := m.reloadGrafana(sm)
					if err != nil {
						return err
					}
					logger.Debug("ensure grafana global ingress endpoint resoucres")
					return m.ensureGrafanaGlobalIngressService(sm)
				}},
			{payload: alaudaServiceMeshReq},
		},
	}, nil
}

func allCompleted(completes map[int]bool) bool {
	for _, c := range completes {
		if !c {
			return false
		}
	}
	return true
}

func loopUntil(ctx context.Context, interval time.Duration, maxRetries int, f func() (bool, error)) error {
	count := 0
	for {
		if stop, err := f(); err != nil {
			if count++; count > maxRetries {
				return err
			}
		} else if stop {
			break
		}
		select {
		case <-time.After(interval):
		case <-ctx.Done():
			return fmt.Errorf("execute canceled")
		}
	}
	return nil
}
func loopItemsUntil(ctx context.Context, interval time.Duration, maxRetries int, items []*executeItem, f func(*executeItem) (bool, error)) error {
	completes := make(map[int]bool)
	retries := make(map[int]int)
	for i := range items {
		completes[i] = false
	}
	for {
		for i, item := range items {
			if completes[i] {
				continue
			}
			if stop, err := f(item); err != nil {
				logger.Errorf("execute failed: %v, retrying \n", err)
				if retries[i] += 1; retries[i] > maxRetries {
					return err
				}
			} else if stop {
				completes[i] = true
			}
		}
		if allCompleted(completes) {
			return nil
		}
		select {
		case <-time.After(interval):
		case <-ctx.Done():
			err := fmt.Errorf("execute canceled")
			for i, done := range completes {
				if !done {
					items[i].err = err
				}
			}
			return err
		}
	}
}

func (m *ServiceMeshManager) waitStageCleanupFinished(ctx context.Context, items []*executeItem) error {
	return loopItemsUntil(ctx, 5*time.Second, 5, items, func(item *executeItem) (bool, error) {
		name := item.payload.(string)
		logger.Debugf("waiting for helmrequest %s deleted ", name)
		if _, err := m.helmRequestClient.Namespace(config.AlaudaNamespace()).Get(name, metav1.GetOptions{}); err != nil {
			if k8serrors.IsNotFound(err) {
				return true, nil
			}
			return false, err
		}
		return false, nil
	})
}

func (m *ServiceMeshManager) waitStageDeployFinished(ctx context.Context, items []*executeItem) error {
	return loopItemsUntil(ctx, 5*time.Second, 5, items, func(item *executeItem) (bool, error) {
		request := item.payload.(*HelmRequest)
		logger.Debugf("waiting for helmrequest %s synced ", request.Name)
		synced, err := m.isHelmRequestSynced(request)
		return synced, err
	})
}

func (m *ServiceMeshManager) processStages(ctx context.Context, stages [][]*executeItem, processStage func([]*executeItem) error) error {
	resetFailedReason := func() error {
		for _, items := range stages {
			for _, item := range items {
				if err := m.updateHelmRequestFailedReason(item, item.err); err != nil {
					return err
				}
			}
		}
		return nil
	}
	defer resetFailedReason()
	if err := resetFailedReason(); err != nil {
		return err
	}
	for _, items := range stages {
		for _, item := range items {
			if item.preCheck != nil {
				logger.Debugf("run precheck")
				if err := loopUntil(ctx, 5*time.Second, 10, item.preCheck); err != nil {
					item.err = err
					return err
				}
			}
		}
		for _, item := range items {
			if item.preRun != nil {
				logger.Debugf("run prerun")
				if err := item.preRun(); err != nil {
					item.err = err
					return err
				}
			}
		}
		if err := processStage(items); err != nil {
			return err
		}
		for _, item := range items {
			if item.postRun != nil {
				logger.Debugf("run postrun")
				if err := item.postRun(); err != nil {
					item.err = err
					return err
				}
			}
		}
		for _, item := range items {
			if item.postCheck != nil {
				logger.Debugf("run postcheck")
				if err := loopUntil(ctx, 5*time.Second, 10, item.postCheck); err != nil {
					item.err = err
					return err
				}
			}
		}
	}
	return nil
}

func (m *ServiceMeshManager) prodCleanup(ctx context.Context, cluster string) error {
	stages, err := m.cleanupStages(cluster)
	if err != nil {
		return err
	}
	return m.processStages(ctx, stages, func(items []*executeItem) error {
		for _, item := range items {
			if err := m.deleteHelmRequest(item.payload.(string)); err != nil {
				item.err = err
				return err
			}
		}
		if err := m.waitStageCleanupFinished(ctx, items); err != nil {
			return err
		}
		return nil
	})
}

func (m *ServiceMeshManager) prodDeploy(ctx context.Context, sm *ServiceMesh) error {
	stages, err := m.deployStages(sm)
	if err != nil {
		return err
	}
	return m.processStages(ctx, stages, func(items []*executeItem) error {
		for _, item := range items {
			if err := m.createOrUpdateHelmRequest(item.payload.(*HelmRequest)); err != nil {
				item.err = err
				return err
			}
		}
		return m.waitStageDeployFinished(ctx, items)
	})
}

func (m *ServiceMeshManager) isHelmRequestSynced(request *HelmRequest) (bool, error) {
	var unstructReq *unstructured.Unstructured
	var err error

	if unstructReq, err = m.helmRequestClient.Namespace(request.GetNamespace()).Get(request.GetName(), metav1.GetOptions{}); err != nil {
		return false, err
	}
	req := &HelmRequest{}
	if err := common.JsonConvert(unstructReq, req); err != nil {
		return false, err
	}
	switch req.Status.Phase {
	case helmRequestFailed:
		return false, fmt.Errorf("chart %s in %s deploy failed", req.Spec.Chart, req.Spec.ClusterName)
	case helmRequestSynced:
		return true, nil
	default:
		return false, nil
	}
}

func (m *ServiceMeshManager) updateHelmRequestFailedReason(item *executeItem, err1 error) error {
	name := ""
	switch typedPayload := item.payload.(type) {
	case string:
		name = typedPayload
	case *HelmRequest:
		name = typedPayload.Name
	default:
		return fmt.Errorf("unknown payload: %v", typedPayload)
	}
	unstructReq, err := m.helmRequestClient.Namespace(config.AlaudaNamespace()).Get(name, metav1.GetOptions{})
	if err != nil {
		if k8serrors.IsNotFound(err) {
			return nil
		}
		return err
	}
	req := &HelmRequest{}
	if err := common.JsonConvert(unstructReq, req); err != nil {
		return err
	}
	errorMsg := ""
	if err1 != nil {
		errorMsg = err1.Error()
	}
	if req.Annotations[asmHelmRequestFailedReason] == errorMsg {
		return nil
	}
	if req.Annotations == nil {
		req.Annotations = map[string]string{}
	}
	req.Annotations[asmHelmRequestFailedReason] = errorMsg
	if err := common.JsonConvert(req, unstructReq); err != nil {
		return err
	}
	if _, err = m.helmRequestClient.Namespace(unstructReq.GetNamespace()).Update(unstructReq, metav1.UpdateOptions{}); err != nil {
		return err
	}
	return nil
}

func (m *ServiceMeshManager) updateHelmRequestPhase(name, namespace string, phase helmRequestPhase) (*unstructured.Unstructured, error) {
	unstructReq, err := m.helmRequestClient.Namespace(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	req := &HelmRequest{}
	if err := common.JsonConvert(unstructReq, req); err != nil {
		return nil, err
	}
	req.Status.Phase = phase
	if err := common.JsonConvert(req, unstructReq); err != nil {
		return nil, err
	}
	if unstructReq, err = m.helmRequestClient.Namespace(unstructReq.GetNamespace()).UpdateStatus(unstructReq, metav1.UpdateOptions{}); err != nil {
		return nil, err
	}
	return unstructReq, nil
}

func (m *ServiceMeshManager) createOrUpdateHelmRequest(tplReq *HelmRequest) error {
	logger.Debugf("update helmrequest %s", tplReq.Name)
	unstructReq, err := m.helmRequestClient.Namespace(tplReq.GetNamespace()).Get(tplReq.GetName(), metav1.GetOptions{})
	if err != nil {
		if k8serrors.IsNotFound(err) {
			unstructReq := &unstructured.Unstructured{}
			if err = common.JsonConvert(tplReq, unstructReq); err != nil {
				return err
			}
			if unstructReq, err = m.helmRequestClient.Namespace(unstructReq.GetNamespace()).Create(unstructReq, metav1.CreateOptions{}); err != nil {
				logger.Errorf("m.helmRequestClient.Namespace  create us  get error %v", err)
				return err
			}
			/*
				if _, err := m.updateHelmRequestPhase(unstructReq.GetName(), unstructReq.GetNamespace(), helmRequestPending); err != nil {
					logger.Errorf("m.updateHelmRequestPhase.Namespace  update us  get error %v", err)
					return err
				}*/
			return nil
		}
		return err
	}

	withNewSpec := func(hr *HelmRequest) *HelmRequest {
		newReq := &HelmRequest{}
		*newReq = *hr
		newReq.Spec.Chart = tplReq.Spec.Chart
		newReq.Spec.Dependencies = tplReq.Spec.Dependencies
		newReq.Spec.Values = tplReq.Spec.Values
		newReq.Spec.ValuesFrom = tplReq.Spec.ValuesFrom
		return newReq
	}
	// check update status.phase to pending
	var newReq *HelmRequest
	oldReq := &HelmRequest{}
	if err := common.JsonConvert(unstructReq, oldReq); err != nil {
		return err
	}
	newReq = withNewSpec(oldReq)
	if !jsonDeepEqual(newReq.Spec, oldReq.Spec) {
		data1, _ := json.Marshal(newReq.Spec)
		data2, _ := json.Marshal(oldReq.Spec)
		logger.Tracef("newspec: %s \noldspec: %s", string(data1), string(data2))
		logger.Tracef("update %s status to pending ", oldReq.GetName())
		if unstructReq, err = m.updateHelmRequestPhase(unstructReq.GetName(), unstructReq.GetNamespace(), helmRequestPending); err != nil {
			logger.Errorf("m.updateHelmRequestPhase get error %v", err)
			return err
		}
	}
	// update spec
	if err := common.JsonConvert(unstructReq, oldReq); err != nil {
		return err
	}
	newReq = withNewSpec(oldReq)
	err = common.JsonConvert(newReq, unstructReq)
	if err != nil {
		return err
	}

	if unstructReq, err = m.helmRequestClient.Namespace(unstructReq.GetNamespace()).Update(unstructReq, metav1.UpdateOptions{}); err != nil {
		logger.Errorf("m.helmRequestClient update us  get error %v", err)
		return err
	}
	return nil
}

func jsonDeepEqual(v1, v2 interface{}) bool {
	data1, err := json.Marshal(v1)
	if err != nil {
		return false
	}
	data2, err := json.Marshal(v2)
	if err != nil {
		return false
	}
	return bytes.Equal(data1, data2)

}

func (m *ServiceMeshManager) deleteHelmRequest(name string) error {
	logger.Debugf("delete helmrequest %s", name)
	err := m.helmRequestClient.Namespace(config.AlaudaNamespace()).Delete(name, &metav1.DeleteOptions{})
	if k8serrors.IsNotFound(err) {
		return nil
	}
	return err
}

func (m *DeployManager) putQueue(smm *ServiceMeshManager) error {
	var err error
	smm.lock, err = smm.lockCreator()
	if err != nil {
		return err
	}
	alertText := "服务网格操作中，无法执行"
	if err := smm.acquire(); err != nil {
		return errors.New(alertText)
	}
	select {
	case m.managerCh <- smm:
		return nil
	default:
		return errors.New(alertText)
	}
}
func (m *DeployManager) goFunc(f func()) {
	m.wg.Add(1)
	go func() {
		defer m.wg.Done()
		f()
	}()
}

func (m *DeployManager) runWorker() {
	m.goFunc(func() {
		for {
			var smm *ServiceMeshManager
			select {
			case <-m.quit:
				return
			case smm = <-m.managerCh:
			}
			m.goFunc(func() {
				defer smm.release()
				if smm.obj != nil {
					logger.Debugf("start deploy %s", smm.obj.Name)
				}

				if err := smm.execute(); err != nil {
					if smm.obj != nil {
						logger.Errorf("deploy or cleanup %s failed:%+v", smm.obj.Name, err)
					}
				} else {
					if smm.obj != nil {
						logger.Infof("deploy or cleanup %s succeed", smm.obj.Name)
					}
				}
			})
		}
	})
}

func resetManager() {
	if deployManager != nil {
		close(deployManager.quit)
		deployManager.wg.Wait()
	}
	deployManager = &DeployManager{
		managerCh: make(chan *ServiceMeshManager, 0),
		quit:      make(chan struct{}, 0),
	}
	deployManager.runWorker()
}

func init() {
	resetManager()
}
