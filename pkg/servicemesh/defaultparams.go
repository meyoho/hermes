package servicemesh

import (
	"fmt"
	"net/url"
	"strings"

	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/config"
	"bitbucket.org/mathildetech/hermes/pkg/kubeclient"
	"github.com/pkg/errors"
	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

const (
	EsServiceName       = "cpaas-elasticsearch"
	EsSecret            = "cpaas-es"
	kubeSystemNamespace = "kube-system"

	ServiceMonitorDefaultLabel = "kube-Prometheus"
)

type DefaultParams struct {
	Elasticsearch struct {
		URL string `json:"url"`
	} `json:"elasticsearch"`
	Prometheus struct {
		URL                  string            `json:"url"`
		ServiceMonitorLabels map[string]string `json:"serviceMonitorLabels,omitempty"`
	} `json:"prometheus"`
	IPRanges struct {
		Ranges []string `json:"ranges"`
	} `json:"ipranges"`
	RegistryAddress string `json:"registryAddress"`
	ExternalHost    string `json:"externalHost"`
	APIServerHost   string `json:"apiserverHost"`
}

var (
	featureGVR = schema.GroupVersionResource{
		Group:    "infrastructure.alauda.io",
		Version:  "v1alpha1",
		Resource: "features",
	}
)

func NewDefaultParamsGetter(clusterName string) (*DefaultParamsGetter, error) {
	client, err := kubeclient.Manager.InClusterKubeClient()
	if err != nil {
		return nil, err
	}
	remoteCfg, err := kubeclient.Manager.RemoteRestConfig(clusterName)
	if err != nil {
		return nil, err
	}
	remoteClient, err := kubernetes.NewForConfig(remoteCfg)
	if err != nil {
		return nil, err
	}
	dyclient, err := dynamic.NewForConfig(remoteCfg)
	if err != nil {
		return nil, err
	}

	return &DefaultParamsGetter{
		client:              client,
		remoteClient:        remoteClient,
		remoteFeatureClient: dyclient.Resource(featureGVR),
		clusterConfig:       remoteCfg,
		cluster:             clusterName,
	}, nil
}

type DefaultParamsGetter struct {
	remoteClient        kubernetes.Interface
	remoteFeatureClient dynamic.NamespaceableResourceInterface
	client              kubernetes.Interface
	clusterConfig       *rest.Config
	cluster             string
}

func (g *DefaultParamsGetter) prometheusConfig() (string, map[string]string, error) {
	labels := map[string]string{}
	us, err := g.remoteFeatureClient.Get("prometheus", v1.GetOptions{})
	if err != nil {
		return "", labels, err
	}
	type PrometheusFeature struct {
		Spec struct {
			AccessInfo struct {
				PrometheusURL string `json:"prometheusUrl"`
			} `json:"accessInfo"`
		} `json:"spec"`
	}
	feature := &PrometheusFeature{}
	if err := common.JsonConvert(us, feature); err != nil {
		return "", labels, err
	}

	if feature.Spec.AccessInfo.PrometheusURL == "" {
		return "", labels, errors.WithStack(fmt.Errorf("prometheus feature format unexpect"))
	}
	labels = g.parseServiceMonitorSelector(us)
	return feature.Spec.AccessInfo.PrometheusURL, labels, nil
}

func (g *DefaultParamsGetter) elasticsearchURLs() ([]string, error) {
	endpoints, err := g.client.CoreV1().Endpoints(config.AlaudaNamespace()).Get(EsServiceName, v1.GetOptions{})
	if err != nil {
		return nil, err
	}
	if len(endpoints.Subsets) == 0 || len(endpoints.Subsets[0].Addresses) == 0 || len(endpoints.Subsets[0].Ports) == 0 {
		return nil, errors.WithStack(fmt.Errorf("endpoints %s is empty", EsServiceName))

	}
	result := make([]string, 0)
	port := endpoints.Subsets[0].Ports[0].Port
	for _, address := range endpoints.Subsets[0].Addresses {
		address := address.IP
		result = append(result, strings.TrimSpace(fmt.Sprintf("http://%s:%d", address, port)))
	}
	return result, nil
}

func (g *DefaultParamsGetter) ElasticsearchSecret() (string, string, error) {
	secret, err := g.client.CoreV1().Secrets(config.AlaudaNamespace()).Get(EsSecret, v1.GetOptions{})
	if err != nil {
		return "", "", err
	}
	return string(secret.Data["username"]), string(secret.Data["password"]), nil
}

func (g *DefaultParamsGetter) registryAddress() (string, error) {
	deploy, err := g.client.AppsV1().Deployments(config.AlaudaNamespace()).Get("hermes", v1.GetOptions{})
	if err != nil {
		return "", err
	}
	image := deploy.Spec.Template.Spec.Containers[0].Image
	items := strings.Split(image, "/")
	return items[0], nil
}

func (g *DefaultParamsGetter) apiServerPod() (*corev1.Pod, error) {
	podList, err := g.remoteClient.CoreV1().Pods(kubeSystemNamespace).List(v1.ListOptions{LabelSelector: fmt.Sprintf("component=kube-apiserver")})
	if err != nil {
		return nil, err
	}
	if len(podList.Items) == 0 {
		return nil, errors.WithStack(fmt.Errorf("apiserver pod not found"))
	}
	return &podList.Items[0], nil
}

func (g *DefaultParamsGetter) ipranges() ([]string, error) {
	apiserverPod, err := g.apiServerPod()
	if err != nil {
		return nil, err
	}
	apiServerContainer := apiserverPod.Spec.Containers[0]
	result := make([]string, 0, 2)
	for _, line := range append(apiServerContainer.Command, apiServerContainer.Args...) {
		if strings.Contains(line, "service-cluster-ip-range") {
			items := strings.Split(line, "=")
			if len(items) != 2 {
				return nil, errors.WithStack(fmt.Errorf("args format unexpect: %s", line))
			}
			result = append(result, items[1])
		}
	}
	return result, nil
}

func (g *DefaultParamsGetter) externalHost() (string, error) {
	u, err := url.Parse(g.clusterConfig.Host)
	if err != nil {
		return "", err
	}
	items := strings.Split(u.Host, ":")
	if len(items) == 0 {
		return "", fmt.Errorf("")

	}
	return items[0], nil
}

func (g *DefaultParamsGetter) Get() (*DefaultParams, error) {
	params := &DefaultParams{}
	var err error
	var esURLs []string
	var esURL string
	var prometheusURL string
	var serviceMonitorlabels map[string]string
	var ipranges []string
	var registryAddress string
	var externalHost string
	if esURLs, err = g.elasticsearchURLs(); err != nil {
		logger.Errorf("global es not existed")
		esURL = ""
	} else {
		esURL = esURLs[0]
	}
	if prometheusURL, serviceMonitorlabels, err = g.prometheusConfig(); err != nil {
		logger.Errorf("%s prometheus get failed", g.cluster)
	}
	if ipranges, err = g.ipranges(); err != nil {
		logger.Errorf("%s ipranges get failed", g.cluster)
		ipranges = []string{"*"}
	}
	if registryAddress, err = g.registryAddress(); err != nil {
		logger.Errorf("%s registryAddress get failed", g.cluster)
	}
	if externalHost, err = g.externalHost(); err != nil {
		logger.Errorf("%s externalHost get failed", g.cluster)
	}
	params.Elasticsearch.URL = esURL
	params.Prometheus.URL = prometheusURL
	params.Prometheus.ServiceMonitorLabels = serviceMonitorlabels
	params.IPRanges.Ranges = ipranges
	params.RegistryAddress = registryAddress
	params.APIServerHost = g.clusterConfig.Host
	params.ExternalHost = externalHost
	return params, nil
}

func SetDefaultParams(sm *ServiceMesh) error {
	g, err := NewDefaultParamsGetter(sm.Spec.Cluster)
	if err != nil {
		return err
	}
	if sm.Spec.Elasticsearch.IsDefault {
		defaultEsURLs, err := g.elasticsearchURLs()
		if err != nil {
			return err
		}
		iurl := strings.TrimSpace(sm.Spec.Elasticsearch.URL)
		for _, url := range defaultEsURLs {
			if iurl == url {
				username, password, err := g.ElasticsearchSecret()
				if err != nil {
					return err
				}
				sm.Spec.Elasticsearch.Username = username
				sm.Spec.Elasticsearch.Password = password
				break
			}
		}
	}
	// set Prometheus config
	prometheusURL, serviceMonitorLabels, err := g.prometheusConfig()
	if err != nil {
		return err
	}
	sm.Spec.ServiceMonitorLabels = serviceMonitorLabels
	sm.Spec.PrometheusURL = prometheusURL
	return nil
}

func (g *DefaultParamsGetter) parseServiceMonitorSelector(prometheus *unstructured.Unstructured) map[string]string {
	matchLabels := map[string]string{}
	inLabels := map[string]string{}
	notInLabels := map[string]string{}
	existsLabels := map[string]string{}
	notExistLabels := map[string]string{}
	labels := map[string]string{}

	if prometheus != nil {
		prometheusSpec := (prometheus.UnstructuredContent())["spec"].(map[string]interface{})

		accessInfo := prometheusSpec["accessInfo"].(map[string]interface{})
		prometheusConfig := accessInfo["prometheusConfig"].(map[string]interface{})
		prometheusSelector := prometheusConfig["serviceMonitorSelector"].(map[string]interface{})
		if labels, ok := prometheusSelector["matchLabels"]; ok {
			for key, value := range labels.(map[string]interface{}) {
				matchLabels[key] = value.(string)
			}
		}
		if labels, ok := prometheusSelector["matchExpressions"]; ok {
			for _, label := range labels.([]interface{}) {
				label := label.(map[string]interface{})
				if label["operator"] == "In" {
					labelValues := label["values"].([]interface{})
					for _, value := range labelValues {
						inLabels[label["key"].(string)] = value.(string)
					}
				} else if label["operator"] == "NotIn" {
					labelValues := label["values"].([]interface{})
					for _, value := range labelValues {
						notInLabels[label["key"].(string)] = value.(string)
					}
				} else if label["operator"] == "Exists" {
					existsLabels[label["key"].(string)] = ""
				} else if label["operator"] == "DoesNotExist" {
					notExistLabels[label["key"].(string)] = ""
				}
			}
		}
	}

	for key, val := range matchLabels {
		labels[key] = val
	}

	for key, val := range inLabels {
		labels[key] = val
	}
	for key, val := range notInLabels {
		if currentVal, ok := labels[key]; ok {
			if val == currentVal {
				labels[key] = ServiceMonitorDefaultLabel + "-" + common.RandStringRunes(5)
			}
		}
	}

	for key := range existsLabels {
		if _, ok := labels[key]; !ok {
			labels[key] = ServiceMonitorDefaultLabel + "-" + common.RandStringRunes(5)
		}
	}

	for key := range notExistLabels {
		if _, ok := labels[key]; ok {
			delete(labels, key)
		}
	}
	return labels
}
