package servicemesh

import (
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/config"
	"bitbucket.org/mathildetech/hermes/pkg/logging"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	lock "github.com/pulcy/kube-lock"
	corev1 "k8s.io/api/core/v1"
	k8serror "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"os"
	"time"
)

var (
	logger = logging.RegisterScope("servicemesh")
)

const (
	leaseTimeout = 10 * time.Second

	lockNameTpl = "asm-deploy-lock-%s"
)

var annotationKey = "asm." + common.GetLocalBaseDomain() + "/deploy"

func (m *ServiceMeshManager) k8sLockCreator() (lock.KubeLock, error) {
	id, err := uuid.NewUUID()
	if err != nil {
		return nil, err
	}
	hostname, err := os.Hostname()
	if err != nil {
		return nil, err
	}
	ownerID := fmt.Sprintf("%s-%s", hostname, id.String())
	l, err := lock.NewKubeLock(annotationKey, ownerID, leaseTimeout, m.getmeta, m.updatemeta)
	if err != nil {
		return nil, err
	}
	return l, nil
}

func (m *ServiceMeshManager) autoRefreshLock() {
	tick := time.NewTicker(leaseTimeout / 4)
	m.goFunc(func() {
		defer tick.Stop()
		for {
			select {
			case <-tick.C:
				if err := m.lock.Acquire(); err != nil {
					logger.Errorf("acquire lock failed %v", err)
				}
				logger.Debug("auto refresh lock")
			case <-m.ctx.Done():
				return
			}
		}
	})
}

func (m *ServiceMeshManager) acquire() error {
	if err := m.lock.Acquire(); err != nil {
		return err
	}
	m.autoRefreshLock()
	return nil
}
func (m *ServiceMeshManager) release() error {
	m.cancelFunc()
	m.wg.Wait()
	return m.lock.Release()
}

func (m *ServiceMeshManager) currentOwner() (string, error) {
	ann, _, _, err := m.getmeta()
	if err != nil {
		return "", err
	}
	if ann == nil {
		ann = make(map[string]string)
	}
	if lockDataRaw, ok := ann[annotationKey]; ok && lockDataRaw != "" {
		var lockData lock.LockData
		if err := json.Unmarshal([]byte(lockDataRaw), &lockData); err != nil {
			return "", err
		}
		if time.Now().Before(lockData.ExpiresAt) {
			return lockData.Owner, nil
		}
	}
	return "", nil
}

func (m *ServiceMeshManager) getmeta() (map[string]string, string, interface{}, error) {
	cm, err := m.k8sClient.CoreV1().ConfigMaps(config.AlaudaNamespace()).Get(fmt.Sprintf(lockNameTpl, m.currentCluster), metav1.GetOptions{})
	if err != nil {
		if !k8serror.IsNotFound(err) {
			return nil, "", nil, err
		}
		return nil, "", nil, nil
	}
	return cm.Annotations, cm.ResourceVersion, nil, nil
}

func (m *ServiceMeshManager) updatemeta(annotations map[string]string, resourceVersion string, extra interface{}) error {
	alaudaNamespace := config.AlaudaNamespace()
	cm := &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:            fmt.Sprintf(lockNameTpl, m.currentCluster),
			Namespace:       alaudaNamespace,
			Annotations:     annotations,
			ResourceVersion: resourceVersion,
		},
	}
	_, err := m.k8sClient.CoreV1().ConfigMaps(alaudaNamespace).Update(cm)
	if k8serror.IsNotFound(err) {
		_, err := m.k8sClient.CoreV1().ConfigMaps(alaudaNamespace).Create(cm)
		return err
	}
	return err
}
