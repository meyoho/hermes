package servicemesh

import (
	"bitbucket.org/mathildetech/hermes/pkg/config"
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	pkgerrors "github.com/pkg/errors"
	"io/ioutil"
	v1 "k8s.io/api/core/v1"
	extv1beta1 "k8s.io/api/extensions/v1beta1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/util/intstr"
	"net"
	"net/url"
	"sigs.k8s.io/yaml"
	"strings"

	"net/http"
)

const (
	istioCrdGroup           = "istio.io"
	minIstioCrdGroups       = 1
	globalJaegerNamePrefix  = "jaeger-asm-"
	globalJaegerNodePort    = 30668
	globalJaegerSvcPort     = 16686
	globalGrafanaNamePrefix = "grafana-asm-"
	globalGrafanaNodePort   = 30667
	globalGrafanaSvcPort    = 3000
)

func (m *ServiceMeshManager) reloadGrafana(sm *ServiceMesh) error {
	var grafanaURL string
	var client *http.Client
	if sm.Spec.GlobalIngressHost == "" {
		return fmt.Errorf("GlobalIngressHost is nil ,%s", sm.Spec.Cluster)
	}

	grafanaURL = fmt.Sprintf("%s/%s/api/datasources/1", sm.Spec.GlobalIngressHost,
		globalGrafanaNamePrefix+sm.Spec.Cluster)
	logger.Debugf("reloadGrafana  grafanaURL is %s", grafanaURL)
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client = &http.Client{Transport: tr}

	resp, err := client.Get(grafanaURL)
	if err != nil {
		return nil
	}
	if resp.StatusCode != http.StatusOK {
		return nil
	}
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	body := map[string]interface{}{}
	if err := json.Unmarshal(data, &body); err != nil {
		return fmt.Errorf("decode grafana response failed: %v", err)
	}
	body["url"] = sm.Spec.PrometheusURL
	data, err = json.Marshal(body)
	if err != nil {
		return err
	}
	buf := bytes.NewBuffer(data)
	req, err := http.NewRequest(http.MethodPut, grafanaURL, buf)
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	resp, err = client.Do(req)
	if err != nil {
		return err
	}
	data, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	logger.Debugf("update grafana datasource response: %s \n", string(data))
	return nil
}

func (m *ServiceMeshManager) ensureIstioNamespace() error {
	istioNamespace := config.IstioNamespace()
	_, err := m.remotek8sClient.CoreV1().Namespaces().Get(istioNamespace, metav1.GetOptions{})
	if err != nil {
		if !k8serrors.IsNotFound(err) {
			return err
		}
		_, err := m.remotek8sClient.CoreV1().Namespaces().Create(
			&v1.Namespace{ObjectMeta: metav1.ObjectMeta{Name: istioNamespace}},
		)
		if err != nil {
			return err
		}
	}
	return nil
}

func (m *ServiceMeshManager) createUpdateGlobalService(sm *ServiceMesh, name string, nodeport, port int32) error {
	jaegerName := name + sm.Spec.Cluster
	svcport := v1.ServicePort{
		Port:     port,
		Protocol: "TCP",
		TargetPort: intstr.IntOrString{
			IntVal: nodeport,
			Type:   intstr.Int,
		},
	}
	svc := &v1.Service{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Service",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Namespace: config.AlaudaNamespace(),
			Name:      jaegerName,
		},
		Spec: v1.ServiceSpec{
			Ports:    []v1.ServicePort{svcport},
			Type:     "ClusterIP",
			Selector: nil,
		},
	}

	_, err := m.k8sClient.CoreV1().Services(config.AlaudaNamespace()).Get(jaegerName, metav1.GetOptions{})
	if k8serrors.IsNotFound(err) {
		_, err = m.k8sClient.CoreV1().Services(config.AlaudaNamespace()).Create(svc)
		return err
	} else {
		_ = m.k8sClient.CoreV1().Services(config.AlaudaNamespace()).Delete(jaegerName, &metav1.DeleteOptions{})

		_, err = m.k8sClient.CoreV1().Services(config.AlaudaNamespace()).Create(svc)
		return err
	}
	return nil
}

func (m *ServiceMeshManager) createUpdateJaegerGlobalEndpoint(sm *ServiceMesh, name string, nodeport int32) error {
	jaegerName := name + sm.Spec.Cluster

	var addresses []v1.EndpointAddress
	for _, item := range sm.Spec.ClusterNodeIps {
		address := v1.EndpointAddress{IP: item}
		addresses = append(addresses, address)
	}
	epport := v1.EndpointPort{
		Port:     nodeport,
		Protocol: "TCP",
	}
	epSubSet := v1.EndpointSubset{
		Addresses: addresses,
		Ports:     []v1.EndpointPort{epport},
	}

	endpoint := &v1.Endpoints{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Endpoints",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Namespace: config.AlaudaNamespace(),
			Name:      jaegerName,
		},
		Subsets: []v1.EndpointSubset{epSubSet},
	}

	_, err := m.k8sClient.CoreV1().Endpoints(config.AlaudaNamespace()).Get(jaegerName, metav1.GetOptions{})
	if k8serrors.IsNotFound(err) {
		_, err = m.k8sClient.CoreV1().Endpoints(config.AlaudaNamespace()).Create(endpoint)
		return err
	} else {
		_, err = m.k8sClient.CoreV1().Endpoints(config.AlaudaNamespace()).Update(endpoint)
		return err
	}
	return nil

}

func (m *ServiceMeshManager) createUpdateJaegerGlobalIngress(sm *ServiceMesh, name, prefixpath string,
	port int32, annotations map[string]string) error {

	jaegerName := name + sm.Spec.Cluster
	path := extv1beta1.HTTPIngressPath{
		Path: prefixpath,
		Backend: extv1beta1.IngressBackend{
			ServicePort: intstr.IntOrString{
				IntVal: port,
				Type:   intstr.Int,
			},
			ServiceName: jaegerName,
		},
	}

	u, err := url.Parse(sm.Spec.GlobalIngressHost)
	if err != nil {
		return err
	}

	rule := extv1beta1.IngressRule{
		IngressRuleValue: extv1beta1.IngressRuleValue{
			HTTP: &extv1beta1.HTTPIngressRuleValue{
				Paths: []extv1beta1.HTTPIngressPath{path},
			},
		},
	}

	addr := net.ParseIP(u.Host)
	if addr == nil {
		rule.Host = u.Host
	} else {
	}

	ingress := &extv1beta1.Ingress{
		TypeMeta: metav1.TypeMeta{
			Kind:       "Ingress",
			APIVersion: "extensions/v1beta1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Namespace:   config.AlaudaNamespace(),
			Name:        jaegerName,
			Annotations: annotations,
		},
		Spec: extv1beta1.IngressSpec{
			Backend: nil,
			TLS:     nil,
			Rules:   []extv1beta1.IngressRule{rule},
		},
	}

	_, err = m.k8sClient.ExtensionsV1beta1().Ingresses(config.AlaudaNamespace()).Get(jaegerName, metav1.GetOptions{})
	if k8serrors.IsNotFound(err) {
		_, err = m.k8sClient.ExtensionsV1beta1().Ingresses(config.AlaudaNamespace()).Create(ingress)
		return err
	} else {
		_, err = m.k8sClient.ExtensionsV1beta1().Ingresses(config.AlaudaNamespace()).Update(ingress)
		return err
	}

	return nil

}

func (m *ServiceMeshManager) ensureJaegerGlobalIngressService(sm *ServiceMesh) error {

	err := m.createUpdateGlobalService(sm, globalJaegerNamePrefix, globalJaegerNodePort, globalJaegerSvcPort)
	if err != nil {
		return err
	}

	err = m.createUpdateJaegerGlobalEndpoint(sm, globalJaegerNamePrefix, globalJaegerNodePort)
	if err != nil {
		return err
	}

	err = m.createUpdateJaegerGlobalIngress(sm, globalJaegerNamePrefix, "/"+globalJaegerNamePrefix+sm.Spec.Cluster,
		globalJaegerSvcPort, nil)
	if err != nil {
		return err
	}
	return nil
}

func (m *ServiceMeshManager) ensureGrafanaGlobalIngressService(sm *ServiceMesh) error {
	err := m.createUpdateGlobalService(sm, globalGrafanaNamePrefix, globalGrafanaNodePort, globalGrafanaSvcPort)
	if err != nil {
		return err
	}

	err = m.createUpdateJaegerGlobalEndpoint(sm, globalGrafanaNamePrefix, globalGrafanaNodePort)
	if err != nil {
		return err
	}

	path := "/" + globalGrafanaNamePrefix + sm.Spec.Cluster + "(/|$)(.*)"
	annotations := make(map[string]string)
	annotations["nginx.ingress.kubernetes.io/rewrite-target"] = "/$2"
	err = m.createUpdateJaegerGlobalIngress(sm, globalGrafanaNamePrefix, path, globalGrafanaSvcPort, annotations)
	if err != nil {
		return err
	}
	return nil
}

func (m *ServiceMeshManager) cleanupGrafanaJaegerGlobalResource(clustername string) error {
	jaegerName := globalJaegerNamePrefix + clustername
	_ = m.k8sClient.CoreV1().Services(config.AlaudaNamespace()).Delete(jaegerName, &metav1.DeleteOptions{})
	_ = m.k8sClient.CoreV1().Endpoints(config.AlaudaNamespace()).Delete(jaegerName, &metav1.DeleteOptions{})
	_ = m.k8sClient.ExtensionsV1beta1().Ingresses(config.AlaudaNamespace()).Delete(jaegerName, &metav1.DeleteOptions{})

	grafanaName := globalGrafanaNamePrefix + clustername
	_ = m.k8sClient.CoreV1().Services(config.AlaudaNamespace()).Delete(grafanaName, &metav1.DeleteOptions{})
	_ = m.k8sClient.CoreV1().Endpoints(config.AlaudaNamespace()).Delete(grafanaName, &metav1.DeleteOptions{})
	_ = m.k8sClient.ExtensionsV1beta1().Ingresses(config.AlaudaNamespace()).Delete(grafanaName, &metav1.DeleteOptions{})
	return nil
}

func (m *ServiceMeshManager) checkIstioInitCrdJobSucceed() error {
	groups, err := m.remotek8sClient.Discovery().ServerGroups()
	if err != nil {
		return err
	}
	count := 0
	for _, group := range groups.Groups {
		if strings.Contains(group.Name, istioCrdGroup) {
			count++
		}
	}
	if count < minIstioCrdGroups {
		return fmt.Errorf("istio init crd too few")
	}
	return nil
}

func (m *ServiceMeshManager) cleanupIstioCRD() error {
	crdConfigMapNames := []string{"istio-crd-10", "istio-crd-11", "istio-crd-12"}
	for _, name := range crdConfigMapNames {

		cm, err := m.remotek8sClient.CoreV1().ConfigMaps(config.IstioNamespace()).Get(name, metav1.GetOptions{})
		if err != nil {

			if !k8serrors.IsNotFound(err) {
				return err
			}
			logger.Debugf("no configmap named %s, skip", name)
			continue
		}
		for _, value := range cm.Data {
			for _, data := range strings.Split(value, "---") {
				crd := &unstructured.Unstructured{}
				jsonData, err := yaml.YAMLToJSON([]byte(data))
				if err != nil {
					return err
				}
				if err := json.Unmarshal(jsonData, crd); err != nil {
					logger.Errorf("decode istio crd configmap failed: %v", err)
					continue
				}
				if err := m.remoteAPIExtensionClient.ApiextensionsV1beta1().CustomResourceDefinitions().Delete(crd.GetName(), &metav1.DeleteOptions{}); err != nil {
					logger.Errorf("%+v", pkgerrors.WithStack(err))
				}
			}
		}
	}
	return nil
}

func (m *ServiceMeshManager) waitAsmCRDCleanup() (bool, error) {
	allDeleted := true
	logger.Debug("wait asm crd cleanup")
	for _, name := range []string{"whitelists.asm.alauda.io"} {
		if _, err := m.remoteAPIExtensionClient.ApiextensionsV1beta1().CustomResourceDefinitions().Get(name, metav1.GetOptions{}); err != nil {
			if !k8serrors.IsNotFound(err) {
				return false, err
			}
		} else {
			allDeleted = false
		}
	}
	return allDeleted, nil
}

func (m *ServiceMeshManager) deleteIstioResource() error {
	_, err := m.remotek8sClient.CoreV1().Namespaces().Get(config.IstioNamespace(), metav1.GetOptions{})
	if k8serrors.IsNotFound(err) {
		return nil
	}
	return m.remotek8sClient.CoreV1().Namespaces().Delete(config.IstioNamespace(), &metav1.DeleteOptions{})
}
