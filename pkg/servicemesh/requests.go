package servicemesh

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/config"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	alaudak8sRepo    = "asm"
	ChartRepoEnv     = "CHARTREPO_%s"
	ChartVersionEnv  = "CHARTVERSION_%s"
	defaultChartRepo = "stable"
)

func fullChartName(name chartName) string {
	repo := defaultChartRepo
	if value := os.Getenv(fmt.Sprintf(ChartRepoEnv, strings.ToUpper(string(name)))); value != "" {
		repo = value
	}
	return fmt.Sprintf("%s/%s", repo, name)
}

func chartVersion(name chartName) string {
	return os.Getenv(fmt.Sprintf(ChartVersionEnv, strings.ToUpper(string(name))))
}

func treeValue(values map[string]interface{}) map[string]interface{} {
	result := make(map[string]interface{})
	for k, v := range values {
		setTreeValue(result, k, v)
	}
	return result
}

func setTreeValue(tree map[string]interface{}, key string, value interface{}) {
	tmp := tree
	keys := strings.Split(key, ".")
	count := len(keys)
	for i, k := range keys {
		if i < count-1 {
			if _, ok := tmp[k]; !ok {
				tmp[k] = map[string]interface{}{}
			}
			tmp = tmp[k].(map[string]interface{})
		} else {
			tmp[k] = value
		}
	}
}

func asmInitReqFactory(sm *ServiceMesh) *HelmRequest {
	alaudaNamespace := config.AlaudaNamespace()
	req := &HelmRequest{
		TypeMeta: v1.TypeMeta{
			APIVersion: helmRequestGVK.GroupVersion().String(),
			Kind:       helmRequestGVK.Kind,
		},
		ObjectMeta: v1.ObjectMeta{
			Name:      helmRequestName(sm.Spec.Cluster, asmInitChart),
			Namespace: alaudaNamespace,
		},
		Spec: HelmRequestSpec{
			ClusterName:          sm.Spec.Cluster,
			InstallToAllClusters: false,
			Dependencies:         nil,
			ReleaseName:          helmRequestName(sm.Spec.Cluster, asmInitChart),
			Chart:                fullChartName(asmInitChart),
			Namespace:            alaudaNamespace,
			Version:              chartVersion(asmInitChart),
			Values: treeValue(map[string]interface{}{
				"install.mode": "asm",
			}),
		},
	}
	return req
}

func istioInitReqFactory(sm *ServiceMesh) *HelmRequest {
	req := &HelmRequest{
		TypeMeta: v1.TypeMeta{
			APIVersion: helmRequestGVK.GroupVersion().String(),
			Kind:       helmRequestGVK.Kind,
		},

		ObjectMeta: v1.ObjectMeta{
			Name:      helmRequestName(sm.Spec.Cluster, istioInitChart),
			Namespace: config.AlaudaNamespace(),
		},
		Spec: HelmRequestSpec{
			ClusterName:          sm.Spec.Cluster,
			InstallToAllClusters: false,
			ReleaseName:          helmRequestName(sm.Spec.Cluster, istioInitChart),
			Dependencies:         nil,
			Chart:                fullChartName(istioInitChart),
			Version:              chartVersion(istioInitChart),
			Namespace:            config.IstioNamespace(),
			Values: treeValue(
				map[string]interface{}{
					"global.hub": fmt.Sprintf("%s/%s", sm.Spec.RegistryAddress, alaudak8sRepo),
				}),
		},
	}
	return req
}

func istioReqFactory(sm *ServiceMesh) *HelmRequest {
	req := &HelmRequest{
		TypeMeta: v1.TypeMeta{
			APIVersion: helmRequestGVK.GroupVersion().String(),
			Kind:       helmRequestGVK.Kind,
		},

		ObjectMeta: v1.ObjectMeta{
			Name:      helmRequestName(sm.Spec.Cluster, istioChart),
			Namespace: config.AlaudaNamespace(),
		},
		Spec: HelmRequestSpec{
			ClusterName:          sm.Spec.Cluster,
			InstallToAllClusters: false,
			Dependencies:         []string{helmRequestName(sm.Spec.Cluster, istioInitChart)},
			ReleaseName:          helmRequestName(sm.Spec.Cluster, istioChart),
			Chart:                fullChartName(istioChart),
			Version:              chartVersion(istioChart),
			Namespace:            config.IstioNamespace(),
			Values: treeValue(map[string]interface{}{
				"global.hub":                   fmt.Sprintf("%s/%s", sm.Spec.RegistryAddress, alaudak8sRepo),
				"global.labelBaseDomain":       common.GetLocalBaseDomain(),
				"global.proxy.includeIPRanges": strings.Join(sm.Spec.IPRanges.Ranges, ","),
				"pilot.traceSampling":          fmt.Sprintf("%.2f", sm.Spec.TraceSampling),
			}),
		}}
	// set prometheusURL using by grafana
	req.Spec.Values["grafana"] = map[string]interface{}{
		"datasources": map[string]interface{}{
			"datasources.yaml": map[string]interface{}{
				"datasources": []interface{}{
					map[string]interface{}{
						"name":      "Prometheus",
						"type":      "prometheus",
						"orgId":     1,
						"url":       sm.Spec.PrometheusURL,
						"access":    "proxy",
						"isDefault": true,
						"jsonData": map[string]interface{}{
							"timeInterval": "5s",
						},
						"editable": true,
					},
				},
			},
		},
	}

	setTreeValue(req.Spec.Values, "grafana.ingress.enabled", false)
	setTreeValue(req.Spec.Values, "grafana.service.type", "NodePort")
	setTreeValue(req.Spec.Values, "grafana.rootUrl", sm.Spec.GlobalIngressHost+globalGrafanaNamePrefix+sm.Spec.Cluster)

	if sm.Spec.HighAvailability {
		setTreeValue(req.Spec.Values, "sidecarInjectorWebhook.replicaCount", 2)
		setTreeValue(req.Spec.Values, "galley.replicaCount", 2)
		setTreeValue(req.Spec.Values, "mixer.autoscaleMin", 2)
		setTreeValue(req.Spec.Values, "mixer.policy.autoscaleMin", 2)
		setTreeValue(req.Spec.Values, "mixer.telemetry.autoscaleMin", 2)
		setTreeValue(req.Spec.Values, "pilot.autoscaleMin", 2)
		setTreeValue(req.Spec.Values, "security.replicaCount", 2)
	}
	return req
}

func jaegerOperatorReqFactory(sm *ServiceMesh) *HelmRequest {
	req := &HelmRequest{
		TypeMeta: v1.TypeMeta{
			APIVersion: helmRequestGVK.GroupVersion().String(),
			Kind:       helmRequestGVK.Kind,
		},
		ObjectMeta: v1.ObjectMeta{
			Name:      helmRequestName(sm.Spec.Cluster, jaegerOperatorChart),
			Namespace: config.AlaudaNamespace(),
		},
		Spec: HelmRequestSpec{
			ClusterName:          sm.Spec.Cluster,
			InstallToAllClusters: false,
			ReleaseName:          helmRequestName(sm.Spec.Cluster, jaegerOperatorChart),
			Dependencies:         nil,
			Version:              chartVersion(jaegerOperatorChart),
			Chart:                fullChartName(jaegerOperatorChart),
			Namespace:            config.IstioNamespace(),
			Values: treeValue(map[string]interface{}{
				"fullnameOverride":       "jaeger-operator",
				"image.repository":       fmt.Sprintf("%s/%s/jaeger-operator", sm.Spec.RegistryAddress, alaudak8sRepo),
				"global.labelBaseDomain": common.GetLocalBaseDomain(),
			}),
		}}
	return req
}

func alaudaServiceMeshReqFactory(sm *ServiceMesh) *HelmRequest {
	req := &HelmRequest{
		TypeMeta: v1.TypeMeta{
			APIVersion: helmRequestGVK.GroupVersion().String(),
			Kind:       helmRequestGVK.Kind,
		},
		ObjectMeta: v1.ObjectMeta{
			Name:      helmRequestName(sm.Spec.Cluster, alaudaServiceMeshChart),
			Namespace: config.AlaudaNamespace(),
		},
		Spec: HelmRequestSpec{
			ClusterName:          sm.Spec.Cluster,
			InstallToAllClusters: false,
			Dependencies: []string{
				helmRequestName(sm.Spec.Cluster, istioChart),
				helmRequestName(sm.Spec.Cluster, jaegerOperatorChart),
				helmRequestName(sm.Spec.Cluster, asmInitChart)},
			Chart:       fullChartName(alaudaServiceMeshChart),
			ReleaseName: helmRequestName(sm.Spec.Cluster, alaudaServiceMeshChart),
			Version:     chartVersion(alaudaServiceMeshChart),
			Namespace:   config.AlaudaNamespace(),
			Values: treeValue(map[string]interface{}{
				"global.registry.address":          sm.Spec.RegistryAddress,
				"global.labelBaseDomain":           common.GetLocalBaseDomain(),
				"global.scheme":                    sm.Spec.IngressScheme,
				"global.useNodePort":               true,
				"prometheus.url":                   sm.Spec.PrometheusURL,
				"prometheus.serviceMonitorLabels":  sm.Spec.ServiceMonitorLabels,
				"grafana.url":                      sm.Spec.GlobalIngressHost + globalGrafanaNamePrefix + sm.Spec.Cluster,
				"jaeger.query.basepath":            "/" + globalJaegerNamePrefix + sm.Spec.Cluster,
				"jaeger.url":                       sm.Spec.GlobalIngressHost + globalJaegerNamePrefix + sm.Spec.Cluster,
				"jaeger.elasticsearch.serverurl":   sm.Spec.Elasticsearch.URL,
				"jaeger.elasticsearch.username":    wrapInt(sm.Spec.Elasticsearch.Username),
				"jaeger.elasticsearch.password":    wrapInt(sm.Spec.Elasticsearch.Password),
				"jaeger.elasticsearch.indexprefix": fmt.Sprintf("asm-cluster-%s", sm.Spec.Cluster),
				"flagger.metricsServer":            sm.Spec.PrometheusURL,
				"flagger.selectorLabels":           fmt.Sprintf("service.%s/name,app,name,app.kubernetes.io/name", common.GetLocalBaseDomain()),
			}),
		}}

	return req
}

func wrapInt(i string) string {
	if _, err := strconv.Atoi(i); err == nil {
		return fmt.Sprintf(`"%s"`, i)
	}
	return i
}
