package servicemesh

import (
	"bitbucket.org/mathildetech/hermes/pkg/config"
	"fmt"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sort"
	"strings"
)

func (m *ServiceMeshManager) extraCheckers(cluster string) map[string]func() error {
	return map[string]func() error{
		helmRequestName(cluster, istioInitChart): func() error {
			return m.checkIstioInitCrdJobSucceed()
		},
	}
}

func (m *ServiceMeshManager) Statuses(sm *ServiceMesh) (map[string]ServiceMeshStatusResp, error) {
	cluster := sm.Spec.Cluster
	if err := m.setupCurrentRemoteCluster(cluster); err != nil {
		return nil, err
	}
	helmRequestNames := []string{
		helmRequestName(cluster, asmInitChart),
		helmRequestName(cluster, istioInitChart),
		helmRequestName(cluster, istioChart),
		helmRequestName(cluster, jaegerOperatorChart),
		helmRequestName(cluster, alaudaServiceMeshChart),
	}
	extraCheckers := m.extraCheckers(cluster)
	currentOwner, _ := m.currentOwner()
	statuses := make(map[string]ServiceMeshStatusResp)
	for _, name := range helmRequestNames {
		req, err := m.getHelmRequest(name)
		if err != nil {
			if !k8serrors.IsNotFound(err) {
				return nil, err
			}
			statuses[name] = ServiceMeshStatusResp{
				Status: string(helmRequestUnknown),
			}
			continue
		}
		if req.GetDeletionTimestamp() != nil {
			statuses[name] = ServiceMeshStatusResp{
				Status: string(helmRequestDeleting),
			}
			continue
		}
		status := ServiceMeshStatusResp{
			Status: string(req.Status.Phase),
		}
		if reason := req.Annotations[asmHelmRequestFailedReason]; reason != "" {
			status.ErrorMessage = reason
			status.Status = string(helmRequestFailed)
			statuses[name] = status
			continue
		}
		if req.Status.Phase == helmRequestFailed {
			message, err := m.errorMessage(name)
			if err != nil && !k8serrors.IsNotFound(err) {
				return nil, err
			}
			status.ErrorMessage = message
			statuses[name] = status
			continue
		}
		if req.Status.Phase == helmRequestSynced {
			if checker, ok := extraCheckers[name]; ok {
				if err := checker(); err != nil {
					if currentOwner != "" {
						// now has worker deploying, so set status to pending
						status.Status = string(helmRequestPending)
					} else {
						status.Status = string(helmRequestFailed)
					}
					status.ErrorMessage = err.Error()
				}
			}
		}
		statuses[name] = status
	}
	globalState := sm.Status.State
	if currentOwner == "" && (globalState == ServiceMeshPending || globalState == ServiceMeshDeleting) {
		globalState = ServiceMeshFailed
	}

	isAllSuccess := false
	for _, status := range statuses {
		if status.ErrorMessage == "" && status.Status == ServiceMeshSynced {
			isAllSuccess = true
		} else {
			isAllSuccess = false
			break
		}
	}

	if isAllSuccess {
		statuses[globalStatusKey] = ServiceMeshStatusResp{
			Status:       ServiceMeshSynced,
			ErrorMessage: "",
		}
	} else {
		statuses[globalStatusKey] = ServiceMeshStatusResp{
			Status:       string(globalState),
			ErrorMessage: sm.Status.ErrorMessage,
		}
	}

	return statuses, nil
}
func (m *ServiceMeshManager) errorMessage(name string) (string, error) {
	events, err := m.k8sClient.CoreV1().Events(config.AlaudaNamespace()).List(metav1.ListOptions{
		FieldSelector: fmt.Sprintf("involvedObject.name=%s", name),
	})
	if err != nil {
		return "", err
	}
	if len(events.Items) == 0 {
		return "", nil
	}
	sort.Slice(events.Items, func(i, j int) bool {
		return !events.Items[i].LastTimestamp.Before(&events.Items[j].LastTimestamp)
	})
	messages := make([]string, 0)
	count := len(events.Items)
	for i := 0; i < 10 && i < count; i++ {
		messages = append(messages, events.Items[i].Message)
	}
	return strings.Join(messages, "\n"), nil
}
