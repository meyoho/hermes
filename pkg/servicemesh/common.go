package servicemesh

import (
	"context"
	"fmt"
	"net/url"
	"strings"
	"sync"

	"bitbucket.org/mathildetech/alauda-backend/pkg/client"
	"bitbucket.org/mathildetech/hermes/pkg/clusterRegister"
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/config"
	"bitbucket.org/mathildetech/hermes/pkg/web"
	"github.com/emicklei/go-restful"
	lock "github.com/pulcy/kube-lock"
	v1 "k8s.io/api/core/v1"
	apiextension "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
)

type helmRequestPhase string

const (
	helmRequestFailed   helmRequestPhase = "Failed"
	helmRequestSynced   helmRequestPhase = "Synced"
	helmRequestPending  helmRequestPhase = "Pending"
	helmRequestUnknown  helmRequestPhase = "Unknown"
	helmRequestDeleting helmRequestPhase = "Deleting"
)

type chartName string

const (
	asmInitChart           chartName = "asm-init"
	istioInitChart         chartName = "istio-init"
	istioChart             chartName = "istio"
	jaegerOperatorChart    chartName = "jaeger-operator"
	alaudaServiceMeshChart chartName = "cluster-asm"
)

var asmHelmRequestFailedReason = "asm." + common.GetLocalBaseDomain() + "/failedreason"

func helmRequestName(cluster string, name chartName) string {
	return fmt.Sprintf("asm-%s-%s", cluster, name)
}

type HelmRequest struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Status            HelmRequestStatus `json:"status"`
	Spec              HelmRequestSpec   `json:"spec"`
}

type HelmRequestSpec struct {
	ClusterName          string                 `json:"clusterName,omitempty"`
	InstallToAllClusters bool                   `json:"installToAllClusters,omitempty"`
	Dependencies         []string               `json:"dependencies,omitempty"`
	ReleaseName          string                 `json:"releaseName,omitempty"`
	Chart                string                 `json:"chart,omitempty"`
	Version              string                 `json:"version,omitempty"`
	Namespace            string                 `json:"namespace,omitempty"`
	ValuesFrom           []ValuesFromSource     `json:"valuesFrom,omitempty"`
	Values               map[string]interface{} `json:"values,omitempty"`
}

type ValuesFromSource struct {
	ConfigMapKeyRef *v1.ConfigMapKeySelector `json:"configMapKeyRef,omitempty"`
	SecretKeyRef    *v1.SecretKeySelector    `json:"secretKeyRef,omitempty"`
}

type HelmRequestStatus struct {
	Phase          helmRequestPhase `json:"phase,omitempty"`
	LastSpecHash   string           `json:"lastSpecHash,omitempty"`
	SyncedClusters []string         `json:"syncedClusters,omitempty"`
	Notes          string           `json:"notes,omitempty"`
}

type ServiceMesh struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`
	Spec              ServiceMeshSpec   `json:"spec"`
	Status            ServiceMeshStatus `json:"status"`
}

type ServiceMeshState string

const globalStatusKey = "global"

const (
	ServiceMeshPending  ServiceMeshState = "Pending"
	ServiceMeshDeleting                  = "Deleting"
	ServiceMeshSynced                    = "Synced"
	ServiceMeshFailed                    = "Failed"
)

type ServiceMeshStatus struct {
	State        ServiceMeshState `json:"state"`
	ErrorMessage string           `json:"error_message"`
	Canceled     bool             `json:"canceled"`
}

type ServiceMeshSpec struct {
	Cluster              string            `json:"cluster"`
	RegistryAddress      string            `json:"registryAddress"`
	IngressHost          string            `json:"ingressHost"`
	GlobalIngressHost    string            `json:"globalIngressHost"`
	ExternalHost         string            `json:"externalHost"`
	ClusterNodeIps       []string          `json:"clusterNodeIps"`
	TraceSampling        float64           `json:"traceSampling"`
	HighAvailability     bool              `json:"highAvailability"`
	PrometheusURL        string            `json:"prometheusURL"`
	ServiceMonitorLabels map[string]string `json:"serviceMonitorLabels,omitempty"`
	IPRanges             struct {
		Ranges []string `json:"ranges"`
	} `json:"ipranges"`
	Elasticsearch struct {
		IsDefault bool   `json:"isDefault"`
		Password  string `json:"password,omitempty"`
		Username  string `json:"username,omitempty"`
		URL       string `json:"url"`
	} `json:"elasticsearch"`
	IngressScheme string `json:"ingressScheme"`
}

type ServiceMeshStatusResp struct {
	Status       string `json:"status"`
	ErrorMessage string `json:"error_message"`
}
type ServiceMeshResp struct {
	ServiceMesh ServiceMesh                      `json:"serviceMesh"`
	Statuses    map[string]ServiceMeshStatusResp `json:"status"`
}

type ServiceMeshListResp struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata"`
	Items           []ServiceMeshResp `json:"items"`
}

type ServiceMeshManager struct {
	obj            *ServiceMesh
	request        *restful.Request
	clientManager  client.Manager
	currentCluster string

	multiClusterHost string

	ready   chan struct{}
	canResp chan struct{}
	execute func() error

	lock        lock.KubeLock
	lockCreator func() (lock.KubeLock, error)

	ctx        context.Context
	cancelFunc context.CancelFunc
	wg         sync.WaitGroup

	k8sClient         kubernetes.Interface
	helmRequestClient dynamic.NamespaceableResourceInterface
	smClient          dynamic.NamespaceableResourceInterface
	clusterClient     dynamic.NamespaceableResourceInterface

	remotek8sClient          kubernetes.Interface
	remoteAPIExtensionClient apiextension.Interface
}

const (
	alaudaDomain = "alauda.io"
)

var (
	helmRequestGVK = &schema.GroupVersionKind{
		Group:   fmt.Sprintf("app.%s", alaudaDomain),
		Version: "v1alpha1",
		Kind:    "HelmRequest",
	}
	servicemeshGVK = &schema.GroupVersionKind{
		Group:   fmt.Sprintf("asm.%s", alaudaDomain),
		Version: "v1alpha1",
		Kind:    "ServiceMesh",
	}
)

func NewServiceMeshManager(webctx *web.Context) (*ServiceMeshManager, error) {
	request := webctx.Request
	cfg := webctx.Server.GetManager().ManagerConfig()
	ctx, cancel := context.WithCancel(context.Background())

	manager := &ServiceMeshManager{
		multiClusterHost: cfg.MultiClusterHost,
		clientManager:    webctx.Server.GetManager(),
		ctx:              ctx,
		cancelFunc:       cancel,
		request:          request,
		ready:            make(chan struct{}, 0),
		canResp:          make(chan struct{}, 0),
	}
	var err error

	manager.helmRequestClient, err = manager.clientManager.DynamicClient(request, helmRequestGVK)
	if err != nil {
		return nil, err
	}
	manager.smClient, err = manager.clientManager.DynamicClient(request, servicemeshGVK)
	if err != nil {
		return nil, err
	}
	manager.k8sClient, err = manager.clientManager.Client(request)
	if err != nil {
		return nil, err
	}
	manager.clusterClient, err = manager.clientManager.DynamicClient(request, &clusterRegister.ClusterRegistryGVK)
	if err != nil {
		return nil, err
	}
	manager.lockCreator = manager.k8sLockCreator
	return manager, nil
}

func (m *ServiceMeshManager) setupCurrentRemoteCluster(clusterName string) error {
	m.currentCluster = clusterName

	cfg, err := m.clientManager.Config(m.request)
	if err != nil {
		return err
	}
	cfg.Host = fmt.Sprintf("%s/kubernetes/%s", m.multiClusterHost, clusterName)
	m.remotek8sClient, err = kubernetes.NewForConfig(cfg)
	if err != nil {
		return err
	}
	m.remoteAPIExtensionClient, err = apiextension.NewForConfig(cfg)
	if err != nil {
		return err
	}
	return nil
}
func (m *ServiceMeshManager) goFunc(f func()) {
	m.wg.Add(1)
	go func() {
		defer m.wg.Done()
		f()
	}()
}

func (m *ServiceMeshManager) ValidateServiceMesh(sm *ServiceMesh) error {
	if sm.Spec.Cluster == "" {
		return fmt.Errorf("cluster is empty")
	}
	if sm.Spec.IngressScheme != "http" && sm.Spec.IngressScheme != "https" {
		return fmt.Errorf("ingressScheme must be http or https")
	}
	sm.Spec.Elasticsearch.URL = strings.TrimSpace(sm.Spec.Elasticsearch.URL)
	sm.Spec.PrometheusURL = strings.TrimSpace(sm.Spec.PrometheusURL)
	if _, err := url.ParseRequestURI(sm.Spec.Elasticsearch.URL); err != nil {
		return err
	}
	if _, err := url.ParseRequestURI(sm.Spec.PrometheusURL); err != nil {
		return err
	}

	return m.validateDup(sm)
}

func (m *ServiceMeshManager) validateDup(sm *ServiceMesh) error {
	us, err := m.smClient.Namespace(config.AlaudaNamespace()).Get(sm.Name, metav1.GetOptions{})
	if err != nil {
		if errors.IsNotFound(err) {
			return nil
		}
		return err
	}
	oldSm := &ServiceMesh{}
	if err := common.JsonConvert(us, oldSm); err != nil {
		return err
	}
	if sm.Spec.Cluster != oldSm.Spec.Cluster {
		return fmt.Errorf("existed same name ServiceMesh with different cluster")
	}
	return nil
}
