package servicemesh

import (
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/config"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

var (
	serviceMeshClusterLabelKey = "asm." + common.GetLocalBaseDomain() + "/cluster"
)

func (m *ServiceMeshManager) CreateOrUpdateServiceMesh(sm *ServiceMesh) (*unstructured.Unstructured, error) {
	if sm.Labels == nil {
		sm.Labels = map[string]string{}
	}
	sm.APIVersion = servicemeshGVK.GroupVersion().String()
	sm.Status.State = ServiceMeshPending
	sm.Labels[serviceMeshClusterLabelKey] = sm.Spec.Cluster
	sm.Namespace = config.AlaudaNamespace()

	oldUnstruct, err := m.smClient.Namespace(sm.GetNamespace()).Get(sm.Name, metav1.GetOptions{})
	if err != nil {
		if k8serrors.IsNotFound(err) {
			unstruct := &unstructured.Unstructured{}
			if err := common.JsonConvert(sm, unstruct); err != nil {
				return nil, err
			}
			unstruct, err = m.smClient.Namespace(unstruct.GetNamespace()).Create(unstruct, metav1.CreateOptions{})
			if err != nil {
				return nil, err
			}
			return unstruct, nil
		}
		return nil, err
	}
	old := &ServiceMesh{}
	if err := common.JsonConvert(oldUnstruct, old); err != nil {
		return nil, err
	}
	old.Spec = sm.Spec
	old.Status = sm.Status
	old.Annotations = sm.Annotations
	old.Labels = sm.Labels
	err = common.JsonConvert(old, oldUnstruct)
	if err != nil {
		return nil, err
	}
	unstruct, err := m.smClient.Namespace(oldUnstruct.GetNamespace()).Update(oldUnstruct, metav1.UpdateOptions{})
	if err != nil {
		return nil, err
	}
	return unstruct, nil
}

func (m *ServiceMeshManager) DeleteServiceMesh(name string) error {
	_, err := m.smClient.Namespace(config.AlaudaNamespace()).Get(name, metav1.GetOptions{})
	if k8serrors.IsNotFound(err) {
		return nil
	}
	return m.smClient.Namespace(config.AlaudaNamespace()).Delete(name, &metav1.DeleteOptions{})
}
