package servicemesh

import (
	"github.com/onsi/gomega"
	"testing"
)

func TestSetTreeValue(t *testing.T) {
	g := gomega.NewWithT(t)
	data := map[string]interface{}{}
	setTreeValue(data, "aaa.bbb.ccc", 1)
	setTreeValue(data, "aaa.bbb.ddd", 2)
	setTreeValue(data, "eee.bbb.ddd", 2)
	g.Expect(data["aaa"].(map[string]interface{})["bbb"].(map[string]interface{})["ccc"]).To(gomega.Equal(1))
	g.Expect(data["aaa"].(map[string]interface{})["bbb"].(map[string]interface{})["ddd"]).To(gomega.Equal(2))
	g.Expect(data["eee"].(map[string]interface{})["bbb"].(map[string]interface{})["ddd"]).To(gomega.Equal(2))
}
