package servicemesh

import (
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/config"
	"encoding/json"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func (m *ServiceMeshManager) getHelmRequest(name string) (*HelmRequest, error) {
	us, err := m.helmRequestClient.Namespace(config.AlaudaNamespace()).Get(name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	req := &HelmRequest{}
	if err = common.JsonConvert(us, req); err != nil {
		return nil, err
	}
	return req, nil
}

func (m *ServiceMeshManager) GetServiceMesh(name string) (*ServiceMesh, error) {
	us, err := m.smClient.Namespace(config.AlaudaNamespace()).Get(name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	data, err := json.Marshal(us)
	if err != nil {
		return nil, err
	}
	sm := &ServiceMesh{}
	if err := json.Unmarshal(data, sm); err != nil {
		return nil, err
	}
	return sm, nil
}
func (m *ServiceMeshManager) ListServiceMesh(listOpt metav1.ListOptions) (*ServiceMeshList, error) {
	obj, err := m.smClient.Namespace(config.AlaudaNamespace()).List(listOpt)
	if err != nil {
		return nil, err
	}
	smList := &ServiceMeshList{}
	if err := common.JsonConvert(obj, smList); err != nil {
		return nil, err
	}
	return smList, nil
}

type ServiceMeshList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ServiceMesh `json:"items"`
}
