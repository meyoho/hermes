package servicemesh

import (
	"context"
	"fmt"
	"github.com/onsi/gomega"
	kubelock "github.com/pulcy/kube-lock"
	"math/rand"
	"strconv"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

type mockLock struct {
	n int64
}

func newMockLock() *mockLock {
	return &mockLock{
		n: 0,
	}
}

func newFakeServiceMeshManager(lock kubelock.KubeLock, execute func() error) *ServiceMeshManager {
	lockCreator := func() (kubelock.KubeLock, error) {
		return lock, nil
	}
	ctx, cancel := context.WithCancel(context.Background())
	m := &ServiceMeshManager{
		ctx:        ctx,
		cancelFunc: cancel,
	}
	m.execute = execute
	m.lockCreator = lockCreator
	return m
}

func (l *mockLock) Acquire() error {
	if atomic.SwapInt64(&l.n, 1) == 0 {
		return nil
	}
	return fmt.Errorf("acquire by other")
}
func (l *mockLock) Release() error {
	atomic.StoreInt64(&l.n, 0)
	return nil
}
func (l *mockLock) CurrentOwner() (string, error) {
	num := int(atomic.LoadInt64(&l.n))
	if num != -1 {
		return "", nil
	}
	return strconv.Itoa(num), nil
}

func TestDeployAtomic(t *testing.T) {
	g := gomega.NewGomegaWithT(t)
	resetManager()
	mockDeploy := func() error {
		time.Sleep(500 * time.Millisecond)
		return nil
	}
	lock := newMockLock()
	m1 := newFakeServiceMeshManager(lock, mockDeploy)
	m2 := newFakeServiceMeshManager(lock, mockDeploy)

	time.Sleep(500 * time.Millisecond)
	g.Expect(m1.Deploy(nil)).ShouldNot(gomega.HaveOccurred())
	g.Expect(m2.Deploy(nil)).Should(gomega.HaveOccurred())
	g.Expect(m2.Deploy(nil)).Should(gomega.HaveOccurred())
	time.Sleep(600 * time.Millisecond)
	g.Expect(m2.Deploy(nil)).ShouldNot(gomega.HaveOccurred())
	g.Expect(m1.Deploy(nil)).Should(gomega.HaveOccurred())
}

func TestParallelDeployAtomic(t *testing.T) {
	g := gomega.NewGomegaWithT(t)
	resetManager()
	mockDeploy := func() error {
		time.Sleep(500 * time.Millisecond)
		return nil
	}
	lock := newMockLock()
	time.Sleep(time.Millisecond)

	succeed := 0
	var wg sync.WaitGroup
	for i := 0; i < 20; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			d := newFakeServiceMeshManager(lock, mockDeploy)
			if err := d.Deploy(nil); err == nil {
				succeed++
			}
		}()
	}
	wg.Wait()
	g.Expect(succeed).To(gomega.Equal(1))
}

func init() {
	rand.Seed(time.Now().UnixNano())
}
