package filter

import (
	"sync"

	"bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"

	asmContext "bitbucket.org/mathildetech/hermes/pkg/context"

	restful "github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/api/errors"

	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/clientcmd/api"
)

const (
	QueryParameteToken = "token"
	UserConfigName     = "UserConfig"
)

var (
	initClient sync.Once
	clientGen  ConfigFilter
)

// ClientDecorator decorator for client generator
func ConfigDecorator(svr server.Server) ConfigFilter {
	initClient.Do(func() {
		clientGen = NewClient(svr)
	})
	return clientGen
}

// Client client injector middleware
// will inject kubernetes and devops client
type ConfigFilter struct {
	decorator.Client
}

// NewClient constructor for new client
func NewClient(svr server.Server) ConfigFilter {
	return ConfigFilter{
		Client: decorator.Client{
			Server: svr,
		},
	}
}

// PathTokenFilter filter to populate the context with a user's Bearer token client
// will handle errors and may return errors on request
func (d ConfigFilter) PathTokenFilter(req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	// generate config from request
	config, err := d.Client.GetManager().Config(req)
	if err != nil {
		d.Client.Server.HandleError(err, req, res)
		return
	}
	config, err = d.PathTokenConfigGenerator(config, req)
	if err != nil {
		return
	}

	//client, err := versioned.NewForConfig(config)
	d.SetTokenConfigContext(config, err, req, res, chain)
}

func (d ConfigFilter) PathTokenConfigGenerator(cfg *rest.Config, req *restful.Request) (config *rest.Config, err error) {

	token := req.QueryParameter(QueryParameteToken)
	// validates if the query parameter token is present and not empty, otherwise returns an error
	if token == "" {
		err = errors.NewUnauthorized("No Authorization Token provided")
		return
	}

	cmd := d.buildCmdConfig(&api.AuthInfo{Token: token}, cfg)
	config, err = cmd.ClientConfig()
	return
}

func (d ConfigFilter) buildCmdConfig(authInfo *api.AuthInfo, cfg *rest.Config) clientcmd.ClientConfig {
	cmdCfg := api.NewConfig()
	cmdCfg.Clusters[UserConfigName] = &api.Cluster{
		Server:                   cfg.Host,
		CertificateAuthority:     cfg.TLSClientConfig.CAFile,
		CertificateAuthorityData: cfg.TLSClientConfig.CAData,
		InsecureSkipTLSVerify:    cfg.TLSClientConfig.Insecure,
	}
	cmdCfg.AuthInfos[UserConfigName] = authInfo
	cmdCfg.Contexts[UserConfigName] = &api.Context{
		Cluster:  UserConfigName,
		AuthInfo: UserConfigName,
	}
	cmdCfg.CurrentContext = UserConfigName

	return clientcmd.NewDefaultClientConfig(
		*cmdCfg,
		&clientcmd.ConfigOverrides{},
	)
}

// SetTokenConfigContext given a client and an error will create the common logic to handle error
// and populate the context with the client
func (d ConfigFilter) SetTokenConfigContext(config *rest.Config, err error, req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	if err != nil || config == nil {
		d.Client.Server.HandleError(err, req, res)
		return
	}
	if config != nil {
		req.Request = req.Request.WithContext(asmContext.WithPathTokenConfig(req.Request.Context(), config))
	}
	chain.ProcessFilter(req, res)
}
