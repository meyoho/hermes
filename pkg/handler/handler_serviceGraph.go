package handler

import (
	"context"
	"fmt"
	"net/http"
	"strconv"

	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/hermes/pkg/asmConfig"
	"bitbucket.org/mathildetech/hermes/pkg/logging"
	"bitbucket.org/mathildetech/hermes/pkg/metrics"
	"bitbucket.org/mathildetech/hermes/pkg/models"
	"bitbucket.org/mathildetech/hermes/pkg/prometheus"
	"bitbucket.org/mathildetech/hermes/pkg/servicegraph"

	"github.com/pkg/errors"
)

var (
	servicegraphLogger = logging.RegisterScope("handler.servicegraph")
)

func handleGetNamespaceGraph(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response
	k8sClient := abcontext.Client(req.Request.Context())

	clusterConf, err := asmConfig.GetClusterConfig(ctx, opts)
	if err != nil {
		servicegraphLogger.Errorf("fetch ClusterConfig  err  %+v \n", err)
		srv.HandleError(err, req, resp)
		return
	}

	promClient, err := prometheus.NewClient(clusterConf.Spec.Prometheus)
	if err != nil {

		servicegraphLogger.Errorf("prometheus Newclient err  %+v \n", err)
		srv.HandleError(err, req, resp)
		return

	}

	clusterName := opts.GetClusterName()
	if len(clusterName) == 0 {
		servicegraphLogger.Error("clusterName is empty.")

		srv.HandleError(errors.New("clusterName is empty."), req, resp)
		return

	}

	namespace := req.PathParameter(PathParameterNamespace)
	startTime, err := strconv.Atoi(req.QueryParameter(PathParameterStartTime))
	if err != nil {
		srv.HandleError(err, req, resp)
		return
	}
	endTime, err := strconv.Atoi(req.QueryParameter(PathParameterEndTime))
	if err != nil {
		srv.HandleError(err, req, resp)
		return
	}
	injectServiceNodesString := req.QueryParameter(PathParameterInjectServiceNodes)
	var injectServiceNodes bool
	if injectServiceNodesString == "" {
		injectServiceNodes = DefaultInjectServiceNodes
	} else {
		var injectServiceNodesErr error
		injectServiceNodes, injectServiceNodesErr = strconv.ParseBool(injectServiceNodesString)
		if injectServiceNodesErr != nil {
			srv.HandleError(err, req, resp)
			return
		}
	}

	result, err := servicegraph.GetGraph(ctx, opts, k8sClient, promClient, namespace, startTime, endTime, injectServiceNodes, clusterName)
	if err != nil {
		servicegraphLogger.Errorf("fetch graph err %v", err)
		//fmt.Printf("servicegraph get graph error %s", err)
		srv.HandleError(err, req, resp)
		return
	}
	resp.WriteHeaderAndEntity(http.StatusOK, result)
}

func handleGetMetrics(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	request := opts.Request
	response := opts.Response

	clusterName := opts.GetClusterName()
	if clusterName == "" {
		servicegraphLogger.Error("clusterName is empty.")
		srv.HandleError(errors.New("clusterName is empty."), request, response)
		return
	}

	clusterConf, err := asmConfig.GetClusterConfig(ctx, opts)
	if err != nil {
		servicegraphLogger.Errorf("fetch graph  err %v", err)
		srv.HandleError(err, request, response)
		return
	}

	promClient, err := prometheus.NewClient(clusterConf.Spec.Prometheus)
	if err != nil {

		servicegraphLogger.Errorf("prometheus Newclient err %v", err)
		srv.HandleError(err, request, response)
		return

	}
	basicOptions := metrics.MetricsBasicQueryOptions{P8sAPI: promClient.Api}

	metricsOpts := &metrics.MetricsQueryOptions{MetricsBasicQueryOptions: basicOptions}

	if err := metricsOpts.Parse(request); err != nil {
		servicegraphLogger.Errorf("metricsOpts service parse err %v", err)
		srv.HandleError(err, request, response)
		return
	}

	if err := metricsOpts.ParseQueryMetrics(request); err != nil {
		metricsLogger.Errorf("metricsOpts ParseQueryMetrics err %v", err)
		srv.HandleError(err, request, response)
		return
	}

	if request.QueryParameter(PathParameterService) != "" {

		if err := metricsOpts.ParseForService(request); err != nil {
			servicegraphLogger.Errorf("metricsOpts service ParseForService err %v", err)
			srv.HandleError(err, request, response)
			return
		}

		metrics, err := metrics.GetMetrics(ctx, metricsOpts)
		if err != nil {
			servicegraphLogger.Errorf("Get metrics  getmetrics err %v", err)
			srv.HandleError(err, request, response)
			return
		}
		response.WriteHeaderAndEntity(http.StatusOK, metrics)
		return
	}

	if request.QueryParameter(PathParameterWorkload) != "" {

		if err := metricsOpts.ParseForWorkload(request); err != nil {
			servicegraphLogger.Errorf("metricsOpts workload ParseForWorkload err %v", err)
			srv.HandleError(err, request, response)
			return
		}

		metrics, err := metrics.GetMetrics(ctx, metricsOpts)
		if err != nil {
			servicegraphLogger.Errorf("metrics..GetMetrics err %v", err)
			srv.HandleError(err, request, response)
			return
		}
		response.WriteHeaderAndEntity(http.StatusOK, metrics)
		return
	}

	edgQueryOptions := &metrics.EdgeMetricsQueryOptions{MetricsBasicQueryOptions: basicOptions}
	if err := edgQueryOptions.Parse(request); err != nil {
		servicegraphLogger.Errorf("edgQueryOptions parse err %v", err)
		srv.HandleError(err, request, response)
		return
	}

	if err := edgQueryOptions.ParseQueryMetrics(request); err != nil {
		metricsLogger.Errorf("metricsOpts ParseQueryMetrics err %v", err)
		srv.HandleError(err, request, response)
		return
	}

	metrics, err := metrics.GetMetrics(ctx, edgQueryOptions)
	if err != nil {
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, metrics)
}

func handleGetNodeGraph(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	request := opts.Request
	response := opts.Response

	clusterName := opts.GetClusterName()
	if clusterName == "" {
		servicegraphLogger.Error("clusterName is empty.")

		srv.HandleError(errors.New("clusterName is empty."), request, response)
		return
	}

	clusterConf, err := asmConfig.GetClusterConfig(ctx, opts)
	if err != nil {
		servicegraphLogger.Errorf("fetch ClusterConfig %v", err)
		srv.HandleError(err, request, response)
		return
	}

	promClient, err := prometheus.NewClient(clusterConf.Spec.Prometheus)
	if err != nil {

		servicegraphLogger.Errorf("prometheus Newclient err %v", err)
		srv.HandleError(err, request, response)
		return

	}
	k8sClient := abcontext.Client(request.Request.Context())

	namespace := request.QueryParameter(PathParameterNamespace)
	workload := request.QueryParameter(PathParameterWorkload)
	service := request.QueryParameter(PathParameterService)

	app := request.QueryParameter(PathParameterApp)
	version := request.QueryParameter(PathParameterVersion)
	if namespace == "" || (workload == "" && service == "") {
		srv.HandleError(fmt.Errorf("missing paramater namespace/workload/service"), request, response)
		return
	}

	selectedNamespace := request.QueryParameter(PathParameterSelectedNamespace)

	if selectedNamespace == "" {
		selectedNamespace = namespace
	}

	startTime, err := strconv.Atoi(request.QueryParameter(PathParameterStartTime))
	if err != nil {
		srv.HandleError(fmt.Errorf("%s is not a valid timestamp", request.QueryParameter(PathParameterStartTime)), request, response)
		return
	}
	endTime, err := strconv.Atoi(request.QueryParameter(PathParameterEndTime))
	if err != nil {
		srv.HandleError(fmt.Errorf("%s is not a valid duration", request.QueryParameter(PathParameterEndTime)), request, response)
		return
	}

	injectServiceNodesString := request.QueryParameter(PathParameterInjectServiceNodes)
	var injectServiceNodes bool
	if injectServiceNodesString == "" {
		injectServiceNodes = DefaultInjectServiceNodes
	} else {
		var injectServiceNodesErr error
		injectServiceNodes, injectServiceNodesErr = strconv.ParseBool(injectServiceNodesString)
		if injectServiceNodesErr != nil {

			srv.HandleError(fmt.Errorf("%s is not a valid injectServiceNodes", request.QueryParameter(PathParameterEndTime)), request, response)
			return
		}
	}

	result, err := servicegraph.GetNodeGraph(ctx, opts, k8sClient, promClient, namespace, workload, app, version, service, selectedNamespace, startTime, endTime, injectServiceNodes, clusterName)
	if err != nil {

		servicegraphLogger.Errorf("fetch node graph err %v", err)
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}
