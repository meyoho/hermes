package handler

import (
	"context"

	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/filter"
	"bitbucket.org/mathildetech/hermes/pkg/microservice"

	"bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/hermes/pkg/destinationrule"
	"bitbucket.org/mathildetech/hermes/pkg/gateway"
	"bitbucket.org/mathildetech/hermes/pkg/models"
	"bitbucket.org/mathildetech/hermes/pkg/policy"
	"bitbucket.org/mathildetech/hermes/pkg/servicegraph"
	"bitbucket.org/mathildetech/hermes/pkg/virtualservice"
	"bitbucket.org/mathildetech/hermes/pkg/web"
	"github.com/emicklei/go-restful"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	// PathParameterNamespace is the path parameter name of namespace
	PathParameterNamespace = "namespace"
	// PathParameterName is the path parameter name of resource
	PathParameterName = "name"
	// PathParameterServiceName is the path parameter servicename of resource
	PathParameterServiceName = "servicename"
	PolicyType               = "policyType"
	Cluster                  = "cluster"
	// path parameterStart time
	PathParameterStartTime = "start_time"
	// PathParameterEndTime is the path parameter end_time
	PathParameterEndTime            = "end_time"
	PathParameterStep               = "step"
	PathParameterService            = "service"
	PathParameterWorkload           = "workload"
	PathParameterApp                = "app"
	PathParameterVersion            = "version"
	PathParameterInjectServiceNodes = "inject_service_nodes"

	ResponseOKMessage  = "ok"
	DestinationtuleAll = "0"

	DefaultInjectServiceNodes = false

	PathParameterSelectedNamespace = "selected_namespace"
	Version                        = "version"
	OriginName                     = "asm/originname"
	Workload                       = "workload"
)

var (
	AnnotationsCreator = "asm." + common.GetLocalBaseDomain() + "/creator"
)

// RESTfulContextHandler is the interface for restfuul handler(restful.Request,restful.Response)
type RESTfulContextHandler func(*web.Context)

// RESTfulServiceHandler is the wrapper to combine the RESTfulContextHandler with our serviceprovider object
func RESTfulServiceHandler(srv server.Server, handler RESTfulContextHandler) restful.RouteFunction {
	return func(req *restful.Request, resp *restful.Response) {
		query := abcontext.Query(req.Request.Context())
		ctx := web.Context{
			Server:   srv,
			Request:  req,
			Response: resp,
			Query:    query,
		}
		handler(&ctx)
	}
}

// QueryContextHandler is the interface for restfuul handler(restful.Request,restful.Response)
type QueryContextHandler func(ctx context.Context, opts *models.QueryOptions)

// RegisterHandler is the wrapper to combine the RESTfulContextHandler with our serviceprovider object
func RegisterHandler(srv server.Server, handler QueryContextHandler) restful.RouteFunction {
	return func(req *restful.Request, resp *restful.Response) {
		ctx := req.Request.Context()
		opts := models.NewQueryOptions(srv, req, resp)
		handler(ctx, opts)
	}
}

// RegisterToServer is the rooter registration
func RegisterToServer(srv server.Server) (*restful.WebService, error) {
	// decorator package contain a few easy to use functions to
	// create common used features and init a *restful.WebService
	ws := decorator.NewWebService(srv)

	// this is a Filter (middleware) generator and can provide a few
	// useful models to create a kubernetes.Interface
	clientMW := decorator.Client{Server: srv}
	queryFilter := decorator.NewQuery()

	configFilter := filter.ConfigDecorator(srv)

	ws.Doc("Basic APIs for hermes")
	ws.Path("/api/v1")

	ws.Route(
		decorator.WithAuth(
			ws.GET("/apis").
				Filter(clientMW.SecureFilter).
				Doc(`api groups`).
				To(RESTfulServiceHandler(srv, handleGetAPIGroups)).
				Returns(200, "OK", metav1.APIGroupList{}),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.GET("/servicemesh/graphs/{namespace}").

				// SecureFilter generates and injects the client based on Authorization: Bearer token
				// if --enable-multi-cluster is enabled will automatically use the --multi-cluster-param name
				// to generate a multi cluster request
				Filter(clientMW.SecureFilter).
				// DynamicFilterGenerator generates and injects the DynamicClient based on Authorization: Bearer token
				// if --enable-multi-cluster is enabled will automatically use the --multi-cluster-param name
				// to generate a multi cluster request
				To(RegisterHandler(srv, handleGetNamespaceGraph)).
				Doc("get namespace service graph").
				Returns(200, "OK", servicegraph.Graph{}),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.GET("/servicemesh/metrics").
				Filter(clientMW.SecureFilter).
				Doc(`servicemesh metrics`).
				To(RegisterHandler(srv, handleGetMetrics)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.GET("/servicemesh/nodegraphs").
				Filter(clientMW.SecureFilter).
				Doc(`get node graph`).
				To(RegisterHandler(srv, handleGetNodeGraph)).
				Returns(200, "OK", servicegraph.Graph{}),
		),
	)

	ws.Route(
		queryFilter.Build(
			decorator.WithAuth(
				ws.GET("/destinationrule/{namespace}").
					Filter(clientMW.SecureFilter).
					Filter(clientMW.DynamicFilterGenerator(&destinationrule.GVK)).
					Doc("get list of destination rule").
					To(RESTfulServiceHandler(srv, handleListDestinationRule)),
			),
		),
	)

	ws.Route(
		// adds query parameters and filter to the restful.RouteBuilder
		queryFilter.Build(
			decorator.WithAuth(
				ws.GET("/destinationrule/{namespace}/{name}").
					Filter(clientMW.SecureFilter).
					Filter(clientMW.DynamicFilterGenerator(&destinationrule.GVK)).
					Doc("get  destination rule details").
					To(RESTfulServiceHandler(srv, handleGetDestinationRuleDetail)),
			),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.GET("/destinationruleinfohost/{namespace}/{name}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&destinationrule.GVK)).
				Doc("get  destination rule by host").
				To(RESTfulServiceHandler(srv, handleGetDestinationRuleInfoHost)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.PUT("/destinationrule/{namespace}/{name}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&destinationrule.GVK)).
				Doc("update destination rule").
				To(RESTfulServiceHandler(srv, handleUpdateDestinationRule)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.DELETE("/destinationrule/{namespace}/{name}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&destinationrule.GVK)).
				Doc("delete destination rule").
				To(RESTfulServiceHandler(srv, handleDeleteDestinationRule)),
		),
	)

	ws.Route(

		queryFilter.Build(
			decorator.WithAuth(
				ws.GET("/virtualservice/{namespace}").
					Filter(clientMW.SecureFilter).
					Filter(clientMW.DynamicFilterGenerator(&virtualservice.GVK)).
					Doc("get virtualService list").
					To(RESTfulServiceHandler(srv, handleListVirtualService)),
			),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.GET("/virtualservice/{namespace}/{name}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&virtualservice.GVK)).
				Doc("get virtualService details").
				To(RESTfulServiceHandler(srv, handleGetVirtualService)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.GET("/virtualservicehost/{namespace}/{name}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&virtualservice.GVK)).
				Doc("get virtualService By host").
				To(RESTfulServiceHandler(srv, handleGetVirtualServiceByHost)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.POST("/virtualservice/{namespace}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&virtualservice.GVK)).
				Doc("create virtual service").
				To(RESTfulServiceHandler(srv, handleCreateVirtualService)),
		),
	)
	ws.Route(
		decorator.WithAuth(
			ws.PUT("/virtualservice/{namespace}/{name}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&virtualservice.GVK)).
				Doc("Update VirtualService").
				To(RESTfulServiceHandler(srv, handleUpdateVirtualService)),
		),
	)
	ws.Route(
		decorator.WithAuth(
			ws.DELETE("/virtualservice/{namespace}/{name}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&virtualservice.GVK)).
				Doc("Delete VirtualService").
				To(RESTfulServiceHandler(srv, handleDeleteVirtualService)),
		),
	)

	ws.Route(
		queryFilter.Build(
			decorator.WithAuth(
				ws.GET("/policy/{namespace}").
					Filter(clientMW.SecureFilter).
					Filter(clientMW.DynamicFilterGenerator(&policy.GVK)).
					Doc("get Policy list").
					To(RESTfulServiceHandler(srv, handleListPolicy)),
			),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.GET("/policy/{namespace}/{name}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&policy.GVK)).
				Doc("get policy details").
				To(RESTfulServiceHandler(srv, handleGetPolicy)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.POST("/policy/{namespace}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&policy.GVK)).
				Doc("create policy").
				To(RESTfulServiceHandler(srv, handleCreatePolicy)),
		),
	)
	ws.Route(
		decorator.WithAuth(
			ws.PUT("/policy/{namespace}/{name}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&policy.GVK)).
				Doc("update policy details").
				To(RESTfulServiceHandler(srv, handleUpdatePolicy)),
		),
	)
	ws.Route(
		decorator.WithAuth(
			ws.DELETE("/policy/{namespace}/{name}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&policy.GVK)).
				Doc("delete policy").
				To(RESTfulServiceHandler(srv, handleDeletePolicy)),
		),
	)

	ws.Route(

		queryFilter.Build(
			decorator.WithAuth(
				ws.GET("/gateway/{namespace}").
					Filter(clientMW.SecureFilter).
					Filter(clientMW.DynamicFilterGenerator(&gateway.GVK)).
					Doc("get list of gateway").
					To(RESTfulServiceHandler(srv, handleListGateways)),
			),
		),
	)
	ws.Route(
		decorator.WithAuth(
			ws.GET("/gateway/{namespace}/{name}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&gateway.GVK)).
				Doc("get gateway details").
				To(RESTfulServiceHandler(srv, handleGetGateway)),
		),
	)
	ws.Route(
		decorator.WithAuth(
			ws.POST("/gateway/{namespace}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&gateway.GVK)).
				Doc("create gateway").
				To(RESTfulServiceHandler(srv, handleCreateGateway)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.PUT("/gateway/{namespace}/{name}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&gateway.GVK)).
				Doc("update gateway").
				To(RESTfulServiceHandler(srv, handleUpdateGateway)),
		),
	)
	ws.Route(
		decorator.WithAuth(
			ws.DELETE("/gateway/{namespace}/{name}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&gateway.GVK)).
				Doc("delete gateway").
				To(RESTfulServiceHandler(srv, handleDeleteGateway)),
		),
	)
	ws.Route(
		decorator.WithAuth(
			ws.GET("/servicemesh/manage").
				Filter(clientMW.SecureFilter).
				Doc("list servicemeshs").
				To(RESTfulServiceHandler(srv, handleListServiceMesh)),
		),
	)
	ws.Route(
		decorator.WithAuth(
			ws.GET("/servicemesh/manage/{name}/status").
				Filter(clientMW.SecureFilter).
				Doc("get servicemesh status").
				To(RESTfulServiceHandler(srv, handleGetServiceMeshStatus)),
		),
	)
	ws.Route(
		decorator.WithAuth(
			ws.GET("/servicemesh/manage/{name}").
				Filter(clientMW.SecureFilter).
				Doc("get servicemesh").
				To(RESTfulServiceHandler(srv, handleGetServiceMesh)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.DELETE("/servicemesh/manage/{name}").
				Filter(clientMW.SecureFilter).
				Doc("delete servicemesh").
				To(RESTfulServiceHandler(srv, handleDeleteServiceMesh)),
		),
	)
	ws.Route(
		decorator.WithAuth(
			ws.POST("/servicemesh/manage/{name}/cancel").
				Filter(clientMW.SecureFilter).
				Doc("cancel servicemesh update").
				To(RESTfulServiceHandler(srv, handleCancelUpdateServiceMesh)),
		),
	)
	ws.Route(
		decorator.WithAuth(
			ws.GET("/servicemesh/manage/{name}/defaultparams").
				Doc("servicemesh default params").
				To(RESTfulServiceHandler(srv, handleCreateDefaultParams)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.POST("/servicemesh/manage").
				Filter(clientMW.SecureFilter).
				Doc("update servicemesh").
				To(RESTfulServiceHandler(srv, handleUpdateServiceMesh)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.GET("/relation/{namespace}").
				Filter(clientMW.SecureFilter).
				Doc("get namespace deployment and svc relation").
				To(RESTfulServiceHandler(srv, handleGetMicroserviceNamespaceRelation)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.GET("/relation/{namespace}/{name}").
				Filter(clientMW.SecureFilter).
				Doc("get svc relation deployment").
				To(RESTfulServiceHandler(srv, handleGetServiceRelationDeployment)),
		),
	)

	ws.Route(
		queryFilter.Build(
			decorator.WithAuth(
				ws.GET("/microservice/{namespace}/{name}").
					Filter(clientMW.SecureFilter).
					Filter(clientMW.DynamicFilterGenerator(&microservice.GVK)).
					Doc("get microservice relation  By name").
					To(RESTfulServiceHandler(srv, handleGetMicroserviceRelation)),
			),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.POST("/microservice/{namespace}/{name}/service").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&microservice.GVK)).
				Doc("create microservice service").
				To(RESTfulServiceHandler(srv, handleCreateMicroserviceSvc)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.POST("/microservice/{namespace}/{name}/deployment").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&microservice.GVK)).
				Doc("create deployment version ").
				To(RESTfulServiceHandler(srv, handleCreateMicroserviceDeployment)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.PUT("/microservice/{namespace}/{name}/service/{servicename}").
				Filter(clientMW.SecureFilter).
				Filter(clientMW.DynamicFilterGenerator(&microservice.GVK)).
				Doc("update microservice service").
				To(RESTfulServiceHandler(srv, handleUpdateMicroserviceSvc)),
		),
	)

	ws.Route(
		ws.GET("/healthcheck").
			Filter(configFilter.PathTokenFilter).
			Doc("ws healthcheck").
			To(RESTfulServiceHandler(srv, handleHealthCheckWS)),
	)

	ws.Route(
		decorator.WithAuth(
			ws.GET("/overview/resources/{namespace}").
				Filter(clientMW.SecureFilter).
				Doc("get overview resources").
				To(RegisterHandler(srv, handleGetOverviewResources)),
		),
	)
	ws.Route(
		decorator.WithAuth(
			ws.GET("/overview/monitor/{namespace}").
				Filter(clientMW.SecureFilter).
				Doc("get overview service monitor").
				To(RegisterHandler(srv, handleGetOverviewMonitor)),
		),
	)
	ws.Route(
		decorator.WithAuth(
			ws.GET("/overview/circuits/{namespace}").
				Filter(clientMW.SecureFilter).
				Doc("get overview service monitor").
				To(RegisterHandler(srv, handleGetOverviewCircuits)),
		),
	)
	ws.Route(
		decorator.WithAuth(
			ws.GET("/overview/untraffic/{namespace}").
				Filter(clientMW.SecureFilter).
				Doc("get overview service monitor").
				To(RegisterHandler(srv, handleGetOverviewUntraffic)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.GET("/namespaces/{namespace}/workloads/pods").
				Filter(clientMW.SecureFilter).
				Doc("get current namespace workloads and pos").
				To(RegisterHandler(srv, HanldeGetNamespaceWorkloads)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.GET("/workloadmetrics").
				Filter(clientMW.SecureFilter).
				Doc("get current  workloads metrics based on namespace").
				To(RegisterHandler(srv, handleGetWorkloadMetrics)),
		),
	)

	ws.Route(
		decorator.WithAuth(
			ws.GET("/podmetrics").
				Filter(clientMW.SecureFilter).
				Doc("get muti instances metrics based on namespace").
				To(RegisterHandler(srv, handleGetmutiplePosMetrics)),
		),
	)

	// 获取多个客户端调用当前workload的metrics
	ws.Route(
		decorator.WithAuth(
			ws.GET("/workloadmetrics/{workload}/clients").
				Filter(clientMW.SecureFilter).
				Doc("get muti instances metrics based on namespace").
				To(RegisterHandler(srv, handleGetmutipleClientsMetrics)),
		),
	)

	// 获取工作负载的path资源
	ws.Route(
		decorator.WithAuth(
			ws.GET("/namespaces/{namespace}/workloads/{workload}/paths").
				Filter(clientMW.SecureFilter).
				Doc("get paths  from workloads").
				To(RegisterHandler(srv, handleGetWorkloadPaths)),
		),
	)
	// 获取工作负载的path资源
	ws.Route(
		decorator.WithAuth(
			ws.PUT("/namespaces/{namespace}/workloads/{workload}/paths").
				Filter(clientMW.SecureFilter).
				Doc("set paths  from workloads").
				To(RegisterHandler(srv, handleSetWorkloadPaths)),
		),
	)

	// 根据时间段获取当前namespace下的workloads
	ws.Route(
		decorator.WithAuth(
			ws.GET("/namespaces/{namespace}/workloads").
				Filter(clientMW.SecureFilter).
				Doc("get workloads from current ns within special time").
				To(RegisterHandler(srv, handleGetmutipleWorkloads)),
		),
	)

	// 根据时间段获取负载下的实例pods
	ws.Route(
		decorator.WithAuth(
			ws.GET("/namespaces/{namespace}/workloads/{workload}/pods").
				Filter(clientMW.SecureFilter).
				Doc("get pods from current ns and workload within special time").
				To(RegisterHandler(srv, handleGetmutipleWorkloadPods)),
		),
	)
	// 根据时间段获取调用负载的客户端的负载
	ws.Route(
		decorator.WithAuth(
			ws.GET("/namespaces/{namespace}/workloads/{workload}/clients").
				Filter(clientMW.SecureFilter).
				Doc("get client workloads from current ns and workload within special time").
				To(RegisterHandler(srv, handleGetmutipleWorkloadClients)),
		),
	)
	// 根据时间段查询workload path的metrics
	ws.Route(
		decorator.WithAuth(
			ws.GET("/apimetrics").
				Filter(clientMW.SecureFilter).
				Doc("query url metrics within special time").
				To(RegisterHandler(srv, handleGetWorkloadApimetrics)),
		),
	)

	// 根据时间段查询workload methods
	ws.Route(
		decorator.WithAuth(
			ws.GET("/namespaces/{namespace}/workloads/{workload}/methods").
				Filter(clientMW.SecureFilter).
				Doc("query url  methods in metrics within special time").
				To(RegisterHandler(srv, handleGetWorkloadMethods)),
		),
	)

	//srv.Container().Add(ws)
	return ws, nil
}
