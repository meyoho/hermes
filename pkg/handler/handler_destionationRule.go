package handler

import (
	"bitbucket.org/mathildetech/hermes/pkg/logging"
	"net/http"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"

	"bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/hermes/pkg/destinationrule"
	"bitbucket.org/mathildetech/hermes/pkg/web"
)

var (
	destinationRuleLogger = logging.RegisterScope("handler.destinationrule")
)

// get DestinationRule List filter by  namespace
func handleListDestinationRule(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())

	namespace := request.PathParameter(PathParameterNamespace)
	policyType := request.QueryParameter(PolicyType)

	var dsQuery *dataselect.Query
	if policyType == "" {
		policyType = DestinationtuleAll
		dsQuery = nil
	} else {
		dsQuery = ctx.Query
	}
	list, err := destinationrule.GetDestinationRuleListWithPolicy(dyclient, namespace, policyType, dsQuery)
	if err != nil {
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, list)
}

// get DestinationRule detail filter by  namespace and name
func handleGetDestinationRuleDetail(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())

	namespace := request.PathParameter(PathParameterNamespace)
	name := request.PathParameter(PathParameterName)

	rule, err := destinationrule.GetDestinationRuleDetail(dyclient, namespace, name)
	if err != nil {
		destinationRuleLogger.Errorf("fetch destionation rule err %v", err)
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, rule)
}

// get DestinationRule detail filter by  namespace and name
func handleGetDestinationRuleInfoHost(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response
	namespace := request.PathParameter(PathParameterNamespace)
	hostName := request.PathParameter(PathParameterName)

	dyclient := context.DynamicClient(request.Request.Context())

	rules, err := destinationrule.GetDestinationRuleByHostDetail(dyclient, namespace, hostName)
	if rules == nil && err != nil {
		response.WriteHeaderAndEntity(http.StatusNotFound, hostName)
		return
	}
	if err != nil {
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, rules)
}

func handleUpdateDestinationRule(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response
	namespace := request.PathParameter(PathParameterNamespace)
	name := request.PathParameter(PathParameterName)
	dyclient := context.DynamicClient(request.Request.Context())

	destinationRule := &unstructured.Unstructured{}
	if err := request.ReadEntity(destinationRule); err != nil {
		srv.HandleError(err, request, response)
		return
	}
	var result *unstructured.Unstructured
	var err error
	if result, err = destinationrule.UpdateDestinationRule(dyclient, namespace, name, destinationRule); err != nil {
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)

}

func handleDeleteDestinationRule(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	namespace := request.PathParameter(PathParameterNamespace)
	name := request.PathParameter(PathParameterName)
	dyclient := context.DynamicClient(request.Request.Context())

	if err := destinationrule.DeleteDestinationRule(dyclient, namespace, name); err != nil {
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, "ok")
}
