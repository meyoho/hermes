package handler

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/web"
	"net/http"
)

var (
	ServiceKey = "service." + common.GetLocalBaseDomain() + "/name"
)

// handleGetAPIGroups handle
func handleGetAPIGroups(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response
	client := context.Client(request.Request.Context())
	list, err := client.Discovery().ServerGroups()
	if err != nil {
		srv.HandleError(err, request, response)
		return
	}

	response.WriteHeaderAndEntity(http.StatusOK, list)
}
