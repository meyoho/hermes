package handler

import (
	"net/http"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"

	"bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/hermes/pkg/policy"
	"bitbucket.org/mathildetech/hermes/pkg/web"
)

func handleListPolicy(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())

	namespace := request.PathParameter(PathParameterNamespace)

	query := ctx.Query
	list, err := policy.GetPolicyList(dyclient, namespace, query)
	if err != nil {
		//servicegraphLogger.Error("fetch destionation rule err", log.Err(err))
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, list)

}

func handleGetPolicy(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())

	namespace := request.PathParameter(PathParameterNamespace)

	name := request.PathParameter(PathParameterName)

	rule, err := policy.GetPolicyDetail(dyclient, namespace, name)
	if err != nil {
		//servicegraphLogger.Error("fetch destionation rule err", log.Err(err))
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, rule)
}

func handleCreatePolicy(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())

	namespace := request.PathParameter(PathParameterNamespace)

	usPolicy := &unstructured.Unstructured{}
	if err := request.ReadEntity(usPolicy); err != nil {
		//servicegraphLogger.Error("fetch destionation rule err", log.Err(err))
		srv.HandleError(err, request, response)
		return
	}

	var result *unstructured.Unstructured
	var err error
	if result, err = policy.CreatePolicy(dyclient, namespace, "jweToken.Name", usPolicy); err != nil {
		//servicegraphLogger.Error("fetch destionation rule err", log.Err(err))
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func handleUpdatePolicy(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())

	namespace := request.PathParameter(PathParameterNamespace)
	name := request.PathParameter(PathParameterName)

	usPolicy := &unstructured.Unstructured{}
	if err := request.ReadEntity(usPolicy); err != nil {
		//servicegraphLogger.Error("fetch destionation rule err", log.Err(err))
		srv.HandleError(err, request, response)
		return
	}
	var result *unstructured.Unstructured
	var err error
	if result, err = policy.UpdatePolicy(dyclient, namespace, name, usPolicy); err != nil {
		//servicegraphLogger.Error("fetch destionation rule err", log.Err(err))
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)

}

func handleDeletePolicy(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())

	namespace := request.PathParameter(PathParameterNamespace)
	name := request.PathParameter(PathParameterName)

	if err := policy.DeletePolicy(dyclient, namespace, name); err != nil {
		//servicegraphLogger.Error("fetch destionation rule err", log.Err(err))
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, ResponseOKMessage)
}
