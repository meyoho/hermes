package handler

import (
	"context"
	"errors"
	pkgerrors "github.com/pkg/errors"
	"net/http"
	"sync"

	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/hermes/pkg/asmConfig"
	"bitbucket.org/mathildetech/hermes/pkg/logging"
	"bitbucket.org/mathildetech/hermes/pkg/metrics"
	"bitbucket.org/mathildetech/hermes/pkg/models"
	"bitbucket.org/mathildetech/hermes/pkg/prometheus"
)

var (
	metricsLogger = logging.RegisterScope("handler.metrics")
)

func HanldeGetNamespaceWorkloads(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter(PathParameterNamespace)
	if namespace == "" {
		metricsLogger.Error("Error for path parameters namespace connot be empty")
		srv.HandleError(pkgerrors.New("Path parameter namespace cannot be empty"), req, resp)
		return
	}
	clusterName := opts.GetClusterName()
	if clusterName == "" {
		metricsLogger.Error("clusterName is empty.")
		srv.HandleError(errors.New("clusterName is empty."), req, resp)
		return
	}

	clusterConf, err := asmConfig.GetClusterConfig(ctx, opts)
	if err != nil {
		metricsLogger.Errorf("fetch graph  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	promClient, err := prometheus.NewClient(clusterConf.Spec.Prometheus)
	if err != nil {
		metricsLogger.Errorf("prometheus Newclient err %v", err)
		srv.HandleError(err, req, resp)
		return

	}
	basicOptions := metrics.MetricsBasicQueryOptions{P8sAPI: promClient.Api, K8sClient: k8sClient, Namespace: namespace}

	metricsOpts := &metrics.MetricsQueryOptions{MetricsBasicQueryOptions: basicOptions}

	if err := metricsOpts.Parse(req); err != nil {
		metricsLogger.Errorf("metricsOpts parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	viewModels, err := metrics.GetWorkloadsByNamespace(ctx, metricsOpts)
	if err != nil {
		metricsLogger.Errorf("Error GetWorkloadsByNamespace in namespace %s: %s", namespace, err)
		srv.HandleError(err, req, resp)
		return
	}

	resp.WriteHeaderAndEntity(http.StatusOK, viewModels)
}

func handleGetWorkloadMetrics(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response

	clusterName := opts.GetClusterName()
	if clusterName == "" {
		metricsLogger.Error("clusterName is empty.")

		srv.HandleError(errors.New("clusterName is empty."), req, resp)
		return
	}

	clusterConf, err := asmConfig.GetClusterConfig(ctx, opts)
	if err != nil {
		metricsLogger.Errorf("fetch graph  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	promClient, err := prometheus.NewClient(clusterConf.Spec.Prometheus)
	if err != nil {
		metricsLogger.Errorf("prometheus Newclient err %v", err)
		srv.HandleError(err, req, resp)
		return

	}

	basicOptions := metrics.MetricsBasicQueryOptions{P8sAPI: promClient.Api}

	metricsOpts := &metrics.MetricsQueryOptions{MetricsBasicQueryOptions: basicOptions, IsDetails: true}
	if err := metricsOpts.Parse(req); err != nil {
		metricsLogger.Errorf("metricsOpts parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	if err := metricsOpts.ParseForWorkloadName(req); err != nil {
		metricsLogger.Errorf("metricsOpts parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	if err := metricsOpts.ParseQueryMetrics(req); err != nil {
		metricsLogger.Errorf("metricsOpts ParseQueryMetrics err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	metrics, err := metrics.GetMetrics(ctx, metricsOpts)
	if err != nil {
		metricsLogger.Errorf("Get WorkloadMetrics err %v", err)
		srv.HandleError(err, req, resp)
		return
	}
	resp.WriteHeaderAndEntity(http.StatusOK, metrics)
	return
}

//
//func handlePodMetrics(ctx context.Context, opts *models.QueryOptions) {
//	req := opts.Request
//	instances := req.QueryParameter(QueryParameterInstances)
//	if len(instances) > 0 {
//		handleGetmutiplePosMetrics(ctx, opts)
//	}
//	handleGetPodMetrics(ctx, opts)
//}

func handleGetPodMetrics(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response

	clusterName := opts.GetClusterName()
	if clusterName == "" {
		metricsLogger.Error("clusterName is empty.")
		srv.HandleError(errors.New("clusterName is empty."), req, resp)
		return
	}

	clusterConf, err := asmConfig.GetClusterConfig(ctx, opts)
	if err != nil {
		metricsLogger.Errorf("fetch graph  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	promClient, err := prometheus.NewClient(clusterConf.Spec.Prometheus)
	if err != nil {
		metricsLogger.Errorf("prometheus Newclient err %v", err)
		srv.HandleError(err, req, resp)
		return

	}

	basicOptions := metrics.MetricsBasicQueryOptions{P8sAPI: promClient.Api}

	metricsOpts := &metrics.MetricsQueryOptions{MetricsBasicQueryOptions: basicOptions, IsDetails: true}
	if err := metricsOpts.Parse(req); err != nil {
		metricsLogger.Errorf("metricsOpts parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	if err := metricsOpts.ParseForWorkload(req); err != nil {
		metricsLogger.Errorf("metricsOpts parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	// for pod metrics
	//metricsOpts.Pod = req.PathParameter(PathParameterName)
	metricsOpts.Pod = req.QueryParameter(PathParameterName)

	if err := metricsOpts.ParseQueryMetrics(req); err != nil {
		metricsLogger.Errorf("metricsOpts ParseQueryMetrics err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	metrics, err := metrics.GetMetrics(ctx, metricsOpts)
	if err != nil {
		metricsLogger.Errorf("Get WorkloadMetrics err %v", err)
		srv.HandleError(err, req, resp)
		return
	}
	resp.WriteHeaderAndEntity(http.StatusOK, metrics)
	return
}

func handleGetDynamicMetrics(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response

	clusterName := opts.GetClusterName()
	if clusterName == "" {
		metricsLogger.Error("clusterName is empty.")

		srv.HandleError(errors.New("clusterName is empty."), req, resp)
		return
	}

	clusterConf, err := asmConfig.GetClusterConfig(ctx, opts)
	if err != nil {
		metricsLogger.Errorf("fetch graph  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	promClient, err := prometheus.NewClient(clusterConf.Spec.Prometheus)
	if err != nil {

		metricsLogger.Errorf("prometheus Newclient err %v", err)
		srv.HandleError(err, req, resp)
		return

	}

	basicOptions := metrics.MetricsBasicQueryOptions{P8sAPI: promClient.Api}

	metricsOpts := &metrics.MetricsQueryOptions{MetricsBasicQueryOptions: basicOptions, IsDetails: true}
	if err := metricsOpts.Parse(req); err != nil {
		metricsLogger.Errorf("metricsOpts parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	if err := metricsOpts.ParseQueryMetrics(req); err != nil {
		metricsLogger.Errorf("metricsOpts ParseQueryMetrics err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	metrics, err := metrics.GetMetrics(ctx, metricsOpts)
	if err != nil {
		metricsLogger.Errorf("Get WorkloadMetrics err %v", err)
		srv.HandleError(err, req, resp)
		return
	}
	resp.WriteHeaderAndEntity(http.StatusOK, metrics)
	return
}

func handleGetmutipleClientsMetrics(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response

	clusterName := opts.GetClusterName()
	if clusterName == "" {
		metricsLogger.Error("clusterName Clients is empty.")
		srv.HandleError(errors.New("clusterName Clients is empty."), req, resp)
		return
	}

	clusterConf, err := asmConfig.GetClusterConfig(ctx, opts)
	if err != nil {
		metricsLogger.Errorf("fetch graph  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	promClient, err := prometheus.NewClient(clusterConf.Spec.Prometheus)
	if err != nil {
		metricsLogger.Errorf("prometheus Newclient Clients err %v", err)
		srv.HandleError(err, req, resp)
		return

	}

	basicOptions := metrics.MetricsBasicQueryOptions{P8sAPI: promClient.Api}

	metricsOpts := &metrics.MetricsQueryOptions{MetricsBasicQueryOptions: basicOptions}
	if err := metricsOpts.Parse(req); err != nil {
		metricsLogger.Errorf("metricsOpts parse Clients err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	if err := metricsOpts.ParseQueryMetrics(req); err != nil {
		metricsLogger.Errorf("metrics Clients ParseQueryMetrics  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	if err := metricsOpts.ParseClients(req); err != nil {
		metricsLogger.Errorf("metrics OpParseClients  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}
	// 获取多个实例metrics
	maxResults := len(metricsOpts.SourceWorkloads)
	type clientMetrix struct {
		client string
		matrix map[string]interface{}
		err    error
	}

	var wg sync.WaitGroup
	results := make(chan *clientMetrix, maxResults)

	fetchClientsMetrics := func(opts *metrics.MetricsQueryOptions, clientMetrics chan *clientMetrix) {
		defer wg.Done()
		metrics, err := metrics.GetMetrics(ctx, opts)
		getResult := &clientMetrix{
			client: opts.SourceClient,
			matrix: metrics,
			err:    err,
		}

		clientMetrics <- getResult
	}
	for _, workload := range metricsOpts.SourceWorkloads {
		newOpts := metricsOpts.Copy()
		newOpts.SourceClient = workload

		wg.Add(1)

		go fetchClientsMetrics(&newOpts, results)

	}

	wg.Wait()
	Metrix := make(map[string]interface{})

	metricsLogger.Debugf("get clients metrics end length results==%d\n", len(results))
	close(results)
	for result := range results {
		if result.err != nil {
			metricsLogger.Errorf("Get mutiple clients metrics %s err %v", result.client, result.err)
			continue
		}

		Metrix[result.client] = result.matrix

	}
	metricsLogger.Debugf("get clients metrics end mmetrix==%d\n", len(Metrix))
	resp.WriteHeaderAndEntity(http.StatusOK, Metrix)
	return
}
func handleGetmutiplePosMetrics(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response

	clusterName := opts.GetClusterName()
	if clusterName == "" {
		metricsLogger.Error("clusterName is empty.")

		srv.HandleError(errors.New("clusterName is empty."), req, resp)
		return
	}

	clusterConf, err := asmConfig.GetClusterConfig(ctx, opts)
	if err != nil {
		metricsLogger.Errorf("fetch graph  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	promClient, err := prometheus.NewClient(clusterConf.Spec.Prometheus)
	if err != nil {
		metricsLogger.Errorf("prometheus Newclient err %v", err)
		srv.HandleError(err, req, resp)
		return

	}

	basicOptions := metrics.MetricsBasicQueryOptions{P8sAPI: promClient.Api}

	metricsOpts := &metrics.MetricsQueryOptions{MetricsBasicQueryOptions: basicOptions}
	if err := metricsOpts.Parse(req); err != nil {
		metricsLogger.Errorf("metricsOpts parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	if err := metricsOpts.ParseQueryMetrics(req); err != nil {
		metricsLogger.Errorf("metricsOptParseQueryMetricss  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	if err := metricsOpts.ParseInstances(req); err != nil {
		metricsLogger.Errorf("metricssaffOpParseInstances  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}
	// 获取多个实例metrics
	maxResults := len(metricsOpts.Instances)
	type podMetrix struct {
		pod    string
		matrix map[string]interface{}
		err    error
	}

	var wg sync.WaitGroup
	results := make(chan *podMetrix, maxResults)

	//metricsLogger.Debugf("before fetchPodMetrics,results length = %+v\n", len(results))
	fetchPodMetrics := func(opts *metrics.MetricsQueryOptions, podMetrics chan *podMetrix) {
		defer wg.Done()
		metrics, err := metrics.GetMetrics(ctx, opts)
		getResult := &podMetrix{
			pod:    opts.Pod,
			matrix: metrics,
			err:    err,
		}

		podMetrics <- getResult
	}
	for _, pod := range metricsOpts.Instances {
		newOpts := metricsOpts.Copy()
		newOpts.Pod = pod

		wg.Add(1)

		go fetchPodMetrics(&newOpts, results)

	}

	wg.Wait()
	Metrix := make(map[string]interface{})

	metricsLogger.Debugf("get pod metrics end length results==%d\n", len(results))
	close(results)
	for result := range results {
		if result.err != nil {
			metricsLogger.Errorf("Get mutiple pod  %s err %v", result.pod, result.err)
			continue
		}

		Metrix[result.pod] = result.matrix

	}
	metricsLogger.Debugf("get pod metrics end Metrix Metrix==%d\n", len(Metrix))
	resp.WriteHeaderAndEntity(http.StatusOK, Metrix)
	return
}
func handleGetWorkloadPaths(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response
	k8sClient := abcontext.Client(req.Request.Context())

	clusterName := opts.GetClusterName()
	if clusterName == "" {
		metricsLogger.Error("clusterName is empty.")
		srv.HandleError(errors.New("clusterName is empty."), req, resp)
		return
	}
	namespace := req.PathParameter(PathParameterNamespace)
	if namespace == "" {
		metricsLogger.Error("Error for path parameters namespace connot be empty")
		srv.HandleError(pkgerrors.New("Path parameter namespace cannot be empty"), req, resp)
		return
	}

	workload := req.PathParameter(Workload)
	if workload == "" {
		metricsLogger.Error("Error for path parameters workload or name connot be empty.")
		srv.HandleError(errors.New("Error parse parameters workload or name connot be empty."), req, resp)
	}

	path, err := metrics.GetWorkloadPaths(namespace, workload, k8sClient)
	if err != nil {
		metricsLogger.Error("Error getWorkloadPaths worload err")
		srv.HandleError(errors.New("Error getWorkloadPaths worload err"), req, resp)
	}

	metricsLogger.Debugf("ghandleGetWorkloadPaths:::%+v\n", path)
	resp.WriteHeaderAndEntity(http.StatusOK, path)
	return
}

func handleSetWorkloadPaths(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response
	k8sClient := abcontext.Client(req.Request.Context())

	clusterName := opts.GetClusterName()
	if clusterName == "" {
		metricsLogger.Error("clusterName is empty.")
		srv.HandleError(errors.New("clusterName is empty."), req, resp)
		return
	}
	namespace := req.PathParameter(PathParameterNamespace)
	if namespace == "" {
		metricsLogger.Error("Error for path parameters namespace connot be empty")
		srv.HandleError(pkgerrors.New("Path parameter namespace cannot be empty"), req, resp)
		return
	}

	workload := req.PathParameter(Workload)
	if workload == "" {
		metricsLogger.Error("Error for path parameters workload or name connot be empty.")
		srv.HandleError(errors.New("Error for path parameters workload or name connot be empty."), req, resp)
	}

	// 上传path需要解出来然后设置进去
	paths := &models.WorkloadPath{}
	if err := req.ReadEntity(paths); err != nil {
		srv.HandleError(err, req, resp)
		return
	}
	metricsLogger.Debugf("get upload paths==%+v\n", paths)

	deploy, err := metrics.SetWorkloadPaths(namespace, workload, paths, k8sClient)
	if err != nil {
		srv.HandleError(err, req, resp)
	}

	metricsLogger.Debugf("get handleGetWorkloadPaths==%s\n", deploy.Name)
	resp.WriteHeaderAndEntity(http.StatusOK, deploy)
	return
}

func handleGetmutipleWorkloads(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter(PathParameterNamespace)
	if namespace == "" {
		metricsLogger.Error("Error for path parameters namespace connot be empty")
		srv.HandleError(pkgerrors.New("Path parameter namespace cannot be empty"), req, resp)
		return
	}
	clusterName := opts.GetClusterName()
	if clusterName == "" {
		metricsLogger.Error("clusterName is empty.")
		srv.HandleError(errors.New("clusterName is empty."), req, resp)
		return
	}

	clusterConf, err := asmConfig.GetClusterConfig(ctx, opts)
	if err != nil {
		metricsLogger.Errorf("fetch graph  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	promClient, err := prometheus.NewClient(clusterConf.Spec.Prometheus)
	if err != nil {
		metricsLogger.Errorf("prometheus Newclient err %v", err)
		srv.HandleError(err, req, resp)
		return

	}
	basicOptions := metrics.MetricsBasicQueryOptions{P8sAPI: promClient.Api, K8sClient: k8sClient, Namespace: namespace}

	metricsOpts := &metrics.MetricsQueryOptions{MetricsBasicQueryOptions: basicOptions}

	if err := metricsOpts.Parse(req); err != nil {
		metricsLogger.Errorf("metricsOpts parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	viewModels, err := metrics.GetMutiWorkloadsByNamespace(ctx, metricsOpts)
	if err != nil {
		metricsLogger.Errorf("Error getMutiWorkloadsByNamespace for namespace %s: %s", namespace, err)
		srv.HandleError(err, req, resp)
		return
	}

	resp.WriteHeaderAndEntity(http.StatusOK, viewModels)
}

func handleGetmutipleWorkloadClients(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter(PathParameterNamespace)
	if namespace == "" {
		metricsLogger.Error("Error for path parameters namespace connot be empty")
		srv.HandleError(pkgerrors.New("Path parameter namespace cannot be empty"), req, resp)
		return
	}

	clusterName := opts.GetClusterName()
	if clusterName == "" {
		metricsLogger.Error("clusterName is empty.")
		srv.HandleError(errors.New("clusterName is empty."), req, resp)
		return
	}

	clusterConf, err := asmConfig.GetClusterConfig(ctx, opts)
	if err != nil {
		metricsLogger.Errorf("fetch graph  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	promClient, err := prometheus.NewClient(clusterConf.Spec.Prometheus)
	if err != nil {
		metricsLogger.Errorf("prometheus Newclient err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	basicOptions := metrics.MetricsBasicQueryOptions{P8sAPI: promClient.Api, K8sClient: k8sClient, Namespace: namespace}
	metricsOpts := &metrics.MetricsQueryOptions{MetricsBasicQueryOptions: basicOptions}
	if err := metricsOpts.ParseForWorkloadName(req); err != nil {
		metricsLogger.Errorf("metricsOptsParseForWorkloadName parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	if err := metricsOpts.Parse(req); err != nil {
		metricsLogger.Errorf("metricsOpts parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	viewModels, err := metrics.GetMutipleWorkloadClients(metricsOpts)
	if err != nil {
		metricsLogger.Errorf("Error fetch mutipleWorkload clients for namespace %s: %s", namespace, err)
		srv.HandleError(err, req, resp)
		return
	}

	resp.WriteHeaderAndEntity(http.StatusOK, viewModels)
}
func handleGetmutipleWorkloadPods(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter(PathParameterNamespace)
	if namespace == "" {
		metricsLogger.Error("Error for path parameters namespace connot be empty")
		srv.HandleError(pkgerrors.New("Path parameter namespace cannot be empty"), req, resp)
		return
	}

	clusterName := opts.GetClusterName()
	if clusterName == "" {
		metricsLogger.Error("clusterName is empty.")
		srv.HandleError(errors.New("clusterName is empty."), req, resp)
		return
	}

	clusterConf, err := asmConfig.GetClusterConfig(ctx, opts)
	if err != nil {
		metricsLogger.Errorf("fetch graph  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	promClient, err := prometheus.NewClient(clusterConf.Spec.Prometheus)
	if err != nil {
		metricsLogger.Errorf("prometheus Newclient err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	basicOptions := metrics.MetricsBasicQueryOptions{P8sAPI: promClient.Api, K8sClient: k8sClient, Namespace: namespace}
	metricsOpts := &metrics.MetricsQueryOptions{MetricsBasicQueryOptions: basicOptions}
	if err := metricsOpts.ParseForWorkloadName(req); err != nil {
		metricsLogger.Errorf("metricsOptsParseForWorkloadName parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	if err := metricsOpts.Parse(req); err != nil {
		metricsLogger.Errorf("metricsOpts parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	viewModels, err := metrics.GetMutipleWorkloadPods(metricsOpts)
	if err != nil {
		metricsLogger.Errorf("Error fetch mutipleWorkloadPods for namespace %s: %s", namespace, err)
		srv.HandleError(err, req, resp)
		return
	}

	resp.WriteHeaderAndEntity(http.StatusOK, viewModels)
}
func handleGetWorkloadMethods(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response
	k8sClient := abcontext.Client(req.Request.Context())

	namespace := req.PathParameter(PathParameterNamespace)
	if namespace == "" {
		metricsLogger.Error("Error for path parameters namespace connot be empty")
		srv.HandleError(pkgerrors.New("Path parameter namespace cannot be empty"), req, resp)
		return
	}

	clusterName := opts.GetClusterName()
	if clusterName == "" {
		metricsLogger.Error("clusterName is empty.")
		srv.HandleError(errors.New("clusterName is empty."), req, resp)
		return
	}

	clusterConf, err := asmConfig.GetClusterConfig(ctx, opts)
	if err != nil {
		metricsLogger.Errorf("fetch graph  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	promClient, err := prometheus.NewClient(clusterConf.Spec.Prometheus)
	if err != nil {
		metricsLogger.Errorf("prometheus Newclient err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	basicOptions := metrics.MetricsBasicQueryOptions{P8sAPI: promClient.Api, K8sClient: k8sClient, Namespace: namespace}
	metricsOpts := &metrics.MetricsQueryOptions{MetricsBasicQueryOptions: basicOptions}
	if err := metricsOpts.ParseForWorkloadName(req); err != nil {
		metricsLogger.Errorf("metricsOptsParseForWorkloadName parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	if err := metricsOpts.Parse(req); err != nil {
		metricsLogger.Errorf("metricsOpts parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	methods, err := metrics.GetMutipleWorkloadMethods(ctx, metricsOpts)
	if err != nil {
		metricsLogger.Errorf("Error GetMutipleWorkloadMethods workloads for namespace %s: %s", namespace, err)
		srv.HandleError(err, req, resp)
		return
	}

	resp.WriteHeaderAndEntity(http.StatusOK, methods)
}

func handleGetWorkloadApimetrics(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response

	clusterName := opts.GetClusterName()
	if clusterName == "" {
		metricsLogger.Error("apimetrics.clusterName is empty.")
		srv.HandleError(errors.New("apimetrics.clusterName is empty."), req, resp)
		return
	}

	clusterConf, err := asmConfig.GetClusterConfig(ctx, opts)
	if err != nil {
		metricsLogger.Errorf("apimetrics.fetch graph  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	promClient, err := prometheus.NewClient(clusterConf.Spec.Prometheus)
	if err != nil {
		metricsLogger.Errorf("apimetrics.prometheus Newclient err %v", err)
		srv.HandleError(err, req, resp)
		return

	}

	basicOptions := metrics.MetricsBasicQueryOptions{P8sAPI: promClient.Api}
	metricsOpts := &metrics.MetricsQueryOptions{MetricsBasicQueryOptions: basicOptions, IsDetails: true}
	if err := metricsOpts.Parse(req); err != nil {
		metricsLogger.Errorf("apimetrics.metricsOpts parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	if err := metricsOpts.ParseForWorkloadName(req); err != nil {
		metricsLogger.Errorf("apimetrics.metricsOpts parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}
	if err := metricsOpts.ParseForPathAndMethod(req); err != nil {
		metricsLogger.Errorf("apimetrics.metricsOpts parse err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	if err := metricsOpts.ParseQueryMetrics(req); err != nil {
		metricsLogger.Errorf("apimetrics.metricsOpts ParseQueryMetrics err %v", err)
		srv.HandleError(err, req, resp)
		return
	}

	metrics, err := metrics.GetMetrics(ctx, metricsOpts)
	if err != nil {
		metricsLogger.Errorf("apimetrics.Get WorkloadMetrics err %v", err)
		srv.HandleError(err, req, resp)
		return
	}
	resp.WriteHeaderAndEntity(http.StatusOK, metrics)
	return
}
