package handler

import (
	"net/http"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"

	"bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/hermes/pkg/virtualservice"
	"bitbucket.org/mathildetech/hermes/pkg/web"
)

// add/delete/update VirtualService filters by  namespace and name
func handleListVirtualService(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())

	namespace := request.PathParameter(PathParameterNamespace)

	query := ctx.Query
	list, err := virtualservice.GetVirtualServiceList(dyclient, namespace, query)
	if err != nil {
		//servicegraphLogger.Error("fetch destionation rule err", log.Err(err))
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, list)

}

func handleGetVirtualService(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())
	namespace := request.PathParameter(PathParameterNamespace)
	name := request.PathParameter(PathParameterName)

	rule, err := virtualservice.GetVirtualServiceDetail(dyclient, namespace, name)
	if err != nil {
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, rule)
}

func handleGetVirtualServiceByHost(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())
	namespace := request.PathParameter(PathParameterNamespace)
	name := request.PathParameter(PathParameterName)

	rule, err := virtualservice.GetVirtualServiceDetailByHost(dyclient, namespace, name)
	if err != nil {
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, rule)
}

func handleCreateVirtualService(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())

	namespace := request.PathParameter(PathParameterNamespace)

	vs := &unstructured.Unstructured{}
	if err := request.ReadEntity(vs); err != nil {
		srv.HandleError(err, request, response)
		return
	}

	var result *unstructured.Unstructured
	var err error
	if result, err = virtualservice.CreateVirtualService(dyclient, namespace, "jwttoken.name", vs); err != nil {
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func handleUpdateVirtualService(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())

	namespace := request.PathParameter(PathParameterNamespace)
	name := request.PathParameter(PathParameterName)

	vs := &unstructured.Unstructured{}
	if err := request.ReadEntity(vs); err != nil {
		srv.HandleError(err, request, response)
		return
	}
	var result *unstructured.Unstructured
	var err error

	if result, err = virtualservice.UpdateVirtualService(dyclient, namespace, name, vs); err != nil {
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)

}

func handleDeleteVirtualService(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())

	namespace := request.PathParameter(PathParameterNamespace)
	name := request.PathParameter(PathParameterName)

	if err := virtualservice.DeleteVirtualService(dyclient, namespace, name); err != nil {
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, ResponseOKMessage)
}
