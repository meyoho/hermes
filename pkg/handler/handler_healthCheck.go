package handler

import (
	"net/http"

	"bitbucket.org/mathildetech/hermes/pkg/caseMonitor"
	"bitbucket.org/mathildetech/hermes/pkg/web"
	"github.com/gorilla/websocket"

	asmContext "bitbucket.org/mathildetech/hermes/pkg/context"
	"bitbucket.org/mathildetech/hermes/pkg/safe"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}

var pusher = caseMonitor.NewPusher()

func init() {
	safe.Go(func() {
		pusher.Run()
	})
}

func handleHealthCheckWS(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	clusterName := request.PathParameter("cluster")
	if clusterName == "" {
		clusterName = request.QueryParameter("cluster")
	}

	conn, err := upgrader.Upgrade(response, request.Request, nil)
	if err != nil {
		srv.HandleError(err, request, response)
		return
	}

	config := asmContext.PathTokenConfig(request.Request.Context())
	if err != nil {
		srv.HandleError(err, request, response)
		return
	}

	pusher.ServeWs(clusterName, conn, config)

}
