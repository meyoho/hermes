package handler

import (
	"net/http"

	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"

	"bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/hermes/pkg/gateway"
	"bitbucket.org/mathildetech/hermes/pkg/web"
)

func handleListGateways(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())

	namespace := request.PathParameter(PathParameterNamespace)

	query := ctx.Query
	list, err := gateway.GetGatewayList(dyclient, namespace, query)
	if err != nil {
		//servicegraphLogger.Error("fetch destionation rule err", log.Err(err))
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, list)

}

func handleGetGateway(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())

	namespace := request.PathParameter(PathParameterNamespace)

	name := request.PathParameter(PathParameterName)

	gateWay, err := gateway.GetGatewayDetail(dyclient, namespace, name)
	if err != nil {
		//servicegraphLogger.Error("fetch destionation rule err", log.Err(err))
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, gateWay)
}

func handleCreateGateway(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())

	namespace := request.PathParameter(PathParameterNamespace)

	usGateway := &unstructured.Unstructured{}
	if err := request.ReadEntity(usGateway); err != nil {
		//servicegraphLogger.Error("fetch destionation rule err", log.Err(err))
		srv.HandleError(err, request, response)
		return
	}

	var result *unstructured.Unstructured
	var err error
	if result, err = gateway.CreateGateway(dyclient, namespace, "jweToken.Name", usGateway); err != nil {
		//servicegraphLogger.Error("fetch destionation rule err", log.Err(err))
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)
}

func handleUpdateGateway(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())

	namespace := request.PathParameter(PathParameterNamespace)
	name := request.PathParameter(PathParameterName)

	usGateway := &unstructured.Unstructured{}
	if err := request.ReadEntity(usGateway); err != nil {
		//servicegraphLogger.Error("fetch destionation rule err", log.Err(err))
		srv.HandleError(err, request, response)
		return
	}
	var result *unstructured.Unstructured
	var err error
	if result, err = gateway.UpdateGateway(dyclient, namespace, name, usGateway); err != nil {
		//servicegraphLogger.Error("fetch destionation rule err", log.Err(err))
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, result)

}

func handleDeleteGateway(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())
	namespace := request.PathParameter(PathParameterNamespace)
	name := request.PathParameter(PathParameterName)

	if err := gateway.DeleteGateway(dyclient, namespace, name); err != nil {
		//servicegraphLogger.Error("fetch destionation rule err", log.Err(err))
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, ResponseOKMessage)
}
