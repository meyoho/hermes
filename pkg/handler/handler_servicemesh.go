package handler

import (
	"bitbucket.org/mathildetech/hermes/pkg/auth/api"
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/servicemesh"
	"bitbucket.org/mathildetech/hermes/pkg/web"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

func handleListServiceMesh(ctx *web.Context) {
	req := ctx.Request
	resp := ctx.Response
	manager, err := servicemesh.NewServiceMeshManager(ctx)
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
	listOpt, err := common.RequestToListOptions(req)
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
	smList, err := manager.ListServiceMesh(listOpt)
	if err != nil {
		if k8serrors.IsNotFound(err) {
			resp.WriteAsJson(servicemesh.ServiceMeshListResp{Items: make([]servicemesh.ServiceMeshResp, 0)})
			return
		}
		ctx.Server.HandleError(err, req, resp)
		return
	}
	smr := &servicemesh.ServiceMeshListResp{
		ListMeta: smList.ListMeta,
		Items:    make([]servicemesh.ServiceMeshResp, 0, len(smList.Items)),
	}
	for _, sm := range smList.Items {
		statuses, err := manager.Statuses(&sm)
		if err != nil {
			ctx.Server.HandleError(err, req, resp)
			return
		}

		item := servicemesh.ServiceMeshResp{
			ServiceMesh: sm,
			Statuses:    statuses,
		}
		smr.Items = append(smr.Items, item)
	}
	resp.WriteAsJson(smr)
}

func handleGetServiceMesh(ctx *web.Context) {
	req := ctx.Request
	resp := ctx.Response
	manager, err := servicemesh.NewServiceMeshManager(ctx)
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}

	name := req.PathParameter(PathParameterName)
	sm, err := manager.GetServiceMesh(name)
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}

	statuses, err := manager.Statuses(sm)
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}

	item := servicemesh.ServiceMeshResp{
		ServiceMesh: *sm,
		Statuses:    statuses,
	}

	resp.WriteAsJson(item)
}

func handleGetServiceMeshStatus(ctx *web.Context) {
	req := ctx.Request
	resp := ctx.Response

	manager, err := servicemesh.NewServiceMeshManager(ctx)
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
	name := req.PathParameter(PathParameterName)
	sm, err := manager.GetServiceMesh(name)
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
	statuses, err := manager.Statuses(sm)
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
	resp.WriteAsJson(statuses)
}
func handleDeleteServiceMesh(ctx *web.Context) {
	req := ctx.Request
	resp := ctx.Response
	manager, err := servicemesh.NewServiceMeshManager(ctx)
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
	name := req.PathParameter(PathParameterName)
	sm, err := manager.GetServiceMesh(name)
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}

	err = manager.Cleanup(sm)
	manager.WaitCanResp()
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
}

func handleUpdateServiceMesh(ctx *web.Context) {

	req := ctx.Request
	resp := ctx.Response
	sm := &servicemesh.ServiceMesh{}
	if err := req.ReadEntity(sm); err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
	manager, err := servicemesh.NewServiceMeshManager(ctx)
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}

	if err := manager.ValidateServiceMesh(sm); err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}

	jweToken, err := api.ParseJWTFromHeader(req)
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
	if sm.Annotations[AnnotationsCreator] == "" {
		sm.Annotations[AnnotationsCreator] = jweToken.Name
	}
	var copiedSm = &servicemesh.ServiceMesh{}
	if err := common.JsonConvert(sm, copiedSm); err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
	if err := servicemesh.SetDefaultParams(copiedSm); err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
	if err := manager.Deploy(copiedSm); err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}

	var us *unstructured.Unstructured
	if us, err = manager.CreateOrUpdateServiceMesh(sm); err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
	manager.Ready()
	resp.WriteAsJson(us)
}

func handleCancelUpdateServiceMesh(ctx *web.Context) {
	req := ctx.Request
	resp := ctx.Response
	name := req.PathParameter(PathParameterName)
	manager, err := servicemesh.NewServiceMeshManager(ctx)
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
	if err := manager.Cancel(name, true); err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
}

func handleCreateDefaultParams(ctx *web.Context) {
	req := ctx.Request
	resp := ctx.Response
	clusterName := req.PathParameter(PathParameterName)

	getter, err := servicemesh.NewDefaultParamsGetter(clusterName)
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
	params, err := getter.Get()
	if err != nil {
		ctx.Server.HandleError(err, req, resp)
		return
	}
	resp.WriteAsJson(params)
}
