package handler

import (
	"context"
	"errors"
	"net/http"
	"sort"
	"strconv"

	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/hermes/pkg/asmConfig"
	"bitbucket.org/mathildetech/hermes/pkg/logging"
	"bitbucket.org/mathildetech/hermes/pkg/microservice"
	"bitbucket.org/mathildetech/hermes/pkg/models"
	"bitbucket.org/mathildetech/hermes/pkg/overview"
	"bitbucket.org/mathildetech/hermes/pkg/prometheus"
)

var (
	logger = logging.RegisterScope("handler.overview")
)

func handleGetOverviewResources(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	request := opts.Request
	response := opts.Response

	namespace := request.PathParameter(PathParameterNamespace)
	logger.Debugf("namespace is %s\n", namespace)
	k8sClient := abcontext.Client(request.Request.Context())
	dyclient, err := opts.GetDynamicClient(&microservice.GVK)

	resource, err := overview.OverviewResources(namespace, dyclient, k8sClient)
	if err != nil {
		logger.Errorf("handleGetOverviewResources.GetMicroServices err %+v \n", err)
		srv.HandleError(err, request, response)
		return
	}
	logger.Debugf("handleGetOverviewResources resource %+v  \n", resource)
	response.WriteHeaderAndEntity(http.StatusOK, resource)
}

func handleGetOverviewMonitor(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response

	prometheusConfig, err := asmConfig.GetPrometheusConfig(ctx, opts)
	if err != nil {
		srv.HandleError(err, req, resp)
		return
	}

	promClient, err := prometheus.NewClient(*prometheusConfig)
	if err != nil {

		servicegraphLogger.Errorf("prometheus Newclient err %v", err)
		srv.HandleError(err, req, resp)
		return

	}
	namespace := req.PathParameter(PathParameterNamespace)
	startTime, err := strconv.Atoi(req.QueryParameter(PathParameterStartTime))
	if err != nil {
		srv.HandleError(err, req, resp)
		return
	}
	endTime, err := strconv.Atoi(req.QueryParameter(PathParameterEndTime))
	if err != nil {
		srv.HandleError(err, req, resp)
		return
	}
	logger.Debugf("p8sconfig is %v ,startTime is %v ,endtime is %v \n", *prometheusConfig, startTime, endTime)
	if endTime-startTime <= 0 {
		srv.HandleError(errors.New("End time should be greater than start time"), req, resp)
		return
	}

	microDyclient, err := opts.GetDynamicClient(&microservice.GVK)
	rpses, mortalities, mses, err := overview.GetOverviewMonitor(promClient, namespace, startTime, endTime, microDyclient)
	if err != nil {
		logger.Errorf("fetch GetOverviewMonitor  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}
	if rpses != nil {
		sort.SliceStable(rpses, func(i, j int) bool { return rpses[i].Value > rpses[j].Value })
		if len(rpses) > 5 {
			rpses = rpses[0:5]
		}
	}
	if mortalities != nil {
		sort.SliceStable(mortalities, func(i, j int) bool { return mortalities[i].Value > mortalities[j].Value })
		if len(mortalities) > 5 {
			mortalities = mortalities[0:5]
		}
	}

	if mses != nil {
		sort.SliceStable(mses, func(i, j int) bool { return mses[i].Value > mses[j].Value })
		if len(mses) > 5 {
			mses = mses[0:5]
		}
	}

	logger.Debugf("handleGetOverviewMonitor mortalities is %+v , prses is %+v , ms is %+v \n", mortalities, rpses, mses)

	resp.WriteHeaderAndEntity(http.StatusOK, overview.Monitors{Mortality: &mortalities, Rps: &rpses, Ms: &mses})
}

func handleGetOverviewCircuits(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	request := opts.Request
	response := opts.Response

	namespace := request.PathParameter(PathParameterNamespace)
	prometheusConfig, err := asmConfig.GetPrometheusConfig(ctx, opts)
	if err != nil {
		srv.HandleError(err, request, response)
		return
	}

	promClient, err := prometheus.NewClient(*prometheusConfig)
	if err != nil {
		servicegraphLogger.Errorf("prometheus Newclient err %v", err)
		srv.HandleError(err, request, response)
		return
	}
	logger.Debugf("handleGetOverviewCircuits.namespace: %s ,,p8sURL: %v \n", namespace, *prometheusConfig)

	microDyclient, err := opts.GetDynamicClient(&microservice.GVK)
	circuits, err := overview.GetCircuits(promClient, namespace, microDyclient)
	if err != nil {
		logger.Errorf("handleGetOverviewResources.GetMicroServices err %+v \n", err)
		srv.HandleError(err, request, response)
		return
	}
	logger.Debugf("handleGetOverviewCircuits handleGetOverviewCircuits  \n")
	if circuits != nil {
		sort.SliceStable(circuits, func(i, j int) bool { return circuits[i].Time.Time.Unix() > circuits[j].Time.Time.Unix() })
		if len(circuits) > 5 {
			circuits = circuits[0:5]
		}
	}

	logger.Debugf("handleGetOverviewCircuits circuits top 5 is %+v  \n", len(circuits))
	response.WriteHeaderAndEntity(http.StatusOK, circuits)
}

func handleGetOverviewUntraffic(ctx context.Context, opts *models.QueryOptions) {
	srv := opts.Server
	req := opts.Request
	resp := opts.Response
	prometheusConfig, err := asmConfig.GetPrometheusConfig(ctx, opts)
	if err != nil {
		srv.HandleError(err, req, resp)
		return
	}

	promClient, err := prometheus.NewClient(*prometheusConfig)
	if err != nil {
		servicegraphLogger.Errorf("prometheus Newclient err %v", err)
		srv.HandleError(err, req, resp)
		return
	}
	namespace := req.PathParameter(PathParameterNamespace)
	startTime, err := strconv.Atoi(req.QueryParameter(PathParameterStartTime))
	if err != nil {
		srv.HandleError(err, req, resp)
		return
	}
	endTime, err := strconv.Atoi(req.QueryParameter(PathParameterEndTime))
	if err != nil {
		srv.HandleError(err, req, resp)
		return
	}
	microDyclient, err := opts.GetDynamicClient(&microservice.GVK)
	untraffics, err := overview.GetOvervieUntraffics(promClient, namespace, startTime, endTime, microDyclient)
	if err != nil {
		logger.Errorf("fetch GetOverviewMonitor  err %v", err)
		srv.HandleError(err, req, resp)
		return
	}
	sort.SliceStable(untraffics, func(i, j int) bool { return untraffics[i].ErrorRate > untraffics[j].ErrorRate })
	if len(untraffics) > 5 {
		untraffics = untraffics[0:5]
	}

	logger.Debugf("handleGetOverviewUntraffic  traffics top 5 is %+v  \n", untraffics)

	resp.WriteHeaderAndEntity(http.StatusOK, untraffics)
}
