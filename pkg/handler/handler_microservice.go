package handler

import (
	"bitbucket.org/mathildetech/hermes/pkg/logging"
	"bitbucket.org/mathildetech/hermes/pkg/microservice"
	v1 "k8s.io/api/apps/v1"
	"k8s.io/apimachinery/pkg/util/rand"
	"net/http"

	"bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/hermes/pkg/web"
	corev1 "k8s.io/api/core/v1"
)

var (
	microserviceLogger = logging.RegisterScope("handler.microservice")
)

//get microservice relation
func handleGetMicroserviceRelation(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())
	k8sClient := context.Client(request.Request.Context())
	namespace := request.PathParameter(PathParameterNamespace)
	name := request.PathParameter(PathParameterName)
	dsQuery := ctx.Query

	rule, err := microservice.GetMicroServiceRelationDetail(k8sClient, dyclient, dsQuery, namespace, name)
	if err != nil {
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, rule)
}

//get namespace deployment svc relation
func handleGetMicroserviceNamespaceRelation(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	k8sClient := context.Client(request.Request.Context())
	namespace := request.PathParameter(PathParameterNamespace)

	rule, err := microservice.GetNamespaceDeploymentServiceRelationDetail(k8sClient, namespace)
	if err != nil {
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, rule)
}

//get namespace svc relation deployment
func handleGetServiceRelationDeployment(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	k8sClient := context.Client(request.Request.Context())
	namespace := request.PathParameter(PathParameterNamespace)
	name := request.PathParameter(PathParameterName)

	rule, err := microservice.GetServiceRelationDeploymentDetail(k8sClient, name, namespace)
	if err != nil {
		srv.HandleError(err, request, response)
		return
	}
	response.WriteHeaderAndEntity(http.StatusOK, rule)
}

//create microservice asm svc
func handleCreateMicroserviceSvc(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())
	k8sClient := context.Client(request.Request.Context())
	namespace := request.PathParameter(PathParameterNamespace)
	msname := request.PathParameter(PathParameterName)

	spec := new(corev1.Service)
	if err := request.ReadEntity(spec); err != nil {
		srv.HandleError(err, request, response)
		return
	}
	result, err := microservice.CreateMicroServiceSvc(k8sClient, dyclient, namespace, msname, spec)
	if err != nil {
		srv.HandleError(err, request, response)
		return
	}

	response.WriteHeaderAndEntity(http.StatusCreated, result)
}

//update ms svc
func handleUpdateMicroserviceSvc(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	k8sClient := context.Client(request.Request.Context())
	namespace := request.PathParameter(PathParameterNamespace)
	msname := request.PathParameter(PathParameterName)

	svcname := request.PathParameter(PathParameterServiceName)
	spec := new(corev1.Service)
	if err := request.ReadEntity(spec); err != nil {
		srv.HandleError(err, request, response)
		return
	}
	result, err := microservice.UpdateMicroServiceSvc(k8sClient, namespace, msname, svcname, spec)
	if err != nil {
		srv.HandleError(err, request, response)
		return
	}

	response.WriteHeaderAndEntity(http.StatusAccepted, result)
}

//create microservice asm svc
func handleCreateMicroserviceDeployment(ctx *web.Context) {
	srv := ctx.Server
	request := ctx.Request
	response := ctx.Response

	dyclient := context.DynamicClient(request.Request.Context())
	microserviceLogger.Debugf("dyclient= %+v \n", dyclient)
	k8sClient := context.Client(request.Request.Context())
	microserviceLogger.Debugf("k8sClient= %+v \n", k8sClient)
	namespace := request.PathParameter(PathParameterNamespace)
	msname := request.PathParameter(PathParameterName)
	version := request.QueryParameter(Version)
	microserviceLogger.Debugf("namespace=%s, msname=%s version is %s \n", namespace, msname, version)

	deployment := new(v1.Deployment)
	if err := request.ReadEntity(deployment); err != nil {
		microserviceLogger.Debugf("request.ReadEntity err= %+v \n", err)
		srv.HandleError(err, request, response)
		return
	}

	// set deployment name and labels annotation.annotation is record the origin deployment name,and derived others
	annotations := deployment.Annotations
	var originName string
	if annotations != nil {
		originName = annotations[OriginName]
		//deployment.SetAnnotations(map[string]string{})
	}
	if originName == "" {
		originName = deployment.Name
	}
	annotations = map[string]string{}
	annotations[OriginName] = originName
	deployment.SetAnnotations(annotations)
	deployment.SetName(originName + "-" + rand.String(6))

	matchLabels := deployment.Spec.Selector.MatchLabels
	matchLabels[ServiceKey] = "deployment-" + deployment.Name
	deployment.Spec.Selector.MatchLabels = matchLabels

	labels := deployment.Spec.Template.GetLabels()
	versionLabled := labels[Version]
	labels[ServiceKey] = "deployment-" + deployment.Name
	if versionLabled != version {
		labels[Version] = version
		deployment.Spec.Template.SetLabels(labels)
	}
	microserviceLogger.Debugf("deployment=%+v \n", deployment)

	result, err := microservice.CreateMicroServiceDeployment(k8sClient, dyclient, namespace, msname, deployment)
	if err != nil {
		srv.HandleError(err, request, response)
		return
	}

	response.WriteHeaderAndEntity(http.StatusCreated, result)
}
