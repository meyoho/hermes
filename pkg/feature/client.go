package feature

import (
	"context"

	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/models"
)

const (
	prometheusFeatureName = "prometheus"
)

// Get return feature base on query token and clustername
func GetPrometheusFeature(ctx context.Context, opts *models.QueryOptions) (*PrometheusFeature, error) {

	dyClient, err := opts.GetDynamicClient(&GVK)
	if err != nil {
		return nil, err
	}

	unstruct, err := dyClient.Get(prometheusFeatureName, common.GetOptionsInCache)
	if err != nil {
		return nil, err
	}

	prometheusFeature := &PrometheusFeature{}

	err = prometheusFeature.FromUnstructured(unstruct)

	return prometheusFeature, err

}
