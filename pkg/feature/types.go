package feature

import (
	"encoding/json"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

const (
	// Group of feature as infrastructure.alauda.io
	Group = "infrastructure.alauda.io"
	// version of feature as v1alpha1
	Version = "v1alpha1"
	// kind of feature as Feature
	Kind = "Feature"
)

var (
	// GVK is the groupversionkind of feature
	GVK = schema.GroupVersionKind{
		Group:   Group,
		Version: Version,
		Kind:    Kind,
	}
)

// Feature for infrastructure CRD to storage prometheus info
type PrometheusFeature struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec FeatureSpec `json:"spec,omitempty"`
}

// FeatureSpec is the spec of Feature
type FeatureSpec struct {
	AccessInfo AccessInfo `json:"accessInfo,omitempty"`
}

// AccessInfo is the accessinfo of prometheus
type AccessInfo struct {
	PrometheusUrl      string `json:"prometheusUrl,omitempty"`
	Name               string `json:"name,omitempty"`
	Namespace          string `json:"namespace,omitempty"`
	PrometheusUser     string `json:"prometheusUser,omitempty"`
	PrometheusPassword string `json:"prometheusPassword,omitempty"`
}

// FromUnstructured converting feature from unstructured resources
func (feature *PrometheusFeature) FromUnstructured(r *unstructured.Unstructured) error {
	b, err := json.Marshal(r.Object)
	if err != nil {
		return err
	}
	if err := json.Unmarshal(b, feature); err != nil {
		return err
	}
	return nil
}
