package asmConfig

import (
	"context"

	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/models"
)

const (
	asmConfigName = "asm-cluster-config"
)

// Get return ClusterConfig base on query token and clustername
func Get(ctx context.Context, opts *models.QueryOptions) (*ClusterConfig, error) {

	dyClient, err := opts.GetDynamicClient(&GVK)
	if err != nil {
		return nil, err
	}

	unstruct, err := dyClient.Get(asmConfigName, common.GetOptionsInCache)
	if err != nil {
		return nil, err
	}

	asmconfig := &ClusterConfig{}

	err = asmconfig.FromUnstructured(unstruct)

	return asmconfig, err

}
