package asmConfig

import (
	"encoding/json"

	"bitbucket.org/mathildetech/hermes/pkg/logging"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"

	"bitbucket.org/mathildetech/hermes/pkg/feature"
)

const (
	Group   = "asm.alauda.io"
	Version = "v1beta1"
	Kind    = "ClusterConfig"
)

var (
	GVK = schema.GroupVersionKind{
		Group:   Group,
		Version: Version,
		Kind:    Kind,
	}
)

var (
	logger = logging.RegisterScope("asmconfig")
)

type Pilot struct {
	TraceSampling float64 `json:"tracesampling"`
}

type Elasticsearch struct {
	URL      string `json:"url"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type IPRangesMode string

const (
	IncludeMode IPRangesMode = "include"
	ExcludeMode IPRangesMode = "exclude"
)

type IPRanges struct {
	Mode   IPRangesMode `json:"mode"`
	Ranges []string     `json:"ranges"`
}

type JaegerCollector struct {
	Image string `json:"image"`
}

// ClusterConfigSpec defines the desired state of ClusterConfig
type ClusterConfigSpec struct {
	Pilot                            *Pilot           `json:"pilot,omitempty"`
	IPRanges                         *IPRanges        `json:"ipranges,omitempty"`
	Elasticsearch                    *Elasticsearch   `json:"elasticsearch,omitempty"`
	HiddenServiceNameInTracing       []string         `json:"hiddenServiceNamesInTracing,omitempty"`
	ForbiddenSidecarInjectNamespaces []string         `json:"forbiddenSidecarInjectNamespaces,omitempty"`
	JaegerURL                        string           `json:"jaegerURL,omitempty"`
	GrafanaURL                       string           `json:"grafanaURL,omitempty"`
	Prometheus                       PrometheusConfig `json:"_"`
}

// Auth provides authentication data for external services
type Auth struct {
	CAFile             string `json:"ca_file"`
	InsecureSkipVerify bool   `json:"insecure_skip_verify"`
	Password           string `json:"password"`
	Token              string `json:"token"`
	Type               string `json:"type"`
	Username           string `json:"username"`
}

// PrometheusConfig describes configuration of the Prometheus component
type PrometheusConfig struct {
	Auth Auth   `json:"auth,omitempty"`
	URL  string `json:"url,omitempty"`
}

type ClusterConfig struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec ClusterConfigSpec `json:"spec,omitempty"`
}

func (clusterconfig *ClusterConfig) FromUnstructured(r *unstructured.Unstructured) error {
	b, err := json.Marshal(r.Object)
	if err != nil {

		logger.Errorf("error ReleaseFromUnstructured json marshal : %v ,%v", err, r)
		return err
	}

	if err := json.Unmarshal(b, clusterconfig); err != nil {
		logger.Errorf("error ReleaseFromUnstructured Unmarshal: %v", err)
		return err
	}
	return nil
}

func (clusterconfig *ClusterConfig) SetPrometheus(prometheusFeature feature.AccessInfo) {

	clusterconfig.Spec.Prometheus = PrometheusConfig{
		Auth: Auth{
			Username:           prometheusFeature.PrometheusUser,
			Password:           prometheusFeature.PrometheusPassword,
			InsecureSkipVerify: true,
		},
		URL: prometheusFeature.PrometheusUrl,
	}

}
