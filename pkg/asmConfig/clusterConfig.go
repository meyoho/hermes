package asmConfig

import (
	"context"
	"reflect"

	"bitbucket.org/mathildetech/hermes/pkg/feature"
	"bitbucket.org/mathildetech/hermes/pkg/models"

	"github.com/pkg/errors"
)

const (

	// These constants are used for external services auth (Prometheus, Grafana ...) ; not for Kiali auth
	AuthTypeBasic  = "basic"
	AuthTypeBearer = "bearer"
	AuthTypeNone   = "none"
)

func GetClusterConfig(ctx context.Context, opts *models.QueryOptions) (*ClusterConfig, error) {

	clusterName := opts.GetClusterName()

	if len(clusterName) == 0 {
		return nil, errors.Errorf("cann't get clustername from request %v", opts.Request)
	}

	if CachedConfig.IsExist(clusterName) {
		return CachedConfig.Get(clusterName), nil
	}

	asmConfig, err := Get(ctx, opts)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	prometheusFeature, err := feature.GetPrometheusFeature(ctx, opts)

	if err != nil {
		return nil, errors.WithStack(err)
	}

	if reflect.ValueOf(prometheusFeature.Spec).IsZero() || reflect.ValueOf(prometheusFeature.Spec.AccessInfo).IsZero() {
		return nil, errors.Errorf("cann't get prometheusfeature from cluster %s", opts.GetClusterName())
	}

	asmConfig.SetPrometheus(prometheusFeature.Spec.AccessInfo)

	CachedConfig.Put(clusterName, *asmConfig, 600)

	return asmConfig, nil

}

// GetPrometheusConfig fetchs prometheusconfig from clusterconfig base on request clusterName.
func GetPrometheusConfig(ctx context.Context, opts *models.QueryOptions) (*PrometheusConfig, error) {
	asmConfig, err := GetClusterConfig(ctx, opts)
	if err != nil {
		return nil, errors.WithStack(err)
	}

	return &asmConfig.Spec.Prometheus, nil
}
