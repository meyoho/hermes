package overview

import (
	"time"

	"bitbucket.org/mathildetech/hermes/pkg/logging"
	"bitbucket.org/mathildetech/hermes/pkg/metrics"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
)

const (
	Group              = "asm.alauda.io"
	Version            = "v1beta2"
	Kind               = "MicroService"
	POD_STATUS_SUCCEED = "Succeed"
	POD_STATUS_RUNNING = "Running"
)

var (
	logger          = logging.RegisterScope("overview")
	microServiceGVK = &schema.GroupVersionKind{
		Group:   Group,
		Version: Version,
		Kind:    Kind,
	}
)

type StructMetric struct {
	ClusterName string `json:"clusterName,omitempty"`
	Container   string `json:"container,omitempty"`
	Endpoint    string `json:"endpoint,omitempty"`
	Instance    string `json:"instance,omitempty"`
	Job         string `json:"job,omitempty"`
	Namespace   string `json:"namespace,omitempty"`
	Pod         string `json:"pod,omitempty"`
	name        string `json:"__name__,omitempty"`
}
type ValueMetric struct {
	Time  time.Time `json:"time,omitempty"`
	Value float64   `json:"value,omitempty"`
}
type MetricResult struct {
	Metric []StructMetric `json:"metric,omitempty"`
	Values []ValueMetric  `json:"values,omitempty"`
}
type ResultDate struct {
	Result     MetricResult `json:"result,omitempty"`
	ResultType string       `json:"resultType,omitempty"`
}
type MetricOverview struct {
	Data   ResultDate `json:"data,omitempty"`
	Status string     `json:"status,omitempty"`
}

type Resources struct {
	Count        int   `json:"count"`
	VersionCount int32 `json:"versionCount"`
	Entry        int   `json:"entry"`
	Instance     int32 `json:"instance"`
	Unhealthy    int32 `json:"unhealthy"`
	Healthy      int32 `json:"healthy"`
}

type Monitors struct {
	Rps       *[]Attribute `json:"rps,omitempty"`
	Mortality *[]Attribute `json:"mortality,omitempty"`
	Ms        *[]Attribute `json:"ms,omitempty"`
	//Ms        []*MsAttribute `json:"ms,omitempty"`
}

type Attribute struct {
	Name    string  `json:"name,omitempty"`
	Version string  `json:"version,omitempty"`
	Value   float64 `json:"value,omitempty"`
}
type MsAttribute struct {
	Name    string                   `json:"name,omitempty"`
	Version string                   `json:"version,omitempty"`
	MsValue metrics.TimeStampMetrics `json:"msValue,omitempty"`
}

type Circuits struct {
	Service string  `json:"service,omitempty"`
	Version string  `json:"version,omitempty"`
	Time    v1.Time `json:"time,omitempty"`
}
type Traffics struct {
	Service   string  `json:"service,omitempty"`
	Version   string  `json:"version,omitempty"`
	ErrorRate float64 `json:"errorRate"`
}
