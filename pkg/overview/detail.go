package overview

import (
	"bitbucket.org/mathildetech/hermes/pkg/microservice"
	"bitbucket.org/mathildetech/hermes/pkg/prometheus"
	"context"
	"fmt"
	"github.com/juju/errors"
	prometheusv1 "github.com/prometheus/client_golang/api/prometheus/v1"
	"github.com/prometheus/common/model"
	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"sort"
	"strings"
	"time"
)

const (
	DestinationVersion = "destination_version"
	UNKNOWN            = "unknown"
)

var (
	clientMap                    = make(map[string]*prometheus.Client)
	EnvoyClusterOutlierDetection = "envoy_cluster_outlier_detection_ejections_active"
	IstioRequestsTotal           = "istio_requests_total"
	IstioRequestsDurationSum     = "istio_request_duration_seconds_sum"
	IstioRequestsDurationCount   = "istio_request_duration_seconds_count"

	step = time.Second * 60
)

func OverviewResources(namespace string, dyclient dynamic.NamespaceableResourceInterface, k8sclient kubernetes.Interface) (*Resources, error) {
	//get ms resource
	microServices, err := microservice.GetMicroServiceResourceList(dyclient, namespace)
	if err != nil {
		logger.Debugf("GetMicroServiceResourceList err is %v \n", err)
		return nil, err
	}
	logger.Debugf("getMicroServices.OverviewResources is %+v \n", microServices)
	count, versionCount, entry, podList, err := getResourcesCount(namespace, microServices, k8sclient)
	if err != nil && errors.IsNotFound(err) {
		logger.Debugf("getResourcesCount err is %v \n", err)
		return nil, err
	}
	var health int32
	var instance int32
	if len(podList) > 0 {
		instance = int32(len(podList))
		for _, pod := range podList {
			if pod.Status.Phase == POD_STATUS_SUCCEED || pod.Status.Phase == POD_STATUS_RUNNING {
				health++
			}
		}
	}

	return &Resources{
		Count:        count,
		VersionCount: versionCount,
		Entry:        entry,
		Instance:     instance,
		Unhealthy:    instance - health,
		Healthy:      health,
	}, nil
}

func getMonitorMetricsRPS(microServices []microservice.MicroService, ns string, startTime, endTime int, promClient *prometheus.Client) ([]Attribute, error) {
	var rpses []Attribute
	for _, rpsms := range microServices {
		msVector := model.Vector{}
		for _, svc := range rpsms.Spec.Services {
			rpsMetric, err := getRPSMetrics(ns, svc.Name, startTime, endTime, promClient)
			if err != nil {
				logger.Errorf("getGetOverviewMonitor Vector err is %v \n", err)
				return nil, err
			}
			msVector = append(msVector, rpsMetric...)
		}
		for _, sample := range msVector {
			s := sample.Metric
			sVersion, sVersionok := s[DestinationVersion]
			if !sVersionok || UNKNOWN == string(sVersion) {
				logger.Debugf("getMonitorMetrics err,metrics is %s \n", s.String())
				continue
			}
			if float64(sample.Value) > 0 {
				rps := Attribute{Name: rpsms.Name, Version: string(sVersion), Value: float64(sample.Value)}
				rpses = append(rpses, rps)
			}
		}
	}
	return rpses, nil
}

func GetOvervieUntraffics(promClient *prometheus.Client, ns string, startTime, endTime int, dyclient dynamic.NamespaceableResourceInterface) ([]Traffics, error) {
	microServices, err := microservice.GetMicroServiceResourceList(dyclient, ns)
	if err != nil {
		logger.Debugf("GetMicroServiceResourceList err is %v \n", err)
		return nil, err
	}
	logger.Debugf("getOvervieUntraffics.GetOverviewMonitor is %+v \n", microServices)

	untraffics, err := getUntraffics(microServices, ns, startTime, endTime, promClient)
	if err != nil {
		return nil, err
	}

	return untraffics, nil
}

func getUntraffics(microServices []microservice.MicroService, ns string, startTime, endTime int, promClient *prometheus.Client) ([]Traffics, error) {
	var untraffics []Traffics
	for _, ms := range microServices {
		for _, svc := range ms.Spec.Services {
			metricERR, err := getMortalitiesMetricsERR(ns, svc.Name, startTime, endTime, promClient)
			if err != nil {
				logger.Errorf("getUntraffics.getERR Vector err is %v \n", err)
				return nil, err
			}
			metricTOTAL, err := getMortalitiesMetricsTotal(ns, svc.Name, startTime, endTime, promClient)
			if err != nil {
				logger.Errorf("getUntraffics.getTotal Vector err is %v \n", err)
				return nil, err
			}

			for _, m := range metricERR {
				s := m.Metric
				sVersion, sVersionok := s[DestinationVersion]
				logger.Debugf("metricERR.m sVersion is %s,Err value is %v \n", string(sVersion), m.Value)
				if !sVersionok || UNKNOWN == string(sVersion) || float64(m.Value) == 0 {
					logger.Debugf("getUntraffics.getERR, err range syum %s \n", s.String())
					continue
				}
				for _, total := range metricTOTAL {
					t := total.Metric
					cVersion, cVersionok := t[DestinationVersion]
					logger.Debugf("metricTOTAL.t sVersion is %s,totalvalue is %v \n", string(sVersion), total.Value)
					if !cVersionok || UNKNOWN == string(cVersion) || float64(total.Value) == 0 {
						logger.Debugf("getUntraffics.getotal,range coubnt %s \n", t.String())
						continue
					}
					if string(sVersion) == string(cVersion) {
						untraffic := Traffics{Service: ms.Name, Version: string(sVersion), ErrorRate: float64(m.Value/total.Value) * 100}
						logger.Debugf("untraffic: %v \n", untraffic)
						untraffics = append(untraffics, untraffic)
					}
				}
			}
		}
	}
	return untraffics, nil
}
func getMonitorMetricsMortality(microServices []microservice.MicroService, ns string, startTime, endTime int, promClient *prometheus.Client) ([]Attribute, error) {
	var mortalities []Attribute
	for _, ms := range microServices {
		for _, svc := range ms.Spec.Services {
			metricERR, err := getMortalitiesMetricsERR(ns, svc.Name, startTime, endTime, promClient)
			if err != nil {
				logger.Errorf("getMortalitiesMetricsERR Vector err is %v \n", err)
				return nil, err
			}
			metricTOTAL, err := getMortalitiesMetricsTotal(ns, svc.Name, startTime, endTime, promClient)
			if err != nil {
				logger.Errorf("getMortalitiesMetricsTotal Vector err is %v \n", err)
				return nil, err
			}

			for _, m := range metricERR {
				s := m.Metric
				sVersion, sVersionok := s[DestinationVersion]
				logger.Debugf("mortalities.metricERR.m sVersion is %s,Err value is %v \n", string(sVersion), m.Value)
				if !sVersionok || UNKNOWN == string(sVersion) || float64(m.Value) == 0 {
					logger.Debugf("getMortalities.MetricsERR, metric: %s \n", s.String())
					continue
				}
				for _, total := range metricTOTAL {
					t := total.Metric
					cVersion, cVersionok := t[DestinationVersion]
					logger.Debugf("getMortalities.metricTOTAL.t sVersion is %s,totalvalue is %v \n", string(sVersion), total.Value)
					if !cVersionok || UNKNOWN == string(cVersion) || float64(total.Value) == 0 {
						logger.Debugf("getMortalitiesMetricsTotal,metric: %s \n", t.String())
						continue
					}
					if string(sVersion) == string(cVersion) {
						mortality := Attribute{Name: ms.Name, Version: string(sVersion), Value: float64(m.Value/total.Value) * 100}
						mortalities = append(mortalities, mortality)
					}
				}
			}
		}
	}
	return mortalities, nil
}
func getMonitorMetricsMs(microServices []microservice.MicroService, ns string, startTime, endTime int, promClient *prometheus.Client) ([]Attribute, error) {
	var mses []Attribute
	for _, ms := range microServices {
		for _, svc := range ms.Spec.Services {
			metricSum, err := getMsesMetricsSum(ns, svc.Name, startTime, endTime, promClient)
			if err != nil {
				logger.Errorf("getMsesMetricsSum Vector err is %v \n", err)
				return nil, err
			}
			metricCount, err := getMsesMetricsCount(ns, svc.Name, startTime, endTime, promClient)
			if err != nil {
				logger.Errorf("getMsesMetricsCount Vector err is %v \n", err)
				return nil, err
			}
			for _, sum := range metricSum {
				s := sum.Metric
				sVersion, sVersionok := s[DestinationVersion]
				if !sVersionok || UNKNOWN == string(sVersion) || float64(sum.Value) == 0 {
					logger.Debugf("getMsesMetricsSum, continue %s \n", s.String())
					continue
				}
				for _, count := range metricCount {
					c := count.Metric
					cVersion, cVersionok := c[DestinationVersion]
					if !cVersionok || UNKNOWN == string(cVersion) || float64(count.Value) == 0 {
						logger.Debugf("getMsesMetricsCount,continue %s \n", c.String())
						continue
					}
					if string(sVersion) == string(cVersion) {
						mmss := Attribute{Name: ms.Name, Version: string(sVersion), Value: float64(sum.Value / count.Value)}
						mses = append(mses, mmss)
					}
				}
			}
		}
	}
	return mses, nil
}

func GetOverviewMonitor(promClient *prometheus.Client, ns string, startTime, endTime int, dyclient dynamic.NamespaceableResourceInterface) ([]Attribute, []Attribute, []Attribute, error) {
	microServices, err := microservice.GetMicroServiceResourceList(dyclient, ns)
	if err != nil {
		logger.Debugf("GetMicroServiceResourceList err is %v \n", err)
		return nil, nil, nil, err
	}
	logger.Debugf("getMicroServices.GetOverviewMonitor is %+v \n", microServices)

	rpses, err := getMonitorMetricsRPS(microServices, ns, startTime, endTime, promClient)
	if err != nil {
		return nil, nil, nil, err
	}
	mortalities, err := getMonitorMetricsMortality(microServices, ns, startTime, endTime, promClient)
	if err != nil {
		return nil, nil, nil, err
	}
	mses, err := getMonitorMetricsMs(microServices, ns, startTime, endTime, promClient)
	if err != nil {
		return nil, nil, nil, err
	}

	return rpses, mortalities, mses, nil
}

func GetCircuits(promClient *prometheus.Client, ns string, dyclient dynamic.NamespaceableResourceInterface) ([]Circuits, error) {
	microServices, err := microservice.GetMicroServiceResourceList(dyclient, ns)
	if err != nil {
		logger.Debugf("GetMicroServiceResourceList err is %v \n", err)
		return nil, err
	}
	logger.Debugf("getMicroServices.GetCircuits is %+v \n", microServices)

	circuits := []Circuits{}
	metrics, err := getCircuitEjectionMetrics(ns, promClient)
	if err != nil {
		logger.Errorf("getEjectionNamespace Vector err is %v \n", err)
		return nil, err
	} else if len(metrics) <= 0 {
		logger.Debugf("get no circuits, len is %+v \n", len(metrics))
		return circuits, nil
	}
	logger.Debugf("circuits len is %+v \n", len(metrics))

	circuits = getEjectionMetrics(microServices, metrics)

	logger.Debugf("getGetCircuits circuits is %+v \n", circuits)
	return circuits, nil
}
func getEjectionMetrics(microServices []microservice.MicroService, metrics model.Matrix) []Circuits {
	circuits := []Circuits{}
	for _, ms := range microServices {
		// only have one svc
		if len(ms.Spec.Services) == 0 {
			continue
		}
		svcName := ms.Spec.Services[0].Name
		for _, s := range metrics {
			metric := s.Metric
			clusteName, clusteNameOk := metric["cluster_name"]
			if !clusteNameOk {
				logger.Debugf("skippingCircuits %s, getCircuitEjectionMetrics metric labels \n", s.Metric.String())
				continue
			}
			// metric include svcName service, and metrics values isnot empty
			if svcNameEqualMetricName(string(clusteName), svcName) && len(s.Values) > 0 {
				tests := s.Values
				sort.SliceStable(tests, func(i, j int) bool { return tests[i].Timestamp.Unix() < tests[j].Timestamp.Unix() })
				// first value must add
				t := tests[0]
				cs := getCircuit(string(clusteName), ms.Name, svcName, t)
				circuits = append(circuits, cs)
				//for _, value := range s.Values {
				for _, value := range tests {
					if value.Timestamp.Unix()-t.Timestamp.Unix() > 60 {
						cs := getCircuit(string(clusteName), ms.Name, svcName, value)
						circuits = append(circuits, cs)
					}
					t = value
				}
			}
		}
	}
	return circuits
}

func svcNameEqualMetricName(clusteName, svcName string) bool {
	if clusteName == "" || svcName == "" {
		return false
	}
	clusterAttributes := strings.Split(string(clusteName), "|")
	logger.Debugf("svcNameequalMetricName is %v \n", clusterAttributes)
	if clusterAttributes[3] != "" {
		fqdnValues := strings.Split(clusterAttributes[3], ".")
		logger.Debugf("svcNameequalMetricName.fqdnValues is %s \n", fqdnValues)
		if fqdnValues[0] != "" && svcName == fqdnValues[0] {
			return true
		}
	}
	return false
}
func getCircuit(clusteName, msName, svcName string, st model.SamplePair) Circuits {
	cs := Circuits{}
	if clusteName != "" {
		clusterAttributes := strings.Split(string(clusteName), "|")
		logger.Debugf("clusterAttributes is %s \n", clusterAttributes)
		if clusterAttributes[3] != "" {
			fqdnValues := strings.Split(clusterAttributes[3], ".")
			logger.Debugf("fqdnValues is %s \n", fqdnValues)
			if fqdnValues[0] != "" && svcName == fqdnValues[0] {
				var versionValue string
				if clusterAttributes[2] != "" {
					versionValue = clusterAttributes[2]
				}
				timeValue := st.Timestamp.Unix()
				cs = Circuits{
					Service: msName,
					Version: versionValue,
					Time:    v1.Time{Time: time.Unix(timeValue, 0)},
				}
				logger.Debugf("cs is %v \n", cs)
			}
		}
	}
	return cs
}
func getResourcesCount(namespace string, microServices []microservice.MicroService, k8sclient kubernetes.Interface) (msCount int, msVersionCount int32, msEntry int, podList []corev1.Pod, err error) {

	if nil != microServices && len(microServices) > 0 {
		msCount = len(microServices)
		for _, ms := range microServices {
			deployments := ms.Spec.Deployments
			if nil != deployments && len(deployments) > 0 {
				for _, deploy := range deployments {
					msVersionCount++
					deployment, err := k8sclient.AppsV1().Deployments(namespace).Get(deploy.Name, v1.GetOptions{})
					if err != nil {
						//get no deploy then do continue
						continue
					} else {
						var labelSelector string
						len := len(deployment.Spec.Template.Labels)
						i := 1
						for k, v := range deployment.Spec.Template.Labels {
							labelSelector += k + "=" + v
							if i != len {
								labelSelector += ","
							}
							i++
						}
						list, err := k8sclient.CoreV1().Pods(namespace).List(v1.ListOptions{LabelSelector: labelSelector})
						if err != nil && !errors.IsNotFound(err) {
							logger.Debugf("k8sclient.CoreV1.Pods ns is %s,labelSelector is %s ,err is %v \n", namespace, labelSelector, err)
							return 0, 0, 0, nil, err
						}

						podList = append(podList, list.Items...)
					}
				}
			}
			msServices := ms.Spec.Services
			if nil != msServices && len(msServices) > 0 {
				for _, svc := range msServices {
					if svc.Name != "" {
						msEntry++
					}
				}
			}
		}
	}
	logger.Debugf(" msCount is %d, msVersionCount is %d, msEntry is %d, \n", msCount, msVersionCount, msEntry)
	return msCount, msVersionCount, msEntry, podList, nil
}

func getCircuitEjectionMetrics(ns string, client *prometheus.Client) (model.Matrix, error) {
	query := fmt.Sprintf(`%s{container="istio-proxy",namespace="%s"} > 0`,
		EnvoyClusterOutlierDetection, ns)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	queryTime := time.Now()
	value, err := client.Api.QueryRange(ctx, query, prometheusv1.Range{
		Start: queryTime.AddDate(0, 0, -7),
		End:   queryTime,
		Step:  step,
	})
	if err != nil {
		return model.Matrix{}, err
	}
	switch t := value.Type(); t {
	case model.ValMatrix: // Instant Matrix
		return value.(model.Matrix), nil
	default:
		return model.Matrix{}, fmt.Errorf("No Matrix handling for type %v! \n", t)
	}
}

func getRPSMetrics(ns, svcName string, startTime, endTime int, client *prometheus.Client) (model.Vector, error) {
	query := fmt.Sprintf(`round(sum(increase(%s{reporter="source",destination_service_namespace="%s",destination_service_name="%s"} [%vs])) by(destination_version))`,
		IstioRequestsTotal,
		ns,
		svcName,
		endTime-startTime)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	value, err := client.Api.Query(ctx, query, time.Now())
	if err != nil {
		return model.Vector{}, err
	}
	switch t := value.Type(); t {
	case model.ValVector: // Instant Vector
		return value.(model.Vector), nil
	default:
		return model.Vector{}, fmt.Errorf("No Matrix handling for type %v! \n", t)
	}
}

func getMortalitiesMetricsERR(ns, svcName string, startTime, endTime int, client *prometheus.Client) (model.Vector, error) {
	query := fmt.Sprintf(`sum(increase(%s{reporter="source",destination_service_namespace="%s",destination_service_name="%s",response_code=~"[5|4].*"}[%vs])) by(destination_version)`,
		IstioRequestsTotal, ns, svcName, endTime-startTime)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	value, err := client.Api.Query(ctx, query, time.Unix(int64(endTime), 0))
	if err != nil {
		return model.Vector{}, err
	}
	switch t := value.Type(); t {
	case model.ValVector: // Instant Vector
		return value.(model.Vector), nil
	default:
		return model.Vector{}, fmt.Errorf("No Matrix handling for type %v! \n", t)
	}
}
func getMortalitiesMetricsTotal(ns, svcName string, startTime, endTime int, client *prometheus.Client) (model.Vector, error) {
	query := fmt.Sprintf(`sum(increase(%s{reporter="source",destination_service_namespace="%s",destination_service_name="%s"}[%vs])) by(destination_version)`,
		IstioRequestsTotal, ns, svcName, endTime-startTime)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	value, err := client.Api.Query(ctx, query, time.Unix(int64(endTime), 0))
	if err != nil {
		return model.Vector{}, err
	}
	switch t := value.Type(); t {
	case model.ValVector: // Instant Vector
		return value.(model.Vector), nil
	default:
		return model.Vector{}, fmt.Errorf("No Matrix handling for type %v! \n", t)
	}
}

func getMsesMetricsSum(ns, svcName string, startTime, endTime int, client *prometheus.Client) (model.Vector, error) {
	query := fmt.Sprintf(`sum(rate(%s{reporter="source",destination_service_namespace="%s",destination_service_name="%s"}[%vs])) by(destination_version)`,
		IstioRequestsDurationSum, ns, svcName, endTime-startTime)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	value, err := client.Api.Query(ctx, query, time.Now())
	if err != nil {
		return model.Vector{}, err
	}
	switch t := value.Type(); t {
	case model.ValVector: // Instant Vector
		return value.(model.Vector), nil
	default:
		return model.Vector{}, fmt.Errorf("No Matrix handling for type %v! \n", t)
	}
}
func getMsesMetricsCount(ns, svcName string, startTime, endTime int, client *prometheus.Client) (model.Vector, error) {
	query := fmt.Sprintf(`sum(rate(%s{reporter="source",destination_service_namespace="%s",destination_service_name="%s"}[%vs])) by(destination_version)`,
		IstioRequestsDurationCount, ns, svcName, endTime-startTime)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	value, err := client.Api.Query(ctx, query, time.Now())
	if err != nil {
		return model.Vector{}, err
	}
	switch t := value.Type(); t {
	case model.ValVector: // Instant Vector
		return value.(model.Vector), nil
	default:
		return model.Vector{}, fmt.Errorf("No Matrix handling for type %v! \n", t)
	}
}
