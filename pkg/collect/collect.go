package collect

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"sync"
)

type Metrics struct {
	metricName       string
	metricAppVal     string
	metricVersionVal string
	metrics          map[string]*prometheus.Desc
	mutex            sync.Mutex
}

const ProductKey = "product"
const MetricKey = "metricname"
const VersionKey = "version"

func newGlobalMetric(metricName string, docString string, labels []string) *prometheus.Desc {
	return prometheus.NewDesc(metricName, docString, labels, nil)
}

func NewMetrics(metricname, appval, version string) *Metrics {
	return &Metrics{
		metrics: map[string]*prometheus.Desc{
			metricname: newGlobalMetric(metricname, fmt.Sprintf("The description of %s", metricname),
				[]string{ProductKey, VersionKey, MetricKey}),
		},
		metricName:       metricname,
		metricAppVal:     appval,
		metricVersionVal: version,
	}
}

func (c *Metrics) Describe(ch chan<- *prometheus.Desc) {
	for _, m := range c.metrics {
		ch <- m
	}
}

func (c *Metrics) Collect(ch chan<- prometheus.Metric) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	mockGaugeMetricData := c.GetMetricData()
	for key, currentValue := range mockGaugeMetricData {
		ch <- prometheus.MustNewConstMetric(c.metrics[c.metricName],
			prometheus.GaugeValue, float64(currentValue), c.metricAppVal, c.metricVersionVal, key)
	}
}

func (c *Metrics) GetMetricData() (mockGaugeMetricData map[string]int) {
	cjob := GetCronJob()
	totalms, totalwk := cjob.GetMetric()
	mockGaugeMetricData = map[string]int{
		"microservice": totalms,
		"workload":     totalwk,
	}
	return
}
