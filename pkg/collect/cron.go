package collect

import (
	"alauda.io/asm-controller/pkg/apis/asm/v1beta2"
	"bitbucket.org/mathildetech/hermes/pkg/clusterRegister"
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/config"
	"bitbucket.org/mathildetech/hermes/pkg/logging"
	"errors"
	"github.com/robfig/cron"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"sync"
)

var once sync.Once
var cjob *CronJob

var logger = logging.RegisterScope("collect")

const (
	TokenKey = "token"
)

type CronJob struct {
	cronjob *cron.Cron
	jobtime string
	totalms int
	totalwk int
	mutex   sync.Mutex
}

var (
	msGVR = schema.GroupVersionResource{
		Group:    "asm.alauda.io",
		Version:  "v1beta2",
		Resource: "microservices",
	}

	clusGVR = schema.GroupVersionResource{
		Group:    "clusterregistry.k8s.io",
		Version:  "v1alpha1",
		Resource: "clusters",
	}
)

func GetCronJob() *CronJob {
	once.Do(func() {
		cjob = &CronJob{
			cronjob: cron.New(),
			jobtime: "*/30 * * * * *",
		}
	})
	return cjob
}

func (c *CronJob) AddFunc() error {
	err := c.cronjob.AddFunc(c.jobtime, CollectJob)
	if err != nil {
		return err
	}
	return nil
}

func (c *CronJob) Start() {
	c.cronjob.Start()
}

func (c *CronJob) Stop() {
	c.cronjob.Stop()
}

func (c *CronJob) SetMetric(totalms, totalwk int) {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	c.totalms = totalms
	c.totalwk = totalwk
}

func (c *CronJob) GetMetric() (totalms, totalwk int) {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	return c.totalms, c.totalwk
}

func GetClusterNamespaces(cfg *rest.Config) ([]unstructured.Unstructured, error) {
	k8sclient, err := kubernetes.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}
	listop := metav1.ListOptions{
		//change to constant
		LabelSelector: "istio-injection=enabled",
	}
	nslist, err := k8sclient.CoreV1().Namespaces().List(listop)
	dyclient, err := dynamic.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}
	msClient := dyclient.Resource(msGVR)
	var totalmslit []unstructured.Unstructured
	for _, nsitem := range nslist.Items {
		mslist, err := msClient.Namespace(nsitem.Name).List(metav1.ListOptions{})
		logger.Debugf("msClient.Namespace %s ", nsitem.Name)
		if err != nil {
			continue
		}
		for _, ms := range mslist.Items {
			totalmslit = append(totalmslit, ms)
		}
	}

	return totalmslit, nil
}

func clusterToConfig(c *clusterRegister.Cluster) (*rest.Config, error) {
	if len(c.Spec.KubernetesAPIEndpoints.ServerEndpoints) == 0 || c.Spec.KubernetesAPIEndpoints.ServerEndpoints[0].ServerAddress == "" {
		return nil, errors.New("could not found an avaliable server endpoint from cluster config.")
	}

	cfg := &rest.Config{
		Host: c.Spec.KubernetesAPIEndpoints.ServerEndpoints[0].ServerAddress,
	}
	if len(c.Spec.KubernetesAPIEndpoints.CABundle) == 0 {
		cfg.TLSClientConfig.Insecure = true
	} else {
		cfg.TLSClientConfig.CAData = c.Spec.KubernetesAPIEndpoints.CABundle
	}

	if c.Spec.AuthInfo.Controller == nil || c.Spec.AuthInfo.Controller.Name == "" ||
		c.Spec.AuthInfo.Controller.Namespace == "" {
		return nil, errors.New("Secret Reference is not provided fully in the cluster config.")
	}

	secret := &corev1.Secret{}
	kubeClient, err := GetInClusterKubeClient()
	if err != nil {
		return nil, err
	}
	secret, err = kubeClient.CoreV1().Secrets(c.Spec.AuthInfo.Controller.Namespace).Get(c.Spec.AuthInfo.Controller.Name, common.GetOptionsInCache)
	if err != nil {
		return nil, err
	}
	token, exist := secret.Data[TokenKey]
	if !exist {
		return nil, errors.New("Auth secret data is invalid, no token field found.")
	}
	cfg.BearerToken = string(token[:])
	return cfg, nil
}

func ConvertClusterRegistryUsToRestConfigList(obj *unstructured.UnstructuredList) (map[string]*rest.Config, error) {
	result := make(map[string]*rest.Config, len(obj.Items))
	for _, us := range obj.Items {
		cluster := &clusterRegister.Cluster{}
		if err := common.JsonConvert(&us, cluster); err != nil {
			logger.Errorf("cluster %s FromUnstructured error  %v", us.GetName(), err)
			continue
		}
		cfg, err := clusterToConfig(cluster)
		if err != nil {
			logger.Errorf("generate rest config from cluster %s failed: %+v\n", cluster.Name, err)
		}
		result[cluster.Name] = cfg
	}

	return result, nil
}

func GetGlobalClusterRegCfg() (map[string]*rest.Config, error) {
	dyclient, err := GetInClusterDyClient()
	if err != nil {
		return nil, err
	}

	clustersClient := dyclient.Resource(clusGVR)
	crlist, err := clustersClient.Namespace(config.AlaudaNamespace()).List(metav1.ListOptions{})

	return ConvertClusterRegistryUsToRestConfigList(crlist)
}

func GetInClusterKubeClient() (kubernetes.Interface, error) {
	cfg, err := rest.InClusterConfig()
	if err != nil {
		return nil, err
	}
	k8sclient, err := kubernetes.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}

	return k8sclient, nil
}

func GetInClusterDyClient() (dynamic.Interface, error) {
	cfg, err := rest.InClusterConfig()
	if err != nil {
		return nil, err
	}
	dyclient, err := dynamic.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}

	return dyclient, nil
}

func CollectJob() {
	cmmap, err := GetGlobalClusterRegCfg()
	if err != nil {
		return
	}

	//get all cluster ns
	var clusterTotalMsLit []unstructured.Unstructured
	for clustername, rcfg := range cmmap {
		uslist, err := GetClusterNamespaces(rcfg)
		if err != nil {
			logger.Errorf("GetClusterNamespaces error  %v", err)
			continue
		}
		logger.Infof("get cluster %s ms list %d", clustername, len(uslist))
		for _, ms := range uslist {
			clusterTotalMsLit = append(clusterTotalMsLit, ms)
		}
	}

	var totalwk int
	for _, us := range clusterTotalMsLit {
		ms := &v1beta2.MicroService{}
		if err := common.JsonConvert(&us, ms); err != nil {
			logger.Errorf("cluster %s FromUnstructured error  %v", us.GetName(), err)
			continue
		}
		totalwk = totalwk + len(ms.Spec.Deployments)
	}

	cjob := GetCronJob()
	cjob.SetMetric(len(clusterTotalMsLit), totalwk)
	//judge every cluster ns
	logger.Infof("get total ms list %d,total workload %d", len(clusterTotalMsLit), totalwk)
}
