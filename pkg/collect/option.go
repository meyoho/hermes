package collect

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/hermes/pkg/config"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

const (
	metricsName        = "product-metric"
	defaultMetricsName = "product_metric"
)

const (
	configMetricName = "product.metric_name"
)

type CollectOptioner struct {
	metricsName string
}

func NewCollectOptions() *CollectOptioner {
	return &CollectOptioner{
		metricsName: defaultMetricsName,
	}
}

func (o *CollectOptioner) AddFlags(fs *pflag.FlagSet) {
	if o == nil {
		return
	}
	fs.String(metricsName, o.metricsName,
		"set Prometheus metrics name")
	_ = viper.BindPFlag(configMetricName, fs.Lookup(metricsName))
}
func (o *CollectOptioner) ApplyFlags() []error {
	var errs []error
	o.metricsName = viper.GetString(configMetricName)
	return errs
}

func (o *CollectOptioner) ApplyToServer(server server.Server) error {
	metrics := NewMetrics(o.metricsName, "asm", config.ServiceMeshChartVersionEnv())
	registry := prometheus.NewRegistry()
	registry.MustRegister(metrics)

	server.Container().Handle("/product_metric", promhttp.HandlerFor(registry, promhttp.HandlerOpts{}))
	return nil
}
