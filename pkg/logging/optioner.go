package logging

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/spf13/pflag"
	"net/http"
)

type LoggingOptioner struct {
}

func (o *LoggingOptioner) AddFlags(fs *pflag.FlagSet) {

}
func (o *LoggingOptioner) ApplyFlags() []error {
	return nil

}
func (o *LoggingOptioner) ApplyToServer(server server.Server) error {
	server.Container().Handle("/debug/logging", http.HandlerFunc(loggingHandler))
	return nil
}
