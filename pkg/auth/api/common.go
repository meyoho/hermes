// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package api

import (
	"bitbucket.org/mathildetech/hermes/pkg/logging"
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/emicklei/go-restful"
	pkgerrors "github.com/pkg/errors"
	"strings"
)

var (
	logger = logging.RegisterScope("auth")
)

const (
	ConfigMap  = "ConfigMap"
	Deployment = "Deployment"

	HeadParameterAuthorization = "Authorization"
)

type JWEToken struct {
	Issuer        string      `json:"iss"`
	Subject       string      `json:"sub"`
	Audience      string      `json:"aud"`
	Expiry        int         `json:"exp"`
	IssuedAt      int         `json:"iat"`
	Nonce         string      `json:"nonce"`
	Email         string      `json:"email"`
	EmailVerified bool        `json:"email_verified"`
	Name          string      `json:"name"`
	Groups        []string    `json:"groups"`
	Ext           jwtTokenExt `json:"ext"`
	MetadataName  string
}

type jwtTokenExt struct {
	IsAdmin bool   `json:"is_admin"`
	ConnID  string `json:"conn_id"`
}

func ParseJWTFromHeader(request *restful.Request) (*JWEToken, error) {
	var rawToken string
	authorization := strings.TrimSpace(request.HeaderParameter(HeadParameterAuthorization))
	if authorization == "" {
		return nil, errors.New("Authentication head does not exist")
	}
	logger.Debugf("authorization header: %s", authorization)
	bearerPrefix := "bearer "
	if strings.HasPrefix(strings.ToLower(authorization), bearerPrefix) {
		rawToken = strings.TrimSpace(authorization[len(bearerPrefix):])
	}
	return ParseJWT(rawToken)
}

func ParseJWT(rawToken string) (*JWEToken, error) {
	var token JWEToken

	if rawToken == "" {
		return nil, errors.New("Authentication head is invalid")
	}
	parts := strings.Split(rawToken, ".")
	if len(parts) < 2 {
		return nil, fmt.Errorf("oidc: malformed jwt, expected 3 parts got %d", len(parts))
	}
	payload, err := base64.RawURLEncoding.DecodeString(parts[1])
	if err != nil {
		return nil, fmt.Errorf("oidc: malformed jwt payload: %v", err)
	}
	if err := json.Unmarshal(payload, &token); err != nil {
		logger.Error(pkgerrors.WithStack(err))
		return nil, err
	}
	token.MetadataName = GetUserMetadataName(token.Email)

	return &token, nil
}

func GetUserMetadataName(userID string) string {
	md5Ctx := md5.New()
	md5Ctx.Write([]byte(strings.TrimSpace(userID)))
	cipherStr := md5Ctx.Sum(nil)
	return hex.EncodeToString(cipherStr)
}
