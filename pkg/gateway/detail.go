package gateway

import (
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/dynamic"
)

func GetGatewayDetail(dyclient dynamic.NamespaceableResourceInterface, namespace string, name string) (*unstructured.Unstructured, error) {
	return dyclient.Namespace(namespace).Get(name, common.GetOptionsInCache)
}
