package gateway

import (
	dataselect "bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/dynamic"
)

type GatewayList struct {
	ListMeta common.ListMeta             `json:"listMeta"`
	Items    []unstructured.Unstructured `json:"items"`
	Errors   []error                     `json:"errors"`
}

func GetGatewayList(dyclient dynamic.NamespaceableResourceInterface, namespace string, dsQuery *dataselect.Query) (*GatewayList, error) {
	obj, err := dyclient.Namespace(namespace).List(common.ListEverything)
	if err != nil {
		return nil, err
	}
	items := obj.Items
	// filter using standard filters
	itemCells := dataselect.ToObjectCellSlice(items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, dsQuery)
	result := dataselect.FromCellToUnstructuredSlice(itemCells)

	list := &GatewayList{
		ListMeta: common.ListMeta{
			TotalItems: filteredTotal,
		},
		Items:  result,
		Errors: []error{},
	}
	return list, nil
}
