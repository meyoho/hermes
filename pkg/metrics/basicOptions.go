package metrics

import (
	"strconv"
	"strings"
	"time"

	pkgerrors "github.com/pkg/errors"
	prometheus_v1 "github.com/prometheus/client_golang/api/prometheus/v1"

	restful "github.com/emicklei/go-restful"
	client "k8s.io/client-go/kubernetes"
)

// QueryOptions as a interface for all query options
type QueryOptionsInterface interface {
	ParseQueryMetrics(req *restful.Request) error
	buildQueryStrings(q queryBasicMetric) (matricQuery map[string]interface{})
	getRange() prometheus_v1.Range
	getP8sAPI() prometheus_v1.API
	getQueryMetrics() []queryBasicMetric
	getQueryDateTime() time.Time
}

// MetricsBasicQueryOptions as object for metrics query parameters
type MetricsBasicQueryOptions struct {
	P8sAPI       prometheus_v1.API
	K8sClient    client.Interface
	Namespace    string
	StartTime    int
	EndTime      int
	Step         int
	QueryMetrics []queryBasicMetric
	Quantiles    []string
}

// Parse restful request to metricsquery options
func (opts *MetricsBasicQueryOptions) Parse(req *restful.Request) error {

	startTime, err := strconv.Atoi(req.QueryParameter(PathParameterStartTime))
	if err != nil {

		metricsLogger.Error("Error for PathParameterStartTime connot be empty")
		return err
	}
	opts.StartTime = startTime

	endTime, err := strconv.Atoi(req.QueryParameter(PathParameterEndTime))
	if err != nil {
		metricsLogger.Error("Error for PathParameterEndTime connot be empty")
		return err
	}
	opts.EndTime = endTime

	// default step
	opts.Step = DefaultStep

	stepStr := req.QueryParameter(PathParameterStep)
	if len(stepStr) != 0 {
		step, err := strconv.Atoi(stepStr)
		if err != nil {
			metricsLogger.Errorf("prase step err : %v", err)
			step = DefaultStep
		}
		opts.Step = step
	}

	namespace := req.PathParameter(PathParameterNamespace)
	if namespace == "" {
		namespace = req.QueryParameter(PathParameterNamespace)
	}

	if namespace == "" {
		// for edge skip
		namespace = req.QueryParameter(PathParameterSourceNamespace)
	}

	if namespace == "" {
		metricsLogger.Error("Error for path parameters namespace connot be empty")
		return pkgerrors.New("Error for path parameters namespace connot be empty")
	}
	opts.Namespace = namespace

	return nil
}

// Parse restful request to metricsquery options
func (opts *MetricsBasicQueryOptions) ParseQueryMetrics(req *restful.Request) error {

	var metricsQueries []queryBasicMetric
	quantilesRequired := false

	quantiles := req.QueryParameter(QueryParemeterQuantiles)
	metricsLogger.Debugf("quantiles===%s\n", quantiles)
	if quantiles != "" {
		opts.Quantiles = strings.Split(quantiles, ",")
	}

	metrics := req.QueryParameter(QueryParameterMetrics)
	metricsLogger.Debugf("metrics == == %s\n", metrics)
	if metrics != "" {
		for _, metric := range strings.Split(metrics, ",") {

			if queryMetric, ok := istioMetrics[metric]; ok {
				metricsQueries = append(metricsQueries, queryMetric)
			}

			//if response_time required and quantiles is not allowed empty.
			if metric == MetricsResponseTime {
				quantilesRequired = true
			}
		}

	} else {
		// default fetching all the metrics

		for _, queryMetrics := range istioMetrics {
			metricsQueries = append(metricsQueries, queryMetrics)

		}

		opts.Quantiles = []string{"avg", "0.5", "0.95", "0.99"}

	}

	if quantilesRequired && len(opts.Quantiles) == 0 {
		return pkgerrors.New("quantiles is required")
	}

	opts.QueryMetrics = metricsQueries

	return nil

}

func (opts *MetricsBasicQueryOptions) getRange() prometheus_v1.Range {
	return prometheus_v1.Range{
		End:   time.Unix(int64(opts.EndTime), 0),
		Start: time.Unix(int64(opts.StartTime), 0),
		Step:  time.Duration(opts.Step) * time.Second,
	}

}

func (opts *MetricsBasicQueryOptions) getP8sAPI() prometheus_v1.API {
	return opts.P8sAPI
}

func (opts *MetricsBasicQueryOptions) getQueryMetrics() []queryBasicMetric {
	return opts.QueryMetrics
}

func (opts *MetricsBasicQueryOptions) getQueryDateTime() time.Time {
	return time.Unix(int64(opts.EndTime), 0)
}
