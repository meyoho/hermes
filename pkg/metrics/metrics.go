package metrics

import (
	"context"
	"fmt"
	v1 "k8s.io/api/apps/v1"
	"math"
	"strings"
	"sync"
	"time"

	"bitbucket.org/mathildetech/hermes/pkg/common"
	"bitbucket.org/mathildetech/hermes/pkg/models"
	"github.com/prometheus/common/model"

	wd "bitbucket.org/mathildetech/hermes/pkg/workload"
	prom_v1 "github.com/prometheus/client_golang/api/prometheus/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	client "k8s.io/client-go/kubernetes"
)

const (
	WorkloadTypeMetrics          = "FromMetrics"
	DataRoundSignificant float64 = 0.001
	ISTIO_REQUEST_TOTAL          = "istio_requests_total"
)

func GetWorkloadsByNamespace(ctx context.Context, opts *MetricsQueryOptions) ([]*models.ViewWorkload, error) {

	nsQuery := common.NewSameNamespaceQuery(opts.Namespace)

	workloadsChan := make(chan []*wd.Workload)
	errChan := make(chan error)

	workloadsFromMetricsChanDestination := make(chan []string)

	go func() {
		defer close(workloadsChan)
		workloads, err := wd.FetchWorkloads(opts.K8sClient, nsQuery, "")
		if err != nil {
			errChan <- err
		}
		workloadsChan <- workloads
	}()
	workloadsFromMetricsChanSource := make(chan []string)
	go func() {
		defer close(workloadsFromMetricsChanSource)
		fromMetricsSource, err := GetNamespaceWorkloadBySource(opts.getP8sAPI(), opts.Namespace, opts.StartTime, opts.EndTime)
		if err != nil {
			errChan <- err
		}
		workloadsFromMetricsChanSource <- fromMetricsSource
	}()

	go func() {
		defer close(workloadsFromMetricsChanDestination)
		fromMetricsDestination, err := GetNamespaceWorkloadByDestination(opts.getP8sAPI(), opts.Namespace, opts.StartTime, opts.EndTime)
		if err != nil {
			errChan <- err
		}
		workloadsFromMetricsChanDestination <- fromMetricsDestination
	}()

	workloads := <-workloadsChan
	viewModels := []*models.ViewWorkload{}

	for _, workload := range workloads {
		viewModel := &models.ViewWorkload{}
		viewModel.Parse(workload)
		viewModels = append(viewModels, viewModel)
	}
	workloadsFromMetrics := <-workloadsFromMetricsChanSource
	workloadsFromMetricsDestination := <-workloadsFromMetricsChanDestination
	workloadsFromMetrics = append(workloadsFromMetrics, workloadsFromMetricsDestination...)

	var found bool
	for _, workload := range workloadsFromMetrics {
		found = false
		for _, viewModel := range viewModels {
			if workload == viewModel.Name {
				found = true
				break
			}
		}

		if found == false {
			viewModel := &models.ViewWorkload{
				Name: workload,
				Type: WorkloadTypeMetrics,
			}
			viewModels = append(viewModels, viewModel)
		}

	}

	return viewModels, nil
}

func GetMetrics(ctx context.Context, opts QueryOptionsInterface) (map[string]interface{}, error) {

	var bounds = opts.getRange()
	type resultHolder struct {
		mutipleRanges map[string]*RangeMetrix
		ranges        RangeMetrix
		counts        CountMetrix
		definition    queryBasicMetric
	}
	var wg sync.WaitGroup

	fetchQueryCount := func(queryMetric queryBasicMetric, resultChan chan resultHolder) {
		defer wg.Done()

		querymap := opts.buildQueryStrings(queryMetric)

		counts := make(CountMetrix)

		for metricName, queryinterface := range querymap {
			query := queryinterface.(string)
			query = roundSignificant(query, DataRoundSignificant)
			countmetric := fetchCount(opts.getP8sAPI(), query, opts.getQueryDateTime())

			counts[metricName] = countmetric
		}

		result := resultHolder{definition: queryMetric, counts: counts}
		resultChan <- result
	}

	fetchMutipleQueryRange := func(queryMetric queryBasicMetric, resultChan chan resultHolder) {
		defer wg.Done()

		querymap := opts.buildQueryStrings(queryMetric)

		ranges := make(map[string]*RangeMetrix)
		for metricName, queryinterface := range querymap {

			if _, ok := queryinterface.(map[string]string); !ok {
				metricsLogger.Errorf("not support metrics %s", metricName)

			} else {
				queryMap := queryinterface.(map[string]string)
				rangeMetrics := make(RangeMetrix)
				for metricname, query := range queryMap {

					query = roundSignificant(query, DataRoundSignificant)
					rangeMetrics[metricname] = fetchRange(opts.getP8sAPI(), query, bounds)

				}
				ranges[metricName] = &rangeMetrics
			}
		}

		result := resultHolder{definition: queryMetric, mutipleRanges: ranges}
		resultChan <- result

	}
	queryMetrics := opts.getQueryMetrics()
	maxResults := len(queryMetrics)
	results := make(chan resultHolder, maxResults)

	for _, queryMetric := range queryMetrics {
		wg.Add(1)

		if queryMetric.hasMutipleQuery() {
			go fetchMutipleQueryRange(queryMetric, results)
		} else {
			go fetchQueryCount(queryMetric, results)
		}

	}

	wg.Wait()

	Metrics := make(map[string]interface{})

	close(results)
	for result := range results {

		if result.definition.hasMutipleQuery() {

			if mutipleRanges, ok := result.mutipleRanges[result.definition.queryName]; ok {

				mutipleResult := generateMutipleRateRange(mutipleRanges)
				Metrics[result.definition.queryName] = mutipleResult
			}
		} else if result.definition.isAvg() {

			if countMetric, ok := result.counts[result.definition.queryName]; ok {

				Metrics[result.definition.queryName] = generateRateAVG(countMetric)
			}
		} else {

			if countMetric, ok := result.counts[result.definition.queryName]; ok {

				Metrics[result.definition.queryName] = generateCountGroup(countMetric)
			}
		}

	}

	return Metrics, nil
}

func fetchRange(api prom_v1.API, query string, bounds prom_v1.Range) *RangeMetric {
	result, err := api.QueryRange(context.Background(), query, bounds)
	if err != nil {
		metricsLogger.Errorf("fetchRange query: %s, error: %s", query, err)
		return &RangeMetric{err: err}
	}

	switch result.Type() {
	case model.ValMatrix:
		return &RangeMetric{Matrix: result.(model.Matrix)}
	}
	return &RangeMetric{err: fmt.Errorf("invalid query, matrix expected: %s", query)}
}

func fetchCount(api prom_v1.API, query string, endtime time.Time) *CountMetric {
	result, err := api.Query(context.Background(), query, endtime)
	if err != nil {
		metricsLogger.Errorf("fetcount query: %s, error: %s", query, err)
		return &CountMetric{err: err}
	}
	switch result.Type() {
	case model.ValVector:
		return &CountMetric{Matrix: result.(model.Vector)}
	}
	return &CountMetric{err: fmt.Errorf("invalid query, matrix expected: %s", query)}
}

func generateMutipleRateRange(histo *RangeMetrix) map[string][]*TimeStampMetrics {

	//metricsLogger.Debug(*histo)
	result := make(map[string][]*TimeStampMetrics)
	for k, m := range *histo {

		result[k] = generateTimeStampMetrics(m)
	}

	return result
}
func generateTimeStampMetrics(metrics *RangeMetric) []*TimeStampMetrics {

	matrix := metrics.Matrix

	ts := []*TimeStampMetrics{}

	if matrix.Len() != 0 {

		requestRate := matrix[0].Values
		for _, s := range requestRate {
			val := s.Value
			if math.IsNaN(float64(val)) {
				ts = append(ts, &TimeStampMetrics{s.Timestamp.Unix(), 0})
			} else {
				ts = append(ts, &TimeStampMetrics{s.Timestamp.Unix(), float64(s.Value)})
			}

		}

	}
	return ts

}

func generateRateAVG(metrics *CountMetric) (total float64) {

	//metricsLogger.Debug(*metrics)
	samples := metrics.Matrix
	if len(samples) != 0 {

		return float64(samples[0].Value)
	}
	return

}

func generateCountGroup(metrics *CountMetric) RequestCount {
	//metricsLogger.Debug(*metrics)
	requestCount := metrics.Matrix
	r := RequestCount{}
	for _, s := range requestCount {
		m := s.Metric
		lCode, ok := m["response_code"]
		if !ok || len(lCode) == 0 {
			continue
		}

		code := string(lCode)

		switch {
		case strings.HasPrefix(code, "2"):
			r.Http_2xx += int32(s.Value)
		case strings.HasPrefix(code, "3"):
			r.Http_3xx += int32(s.Value)
		case strings.HasPrefix(code, "4"):
			r.Http_4xx += int32(s.Value)
		case strings.HasPrefix(code, "5"):
			r.Http_5xx += int32(s.Value)
		}
	}
	return r
}

// roundSignificant will output promQL that performs rounding only if the resulting value is significant, that is, higher than the requested precision
func roundSignificant(innerQuery string, precision float64) string {
	return fmt.Sprintf("round(%s, %f) > %f or %s", innerQuery, precision, precision, innerQuery)
}

func GetNamespaceWorkloadBySource(api prom_v1.API, namespace string, startTime, endTime int) ([]string, error) {
	query := fmt.Sprintf(`sum(increase(%s{reporter="source",destination_workload_namespace="%s"} [%vs])) by (source_workload)`,
		ISTIO_REQUEST_TOTAL,
		namespace,
		endTime-startTime)
	//log.Info(query)
	queryValue, err := api.Query(context.Background(), query, time.Unix(int64(endTime), 0))
	if err != nil {
		return []string{}, err
	}
	switch queryValue.Type() {
	case model.ValVector:
		v := queryValue.(model.Vector)
		result := make([]string, 0, len(v))
		for _, s := range v {
			if s.Value != 0 {
				result = append(result, string(s.Metric["source_workload"]))
			}
		}
		return result, nil
	}

	return nil, fmt.Errorf("invalid query, matrix expected: %s", query)
}
func GetNamespaceWorkloadServices(api prom_v1.API, namespace, desWorkload string, startTime, endTime int) ([]string, error) {
	query := fmt.Sprintf(`sum(increase(%s{reporter="source",destination_workload_namespace="%s",destination_workload="%s"} [%vs])) by (destination_service_name)`,
		ISTIO_REQUEST_TOTAL,
		namespace,
		desWorkload,
		endTime-startTime)
	//log.Info(query)
	queryValue, err := api.Query(context.Background(), query, time.Unix(int64(endTime), 0))
	if err != nil {
		return []string{}, err
	}
	switch queryValue.Type() {
	case model.ValVector:
		v := queryValue.(model.Vector)
		result := make([]string, 0, len(v))
		for _, s := range v {
			if s.Value != 0 {
				result = append(result, string(s.Metric["destination_service_name"]))
			}
		}
		return result, nil
	}

	return nil, fmt.Errorf("invalid query, matrix expected: %s", query)
}
func GetNamespaceWorkloadByDestination(api prom_v1.API, namespace string, startTime, endTime int) ([]string, error) {
	query := fmt.Sprintf(`sum(increase(%s{reporter="source",destination_workload_namespace="%s"} [%vs])) by (destination_workload)`,
		ISTIO_REQUEST_TOTAL,
		namespace,
		endTime-startTime)
	//log.Info(query)
	queryValue, err := api.Query(context.Background(), query, time.Unix(int64(endTime), 0))
	if err != nil {
		return []string{}, err
	}
	switch queryValue.Type() {
	case model.ValVector:
		v := queryValue.(model.Vector)
		result := make([]string, 0, len(v))
		for _, s := range v {
			if s.Value != 0 {
				result = append(result, string(s.Metric["destination_workload"]))
			}
		}
		return result, nil
	}

	return nil, fmt.Errorf("invalid query, matrix expected: %s", query)
}

func GetMutiWorkloadsByNamespace(ctx context.Context, opts *MetricsQueryOptions) ([]*models.ViewWorkload, error) {
	nsQuery := common.NewSameNamespaceQuery(opts.Namespace)

	workloadsChan := make(chan []*wd.Workload)
	errChan := make(chan error)

	workloadsFromMetricsChanDource := make(chan []string)
	workloadsFromMetricsChanDestination := make(chan []string)

	go func() {
		defer close(workloadsChan)
		workloads, err := wd.FetchWorkloads(opts.K8sClient, nsQuery, "")
		if err != nil {
			errChan <- err
		}
		workloadsChan <- workloads
	}()

	go func() {
		defer close(workloadsFromMetricsChanDource)
		workloadsFromMetricsSource, err := GetNamespaceWorkloadBySource(opts.getP8sAPI(), opts.Namespace, opts.StartTime, opts.EndTime)
		if err != nil {
			errChan <- err
		}
		workloadsFromMetricsChanDource <- workloadsFromMetricsSource
	}()

	go func() {
		defer close(workloadsFromMetricsChanDestination)
		fromMetricsDestination, err := GetNamespaceWorkloadByDestination(opts.getP8sAPI(), opts.Namespace, opts.StartTime, opts.EndTime)
		if err != nil {
			errChan <- err
		}
		workloadsFromMetricsChanDestination <- fromMetricsDestination
	}()

	workloads := <-workloadsChan
	viewModels := []*models.ViewWorkload{}

	for _, workload := range workloads {
		viewModel := &models.ViewWorkload{}
		viewModel.Parse(workload)
		viewModels = append(viewModels, viewModel)
	}

	workloadsFromMetrics := <-workloadsFromMetricsChanDource
	workloadsFromMetricsDestination := <-workloadsFromMetricsChanDestination
	workloadsFromMetrics = append(workloadsFromMetrics, workloadsFromMetricsDestination...)
	var found bool
	for _, workload := range workloadsFromMetrics {
		found = false
		for _, viewModel := range viewModels {
			if workload == viewModel.Name {
				found = true
				break
			}
		}

		if found == false {
			viewModel := &models.ViewWorkload{
				Name: workload,
			}
			viewModels = append(viewModels, viewModel)
		}
	}

	models := []*models.ViewWorkload{}
	for _, viewModel := range viewModels {
		svcsFromMetricsSource := make(chan []string)
		go func() {
			defer close(svcsFromMetricsSource)
			svcsFromMetrics, err := GetNamespaceWorkloadServices(opts.getP8sAPI(), opts.Namespace, viewModel.Name, opts.StartTime, opts.EndTime)
			if err != nil {
				errChan <- err
			}
			svcsFromMetricsSource <- svcsFromMetrics
		}()
		svcs := <-svcsFromMetricsSource
		viewModel.Services = svcs
		models = append(models, viewModel)
	}

	return models, nil
}

func GetMutipleWorkloadClients(opts *MetricsQueryOptions) ([]models.SourceWorkloads, error) {
	errChan := make(chan error)
	clientsChannel := make(chan []models.SourceWorkloads)

	go func() {
		defer close(clientsChannel)
		metrics, err := GetNSWorkloadClients(opts.getP8sAPI(), opts.Namespace, opts.Workload, opts.StartTime, opts.EndTime)
		if err != nil {
			errChan <- err
		}
		clientsChannel <- metrics
	}()

	var clientsWorkloads []models.SourceWorkloads
	select {
	case err := <-errChan:
		metricsLogger.Errorf("fetch clients workloads by workload name failed,workload===%s", opts.Workload)
		return nil, err
	default:
		clientsWorkloads = <-clientsChannel
	}

	return clientsWorkloads, nil
}
func GetMutipleWorkloadPods(opts *MetricsQueryOptions) ([]*models.ViewPod, error) {
	pods, err := wd.FetchK8sPods(opts.K8sClient, opts.Namespace, opts.Workload)
	if err != nil && !k8serrors.IsNotFound(err) {
		metricsLogger.Errorf("metricsOpts FetchK8sPods,worload=%s, err= %v", opts.Workload, err)
		return nil, err
	}
	errChan := make(chan error)
	podFromMetricsChanSource := make(chan []string)
	podFromMetricsChanDestination := make(chan []string)

	go func() {
		defer close(podFromMetricsChanDestination)
		psFromMetrics, err := GetNSWorkloadPodsByDestination(opts.getP8sAPI(), opts.Namespace, opts.Workload, opts.StartTime, opts.EndTime)
		if err != nil {
			errChan <- err
		}
		podFromMetricsChanDestination <- psFromMetrics
	}()

	go func() {
		defer close(podFromMetricsChanSource)
		psFromMetrics, err := GetNSWorkloadPodsBySource(opts.getP8sAPI(), opts.Namespace, opts.Workload, opts.StartTime, opts.EndTime)
		if err != nil {
			errChan <- err
		}
		podFromMetricsChanSource <- psFromMetrics
	}()

	viewModels := []*models.ViewPod{}
	if pods != nil && len(pods) > 0 {
		for _, p := range pods {
			if p.Name == "" || len(p.Name) < 1 {
				continue
			}
			viewModel := &models.ViewPod{Name: p.Name}
			viewModels = append(viewModels, viewModel)
		}
	}

	var podFromMetricsDestination []string
	var podFromMetrics []string
	select {
	case err = <-errChan:
		metricsLogger.Errorf("fetch pods by workload name failed,workload===%s", opts.Workload)
		return nil, err
	default:
		podFromMetricsDestination = <-podFromMetricsChanDestination
		podFromMetrics = <-podFromMetricsChanSource
	}
	podFromMetricsDestination = append(podFromMetricsDestination, podFromMetrics...)

	var found bool
	for _, pod := range podFromMetricsDestination {
		if pod == "" || len(pod) < 1 {
			continue
		}
		found = false
		for _, viewModel := range viewModels {
			if pod == viewModel.Name {
				found = true
				break
			}
		}
		if found == false {
			viewModel := &models.ViewPod{Name: pod}
			viewModels = append(viewModels, viewModel)
		}
	}
	return viewModels, nil
}

func GetMutipleWorkloadMethods(ctx context.Context, opts *MetricsQueryOptions) ([]string, error) {

	errChan := make(chan error)
	metricsChans := make(chan []string)

	go func() {
		defer close(metricsChans)
		methodMetrics, err := GetNSWorkloadMethods(opts.getP8sAPI(), opts.Namespace, opts.Workload, opts.StartTime, opts.EndTime)
		if err != nil {
			errChan <- err
		}
		metricsChans <- methodMetrics
	}()
	reqMethodsMetrics := <-metricsChans
	methods := make([]string, 0)
	for _, m := range reqMethodsMetrics {
		metricsLogger.Debugf("metrics .mmmmm=%+v\n", m)
		methods = append(methods, m)
	}

	metricsLogger.Debugf("metrics viewModels.viewModele=%d\n", len(methods))
	return methods, nil
}

func GetNSWorkloadPodsBySource(api prom_v1.API, namespace, workload string, startTime, endTime int) ([]string, error) {
	query := fmt.Sprintf(`sum(increase(%s{reporter="source",source_workload_namespace="%s",source_workload="%s"} [%vs])) by (source_pod)`,
		ISTIO_REQUEST_TOTAL,
		namespace,
		workload,
		endTime-startTime)
	//log.Info(query)
	queryValue, err := api.Query(context.Background(), query, time.Unix(int64(endTime), 0))
	if err != nil {
		return []string{}, err
	}
	switch queryValue.Type() {
	case model.ValVector:
		v := queryValue.(model.Vector)
		result := make([]string, 0, len(v))
		for _, s := range v {
			if s.Value != 0 {
				result = append(result, string(s.Metric["source_pod"]))
			}
		}
		return result, nil
	}

	return nil, fmt.Errorf("invalid query, matrix expected: %s", query)
}

func GetNSWorkloadClients(api prom_v1.API, namespace, workload string, startTime, endTime int) ([]models.SourceWorkloads, error) {
	query := fmt.Sprintf(`sum(increase(%s{reporter="source",destination_workload_namespace="%s",destination_workload="%s"} [%vs])) by (source_workload,source_app)`,
		ISTIO_REQUEST_TOTAL,
		namespace,
		workload,
		endTime-startTime)
	//log.Info(query)
	queryValue, err := api.Query(context.Background(), query, time.Unix(int64(endTime), 0))
	if err != nil {
		return []models.SourceWorkloads{}, err
	}
	switch queryValue.Type() {
	case model.ValVector:
		v := queryValue.(model.Vector)
		result := make([]models.SourceWorkloads, 0, len(v))
		for _, s := range v {
			updateResult := true
			for i, r := range result {
				if string(s.Metric["source_workload"]) == r.Name {
					updateResult = false
					ap := r.Apps
					update := false
					for _, a := range r.Apps {
						if string(s.Metric["source_app"]) == "" || string(s.Metric["source_app"]) == "unknown" || string(s.Metric["source_app"]) == a {
							continue
						}
						update = true
					}
					if update {
						ap = append(ap, string(s.Metric["source_app"]))
						r.Apps = ap
						result[i] = r
					}
				}
			}
			if updateResult {
				if string(s.Metric["source_app"]) == "" || string(s.Metric["source_app"]) == "unknown" {
					sw := models.SourceWorkloads{Name: string(s.Metric["source_workload"])}
					result = append(result, sw)
				} else {
					sw := models.SourceWorkloads{Name: string(s.Metric["source_workload"]), Apps: []string{string(s.Metric["source_app"])}}
					result = append(result, sw)
				}
			}
		}
		return result, nil
	}

	return nil, fmt.Errorf("invalid query, matrix expected: %s", query)
}
func GetNSWorkloadPodsByDestination(api prom_v1.API, namespace, workload string, startTime, endTime int) ([]string, error) {
	query := fmt.Sprintf(`sum(increase(%s{reporter="source",destination_workload_namespace="%s",destination_workload="%s"} [%vs])) by (destination_pod)`,
		ISTIO_REQUEST_TOTAL,
		namespace,
		workload,
		endTime-startTime)
	//log.Info(query)
	queryValue, err := api.Query(context.Background(), query, time.Unix(int64(endTime), 0))
	if err != nil {
		return []string{}, err
	}
	switch queryValue.Type() {
	case model.ValVector:
		v := queryValue.(model.Vector)
		result := make([]string, 0, len(v))
		for _, s := range v {
			if s.Value != 0 {
				result = append(result, string(s.Metric["destination_pod"]))
			}
		}
		return result, nil
	}

	return nil, fmt.Errorf("invalid query, matrix expected: %s", query)
}

func GetNSWorkloadMethods(api prom_v1.API, namespace, workload string, startTime, endTime int) ([]string, error) {
	query := fmt.Sprintf(`sum(increase(%s{reporter="source",destination_workload_namespace="%s",destination_workload="%s"} [%vs])) by (request_method)`,
		ISTIO_REQUEST_TOTAL,
		namespace,
		workload,
		endTime-startTime)
	//log.Info(query)
	queryValue, err := api.Query(context.Background(), query, time.Unix(int64(endTime), 0))
	if err != nil {
		metricsLogger.Debugf("getNSWorkloadMethods err err  queryValue==%+v\n", err)
		return []string{}, err
	}
	switch queryValue.Type() {
	case model.ValVector:
		v := queryValue.(model.Vector)
		result := make([]string, 0, len(v))
		for _, s := range v {
			if s.Value != 0 {
				result = append(result, string(s.Metric["request_method"]))
			}
		}
		return result, nil
	}

	return nil, fmt.Errorf("invalid query, matrix expected: %s", query)
}

func GetWorkloadPaths(namespace, workload string, client client.Interface) (*models.WorkloadPath, error) {
	deploy, err := wd.FetchK8sDeployment(client, namespace, workload)
	if err != nil && !k8serrors.IsNotFound(err) {
		metricsLogger.Errorf("Error get workload paths namespace %s: %s", namespace, err)
		return nil, err
	}
	if deploy != nil && len(deploy.Annotations) > 0 {
		if c, ok := deploy.Annotations[DeploymentPaths]; ok {
			paths := strings.Split(c, ",")
			if len(paths) > 0 {
				return &models.WorkloadPath{Paths: paths}, nil
			}
		}
	}
	return nil, nil
}

func SetWorkloadPaths(namespace, workload string, wlpath *models.WorkloadPath, client client.Interface) (*v1.Deployment, error) {
	deploy, err := wd.FetchK8sDeployment(client, namespace, workload)
	if err != nil {
		metricsLogger.Errorf("Error set workloads path namespace %s: %s", namespace, err)
		return nil, err
	}
	k := len(wlpath.Paths)
	var paths string
	if k > 0 {
		for i, path := range wlpath.Paths {
			paths += path
			if i != k-1 {
				paths += ","
			}
		}
	}
	update := false
	if len(paths) > 0 {
		if deploy.Annotations == nil {
			deploy.Annotations = make(map[string]string)
		}
		deploy.Annotations[DeploymentPaths] = paths
		update = true
	}
	if len(paths) == 0 {
		if deploy.Annotations != nil {
			delete(deploy.Annotations, DeploymentPaths)
			update = true
		}
	}
	if update {
		deployment, err := client.AppsV1().Deployments(namespace).Update(deploy)
		if err != nil {
			metricsLogger.Errorf("Error update deployment err ns= %s:deploy.name %s,err=%v", namespace, deploy.Name, err)
			return deployment, err
		}
	}

	return deploy, nil
}
