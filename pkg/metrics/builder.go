package metrics

import (
	"fmt"
	"strings"
)

func (opts *MetricsQueryOptions) buildlabelStrings(q queryBasicMetric) (labelstr string, labelsErr string) {
	labels := []string{`reporter="source"`}
	errLabels := []string{}
	ref := "destination"
	if q.direction == "outbound" {
		ref = "source"
	}

	if opts.Service != "" {
		// inbound only
		labels = append(labels, fmt.Sprintf(`destination_service_name="%s"`, opts.Service))
		if opts.Namespace != "" {
			labels = append(labels, fmt.Sprintf(`destination_service_namespace="%s"`, opts.Namespace))
		}
	} else if opts.Namespace != "" {
		labels = append(labels, fmt.Sprintf(`%s_workload_namespace="%s"`, ref, opts.Namespace))
	}
	if opts.Workload != "" {
		labels = append(labels, fmt.Sprintf(`%s_workload="%s"`, ref, opts.Workload))
	}
	if opts.Pod != "" {
		labels = append(labels, fmt.Sprintf(`%s_pod="%s"`, ref, opts.Pod))
	}
	if opts.SourceClient != "" {
		labels = append(labels, fmt.Sprintf(`source_workload="%s"`, opts.SourceClient))
	}

	// TODO add path here if opts.path
	if opts.Path != "" {
		if strings.HasSuffix(opts.Path, "*") {
			labels = append(labels, fmt.Sprintf(`request_url_path=~"%s"`, strings.TrimSuffix(opts.Path, "*")+".*"))
		} else {
			labels = append(labels, fmt.Sprintf(`request_url_path="%s"`, opts.Path))
		}
	}

	if opts.Method != "" {
		labels = append(labels, fmt.Sprintf(`request_method="%s"`, opts.Method))
	}

	labelstr = "{" + strings.Join(labels, ",") + "}"

	if q.useErrorLabels {

		errLabels = append(labels, `response_code=~"^[4-5]\\d\\d$"`)

	}

	labelsErr = "{" + strings.Join(errLabels, ",") + "}"

	return
}

func (opts *MetricsQueryOptions) buildQueryStrings(q queryBasicMetric) (matricQuery map[string]interface{}) {

	dateTimeRange := opts.EndTime - opts.StartTime
	labels, labelsErr := opts.buildlabelStrings(q)
	matricQuery = make(map[string]interface{})
	switch q.dataType {
	case DataTypeRangeWithErr:
		queryMap := map[string]string{
			q.metricName:                        fmt.Sprintf("sum(%s(%s%s[%vs]))", q.rateFunc, q.istioMetricName, labels, opts.Step),
			fmt.Sprintf("%s_err", q.metricName): fmt.Sprintf("sum(%s(%s%s[%vs]))", q.rateFunc, q.istioMetricName, labelsErr, opts.Step),
			"rps_err_ratio":                     fmt.Sprintf("sum(%s(%s%s[%vs])) / sum(%s(%s%s[%vs]))", q.rateFunc, q.istioMetricName, labelsErr, opts.Step, q.rateFunc, q.istioMetricName, labels, opts.Step),
		}
		matricQuery[q.queryName] = queryMap
	case DataTypeAvgCount:
		matricQuery[q.queryName] = fmt.Sprintf("avg(%s(%s%s[%vs]))", q.rateFunc, q.istioMetricName, labels, dateTimeRange)
	case DataTypeAvgHisto:
		matricQuery[q.queryName] = fmt.Sprintf("sum(rate(%s_sum%s[%vs])) / sum(rate(%s_count%s[%vs]))",
			q.istioMetricName, labels, dateTimeRange, q.istioMetricName, labels, dateTimeRange)
	case DataTypeCountGroup:
		matricQuery[q.queryName] = fmt.Sprintf("sum(%s(%s%s[%vs])) by (%s)", q.rateFunc, q.istioMetricName, labels, dateTimeRange, q.groupby)
	case DataTypeHistogram:
		matricQuery[q.queryName] = opts.histogramQueryString(q, labels)
	}

	return
}

func (opts *MetricsQueryOptions) histogramQueryString(q queryBasicMetric, labels string) (histogram map[string]string) {

	dateTimeRange := opts.EndTime - opts.StartTime
	histogram = make(map[string]string)
	for _, quantile := range opts.Quantiles {

		if quantile == "avg" {
			query := fmt.Sprintf("sum(rate(%s_sum%s[%vs])) / sum(rate(%s_count%s[%vs]))",
				q.istioMetricName, labels, dateTimeRange, q.istioMetricName, labels, dateTimeRange)

			histogram["avg"] = query

		} else {
			query := fmt.Sprintf("histogram_quantile(%s, sum(rate(%s_bucket%s[%vs])) by (le))",
				quantile, q.istioMetricName, labels, opts.Step)

			// Example: round(histogram_quantile(0.5, sum(rate(my_histogram_bucket{foo=bar}[5m])) by (le,baz)), 0.001)
			histogram[quantile] = query
		}
	}
	return

}
