package metrics

import (
	"strings"

	pkgerrors "github.com/pkg/errors"

	"bitbucket.org/mathildetech/hermes/pkg/logging"
	restful "github.com/emicklei/go-restful"
)

const (
	// DefaultStep for 60 miniseconds
	DefaultStep = 60

	// PathParameterNamespace is the path parameter name of namespace
	PathParameterNamespace = "namespace"
	// PathParameterName is the path parameter name of resource
	PathParameterName = "name"
	// PathParameterStartTime path parameterStart time
	PathParameterStartTime = "start_time"
	// PathParameterEndTime is the path parameter end_time
	PathParameterEndTime         = "end_time"
	PathParameterStep            = "step"
	PathParameterService         = "service"
	PathParameterWorkload        = "workload"
	PathParameterSourceNamespace = "source_namespace"
	PathParameterSourceWorkload  = "source_workload"
	PathParameterSourceService   = "source_service"
	PathParameterTargetNamespace = "target_namespace"
	PathParameterTargetWorkload  = "target_workload"
	PathParameterTargetService   = "target_service"
	QueryParameterMetrics        = "metrics"
	QueryParemeterQuantiles      = "quantiles"
	QueryParameterInstances      = "instances"
	QueryParameterClients        = "sourceworkloads"
	Path                         = "path"
	Method                       = "method"
	Workload                     = "workload"
)

// MetricsQueryOptions as object for metrics query parameters
type MetricsQueryOptions struct {
	MetricsBasicQueryOptions
	Service         string
	Workload        string
	Pod             string
	IsDetails       bool
	Instances       []string
	SourceWorkloads []string
	SourceClient    string
	Path            string
	Method          string
}

var (
	metricsLogger = logging.RegisterScope("metrics")
)

// ParseForWorkload restful request to metricsquery options workload
func (opts *MetricsQueryOptions) ParseForWorkload(req *restful.Request) error {

	workload := req.PathParameter(PathParameterName)
	if workload == "" {
		workload = req.QueryParameter(PathParameterWorkload)
	}
	if workload == "" {
		metricsLogger.Error("path parameters workload or name connot be empty")
		return pkgerrors.New("Error for path parameters workload or name connot be empty")
	}

	opts.Workload = workload

	return nil

}

// ParseForWorkload restful request to metricsquery options workload
func (opts *MetricsQueryOptions) ParseForWorkloadName(req *restful.Request) error {

	workload := req.PathParameter(Workload)
	if workload == "" {
		workload = req.QueryParameter(Workload)
	}
	if workload == "" {
		metricsLogger.Error("path parameters workload or name connot be empty")
		return pkgerrors.New("Error for path parameters workload or name connot be empty")
	}

	opts.Workload = workload

	return nil

}

// ParseForPathAndMethod restful request to metricsquery options path and method
func (opts *MetricsQueryOptions) ParseForPathAndMethod(req *restful.Request) error {
	path := req.QueryParameter(Path)
	if path == "" {
		metricsLogger.Error("path parameters path connot be empty")
		return pkgerrors.New("Error for path parameters path connot be empty")
	}
	opts.Path = path

	method := req.QueryParameter(Method)
	if method == "" {
		metricsLogger.Error("path param method connot be empty")
		return pkgerrors.New("Error for urlpath param method connot be empty")
	}
	opts.Method = method

	return nil

}

// ParseForService restful request to metricsquery options
func (opts *MetricsQueryOptions) ParseForService(req *restful.Request) error {
	service := req.QueryParameter(PathParameterService)
	if service == "" {
		metricsLogger.Error("path parameters service connot be empty")
		return pkgerrors.New("Error for path parameters service connot be empty")
	}

	opts.Service = service

	workload := req.QueryParameter(PathParameterWorkload)
	if workload != "" {
		opts.Workload = workload
	}

	return nil

}

// Parse restful request to metricsquery options
func (opts *MetricsQueryOptions) ParseInstances(req *restful.Request) error {

	instances := req.QueryParameter(QueryParameterInstances)
	if instances != "" {
		opts.Instances = strings.Split(instances, ",")
	} else {
		return pkgerrors.New("instances is required")
	}

	if len(opts.Instances) == 0 {
		return pkgerrors.New("instances is required")
	}

	return nil
}

// Parse restful request to metricsquery options sourceWorkloads to get clients metrcis
func (opts *MetricsQueryOptions) ParseClients(req *restful.Request) error {

	clients := req.QueryParameter(QueryParameterClients)
	if clients != "" {
		opts.SourceWorkloads = strings.Split(clients, ",")
	} else {
		return pkgerrors.New("sourceWorkloads is required")
	}

	if len(opts.SourceWorkloads) == 0 {
		return pkgerrors.New("sourceWorkloads is required")
	}

	return nil
}

// Parse restful request to metricsquery options
func (opts *MetricsQueryOptions) Copy() MetricsQueryOptions {

	newOpts := MetricsQueryOptions{
		Pod:             opts.Pod,
		Service:         opts.Service,
		Workload:        opts.Workload,
		IsDetails:       opts.IsDetails,
		Instances:       opts.Instances,
		SourceWorkloads: opts.SourceWorkloads,
		SourceClient:    opts.SourceClient,
	}

	metricsQuery := []queryBasicMetric{}

	for _, query := range opts.QueryMetrics {

		metricsQuery = append(metricsQuery, query)
	}

	newOpts.MetricsBasicQueryOptions = MetricsBasicQueryOptions{
		P8sAPI:       opts.P8sAPI,
		Namespace:    opts.Namespace,
		StartTime:    opts.StartTime,
		EndTime:      opts.EndTime,
		Step:         opts.Step,
		Quantiles:    opts.Quantiles,
		QueryMetrics: metricsQuery,
	}

	return newOpts
}
