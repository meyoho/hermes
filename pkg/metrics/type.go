package metrics

import (
	"bitbucket.org/mathildetech/hermes/pkg/common"
	"encoding/json"

	"github.com/prometheus/common/model"
)

const (
	NODE_EDGE_TYPE     = "edge"
	NODE_WORKLOAD_TYPE = "workload"
	NODE_SERVICE_TYPE  = "service"
	NODE_Pod_TYPE      = "pod"
	Quantile50         = 0.50
	Quantile90         = 0.90
	Quantile99         = 0.99
)

var (
	DeploymentPaths = "asm." + common.GetLocalBaseDomain() + "/path"
)

type ServiceMetrics struct {
	StartTime           int                            `json:"start_time"`
	EndTime             int                            `json:"end_time"`
	Step                int                            `json:"step"`
	Type                string                         `json:"type"`
	Namespace           string                         `json:"namespace"`
	Service             string                         `json:"service,omitempty"`
	Workloads           []string                       `json:"workloads"`
	RequestCount        RequestCount                   `json:"request_count"`
	RequestRate         []*TimeStampMetrics            `json:"request_rate,omitempty"`
	ErrorRate           []*TimeStampMetrics            `json:"error_rate,omitempty"`
	RequestResponseTime map[string][]*TimeStampMetrics `json:"request_response_time,omitempty"`
}

type WorkloadMetrics struct {
	StartTime           int                            `json:"start_time"`
	EndTime             int                            `json:"end_time"`
	Step                int                            `json:"step"`
	Type                string                         `json:"type"`
	Namespace           string                         `json:"namespace"`
	Services            []string                       `json:"services,omitempty"`
	Workload            string                         `json:"workload"`
	RequestCountIn      RequestCount                   `json:"request_count_in"`
	RequestCountOut     RequestCount                   `json:"request_count_out"`
	RequestRateIn       []*TimeStampMetrics            `json:"request_rate_in,omitempty"`
	RequestRateOut      []*TimeStampMetrics            `json:"request_rate_out,omitempty"`
	ErrorRateIn         []*TimeStampMetrics            `json:"error_rate_in,omitempty"`
	ErrorRateOut        []*TimeStampMetrics            `json:"error_rate_out,omitempty"`
	RequestResponseTime map[string][]*TimeStampMetrics `json:"request_response_time,omitempty"`
	AvgRequestRateIn    float64                        `json:"avg_request_rate_in,omitempty"`
	AvgRequestRateOut   float64                        `json:"avg_request_rate_out,omitempty"`
	AvgResponseTime     float64                        `json:"avg_response_time,omitempty"`
}

type PodMetrics struct {
	StartTime           int                            `json:"start_time"`
	EndTime             int                            `json:"end_time"`
	Step                int                            `json:"step"`
	Type                string                         `json:"type"`
	Namespace           string                         `json:"namespace"`
	Pod                 string                         `json:"pod"`
	RequestCountIn      RequestCount                   `json:"request_count_in"`
	RequestCountOut     RequestCount                   `json:"request_count_out"`
	RequestRateIn       []*TimeStampMetrics            `json:"request_rate_in,omitempty"`
	RequestRateOut      []*TimeStampMetrics            `json:"request_rate_out,omitempty"`
	ErrorRateIn         []*TimeStampMetrics            `json:"error_rate_in,omitempty"`
	ErrorRateOut        []*TimeStampMetrics            `json:"error_rate_out,omitempty"`
	RequestResponseTime map[string][]*TimeStampMetrics `json:"request_response_time,omitempty"`
	AvgRequestRateIn    float64                        `json:"avg_request_rate_in,omitempty"`
	AvgRequestRateOut   float64                        `json:"avg_request_rate_out,omitempty"`
	AvgResponseTime     float64                        `json:"avg_response_time,omitempty"`
}

type EdgeMetrics struct {
	QueryOptions *EdgeMetricsQueryOptions `json:"query_options"`
	RequestCount RequestCount             `json:"request_count"`
	RequestRate  []*TimeStampMetrics      `json:"request_rate,omitempty"`
	ErrorRate    []*TimeStampMetrics      `json:"error_rate,omitempty"`

	RequestResponseTime map[string][]*TimeStampMetrics `json:"request_response_time,omitempty"`
}

type BasicMetrics struct {
	StartTime int      `json:"start_time"`
	EndTime   int      `json:"end_time"`
	Step      int      `json:"step"`
	Type      string   `json:"type"`
	Namespace string   `json:"namespace"`
	Services  []string `json:"services,omitempty"`
	Workload  string   `json:"workload,omitempty"`
	Service   string   `json:"service,omitempty"`
	Pod       string   `json:"pod,omitempty"`
}

type RequestCount struct {
	Http_2xx int32 `json:"http_2xx"`
	Http_3xx int32 `json:"http_3xx"`
	Http_4xx int32 `json:"http_4xx"`
	Http_5xx int32 `json:"http_5xx"`
}

type TimeStampMetrics struct {
	int64
	float64
}

// MarshalJSON implements json.Marshaler.
func (ts TimeStampMetrics) MarshalJSON() ([]byte, error) {
	return json.Marshal([]interface{}{ts.int64, ts.float64})
}

// Metric holds the Prometheus Matrix model, which contains one or more time series (depending on grouping)
type RangeMetric struct {
	Matrix model.Matrix `json:"matrix"`
	err    error
}

type CountMetric struct {
	Matrix model.Vector `json:"matrix"`
	err    error
}

// RangeMetrics contains Metric objects for several RangeMetric-kind statistics
type RangeMetrix = map[string]*RangeMetric

// CountMetrix contains Metric objects for several CountMetric-kind statistics
type CountMetrix = map[string]*CountMetric
