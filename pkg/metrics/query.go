package metrics

const (
	MetricsResponseTime           = "response_time"
	DataTypeCountGroup   DataType = "CountGroup"
	DataTypeAvgCount     DataType = "AvgCount"
	DataTypeAvgHisto     DataType = "AvgHisto"
	DataTypeHistogram    DataType = "Histo"
	DataTypeRange        DataType = "Range"
	DataTypeRangeWithErr DataType = "RangeWithErr"
)

type DataType string

type queryBasicMetric struct {
	queryName       string
	metricName      string
	istioMetricName string
	direction       string
	isRange         bool
	useErrorLabels  bool
	rateFunc        string
	groupby         string
	dataType        DataType
}

//avg_response_time, avg_rps_in, avg_rps_out, response_time, rps_in, rps_out, requests_total_in, requests_total_out

var istioMetrics = map[string]queryBasicMetric{
	"avg_response_time": {
		queryName:       "avg_response_time",
		istioMetricName: "istio_request_duration_seconds",
		isRange:         false,
		rateFunc:        "rate",
		dataType:        DataTypeAvgHisto,
	},
	"avg_rps_in": {
		queryName:       "avg_rps_in",
		istioMetricName: "istio_requests_total",
		isRange:         false,
		useErrorLabels:  false,
		direction:       "inbound",
		rateFunc:        "rate",
		dataType:        DataTypeAvgCount,
	},
	"avg_rps_out": {
		queryName:       "avg_rps_out",
		istioMetricName: "istio_requests_total",
		isRange:         false,
		useErrorLabels:  false,
		direction:       "outbound",
		rateFunc:        "rate",
		dataType:        DataTypeAvgCount,
	},
	"response_time": {
		queryName:       "response_time",
		istioMetricName: "istio_request_duration_seconds",
		isRange:         true,
		direction:       "inbound",
		useErrorLabels:  false,
		rateFunc:        "rate",
		dataType:        DataTypeHistogram,
	},
	"rps_in": {
		queryName:       "rps_in",
		metricName:      "rps",
		istioMetricName: "istio_requests_total",
		isRange:         true,
		direction:       "inbound",
		useErrorLabels:  true,
		rateFunc:        "rate",
		dataType:        DataTypeRangeWithErr,
	},
	"rps_out": {
		queryName:       "rps_out",
		metricName:      "rps",
		istioMetricName: "istio_requests_total",
		isRange:         true,
		direction:       "outbound",
		useErrorLabels:  true,
		rateFunc:        "rate",
		dataType:        DataTypeRangeWithErr,
	},
	"requests_total_in": {
		queryName:       "requests_total_in",
		istioMetricName: "istio_requests_total",
		isRange:         true,
		direction:       "inbound",
		useErrorLabels:  false,
		rateFunc:        "increase",
		groupby:         "response_code",
		dataType:        DataTypeCountGroup,
	},
	"requests_total_out": {
		queryName:       "requests_total_out",
		istioMetricName: "istio_requests_total",
		isRange:         true,
		direction:       "outbound",
		useErrorLabels:  false,
		rateFunc:        "increase",
		groupby:         "response_code",
		dataType:        DataTypeCountGroup,
	},
}

func (q *queryBasicMetric) hasMutipleQuery() bool {

	if q.dataType == DataTypeRangeWithErr || q.dataType == DataTypeHistogram {
		return true
	}

	return false

}

func (q *queryBasicMetric) isAvg() bool {
	if q.dataType == DataTypeAvgCount || q.dataType == DataTypeAvgHisto {
		return true
	}
	return false
}

func (q *queryBasicMetric) isCountGroup() bool {

	if q.dataType == DataTypeCountGroup {
		return true
	}
	return false
}
