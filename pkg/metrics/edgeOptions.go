package metrics

import (
	"fmt"
	"strconv"
	"strings"

	pkgerrors "github.com/pkg/errors"

	restful "github.com/emicklei/go-restful"
)

type EdgeMetricsQueryOptions struct {
	MetricsBasicQueryOptions
	SourceNamespace string `json:"source_namespace"`
	TargetNamespace string `json:"target_namespace"`
	SourceWorkload  string `json:"source_workload"`
	SourceService   string `json:"source_service"`
	TargetWorkload  string `json:"target_workload"`
	TargetService   string `json:"target_service"`
	MetricsType     string `json:"metrics_type"`
	P8sURL          string `json:"-"`
}

// Parse restful request to EdgeMetricsQueryOptions
func (opts *EdgeMetricsQueryOptions) Parse(req *restful.Request) error {
	sourceNamespace := req.QueryParameter(PathParameterSourceNamespace)
	sourceWorkload := req.QueryParameter(PathParameterSourceWorkload)
	sourceService := req.QueryParameter(PathParameterSourceService)
	targetNamespace := req.QueryParameter(PathParameterTargetNamespace)
	targetWorkload := req.QueryParameter(PathParameterTargetWorkload)
	targetService := req.QueryParameter(PathParameterTargetService)
	if (sourceWorkload == "" && sourceService == "") || targetNamespace == "" || (targetWorkload == "" && targetService == "") {
		return pkgerrors.Errorf("missing paramater source_namespace/source_workload/target_namespace/target_workload")
	}
	opts.SourceNamespace = sourceNamespace
	opts.SourceWorkload = sourceWorkload
	opts.SourceService = sourceService
	opts.TargetNamespace = targetNamespace
	opts.TargetWorkload = targetWorkload
	opts.TargetService = targetService
	opts.MetricsType = NODE_EDGE_TYPE

	startTime, err := strconv.Atoi(req.QueryParameter(PathParameterStartTime))
	if err != nil {

		metricsLogger.Error("Error for PathParameterStartTime connot be empty")
		return err
	}
	opts.StartTime = startTime

	endTime, err := strconv.Atoi(req.QueryParameter(PathParameterEndTime))
	if err != nil {

		metricsLogger.Error("Error for PathParameterEndTime connot be empty")
		return err
	}
	opts.EndTime = endTime

	step, err := strconv.Atoi(req.QueryParameter(PathParameterStep))
	if err != nil {

		metricsLogger.Error("Error for PathParameterStep connot be empty")
		return err
	}
	if step == 0 {
		step = DefaultStep
	}
	opts.Step = step

	return nil

}

// Parse restful request to EdgeMetricsQueryOptions
func (opts *EdgeMetricsQueryOptions) buildlabels() (labelstr string, labelsErr string) {
	labels := []string{`reporter="source"`}

	errLabels := []string{}

	if opts.SourceWorkload != "" {
		labels = append(labels, fmt.Sprintf(`source_workload="%s"`, opts.SourceWorkload))

	}

	if opts.SourceNamespace != "" {
		labels = append(labels, fmt.Sprintf(`source_workload_namespace="%s"`, opts.SourceNamespace))

	}

	if opts.TargetService != "" {
		// inbound only
		labels = append(labels, fmt.Sprintf(`destination_service_name="%s"`, opts.TargetService))
		if opts.TargetNamespace != "" {
			labels = append(labels, fmt.Sprintf(`destination_service_namespace="%s"`, opts.TargetNamespace))
		}
	} else if opts.TargetNamespace != "" {
		labels = append(labels, fmt.Sprintf(`destination_workload_namespace="%s"`, opts.TargetNamespace))
	}
	if opts.TargetWorkload != "" {
		labels = append(labels, fmt.Sprintf(`destination_workload="%s"`, opts.TargetWorkload))
	}

	labelstr = "{" + strings.Join(labels, ",") + "}"

	errLabels = append(labels, `response_code=~"^[4-5]\\d\\d$"`)
	labelsErr = "{" + strings.Join(errLabels, ",") + "}"

	return

}

// Parse restful request to EdgeMetricsQueryOptions
func (opts *EdgeMetricsQueryOptions) buildQueryStrings(q queryBasicMetric) (matricQuery map[string]interface{}) {
	dateTimeRange := opts.EndTime - opts.StartTime
	labels, labelsErr := opts.buildlabels()
	matricQuery = make(map[string]interface{})
	switch q.dataType {
	case DataTypeRangeWithErr:
		queryMap := map[string]string{
			q.metricName:                        fmt.Sprintf("sum(%s(%s%s[%vs]))", q.rateFunc, q.istioMetricName, labels, opts.Step),
			fmt.Sprintf("%s_err", q.metricName): fmt.Sprintf("sum(%s(%s%s[%vs]))", q.rateFunc, q.istioMetricName, labelsErr, opts.Step),
		}
		matricQuery[q.queryName] = queryMap

	case DataTypeAvgCount:
		matricQuery[q.queryName] = fmt.Sprintf("avg(%s(%s%s[%vs]))", q.rateFunc, q.istioMetricName, labels, dateTimeRange)
	case DataTypeAvgHisto:
		matricQuery[q.queryName] = fmt.Sprintf("sum(rate(%s_sum%s[%vs])) / sum(rate(%s_count%s[%vs]))",
			q.istioMetricName, labels, dateTimeRange, q.istioMetricName, labels, dateTimeRange)
	case DataTypeCountGroup:
		matricQuery[q.queryName] = fmt.Sprintf("sum(%s(%s%s[%vs])) by (%s)", q.rateFunc, q.istioMetricName, labels, dateTimeRange, q.groupby)
	case DataTypeHistogram:
		matricQuery[q.queryName] = opts.histogramQueryString(q, labels)
	}

	return
}

func (opts *EdgeMetricsQueryOptions) histogramQueryString(q queryBasicMetric, labels string) (histogram map[string]string) {

	dateTimeRange := opts.EndTime - opts.StartTime
	histogram = make(map[string]string)
	for _, quantile := range opts.Quantiles {

		if quantile == "avg" {
			query := fmt.Sprintf("sum(rate(%s_sum%s[%vs])) / sum(rate(%s_count%s[%vs]))",
				q.istioMetricName, labels, dateTimeRange, q.istioMetricName, labels, dateTimeRange)

			histogram["avg"] = query

		} else {
			query := fmt.Sprintf("histogram_quantile(%s, sum(rate(%s_bucket%s[%vs])) by (le))",
				quantile, q.istioMetricName, labels, opts.Step)

			// Example: round(histogram_quantile(0.5, sum(rate(my_histogram_bucket{foo=bar}[5m])) by (le,baz)), 0.001)
			histogram[quantile] = query
		}
	}
	return

}
