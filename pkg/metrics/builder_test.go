package metrics

import (
	"testing"
)

func testbuildlabelStrings(t *testing.T) {

	opts := &MetricsQueryOptions{}
	query := queryBasicMetric{
		queryName:       "avg_response_time",
		istioMetricName: "istio_request_duration_seconds",
		isRange:         false,
		rateFunc:        "rate",
		dataType:        DataTypeAvgHisto,
	}

	queryMatric := opts.buildQueryStrings(query)

	for matricName, querystring := range queryMatric {
		if matricName == "" {
			t.Errorf("MatricName is required for QUeyr :%s", query.metricName)
		}

		if matricName != query.metricName {
			t.Errorf("MatricName is required for QUeyr :%s", query.metricName)

		}

		if querystring.(string) == "" {
			t.Errorf("querystring is required for Queyr :%s", query.metricName)

		}
	}

}
