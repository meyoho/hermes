FROM index.alauda.cn/alaudaorg/alaudabase-alpine-run:alpine3.10

ENV TZ=Asia/Shanghai
ARG commit_id=dev
ARG app_version=dev

WORKDIR /console

CMD ["/asm/hermes"]

ENTRYPOINT ["/bin/sh", "-c", "/asm/hermes \"$0\" \"$@\""]

ADD output/linux/amd64/hermes /asm/hermes

ENV COMMIT_ID=${commit_id}
ENV APP_VERSION=${app_version}